export class ClubWorkoutPlan {
    planId: number;
    workoutName: string;
    workoutDesc: string;
    img: string;
}