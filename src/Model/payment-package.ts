export class PaymentPackage {
    paymentId: number;
    customerId: number;
    invoiceId: number;
    paymentDate: string;
    paymentAmount: number;
    paymentMode: string;
    particulars: string;
    emailReceipt: string;
    service_id: number;
    invoice_gross_amt: number;
    discount_amt: number;
    invoice_net_amt: number;
    invoice_email_flag: string;
    gymId: any;
    duration: string;


   
}