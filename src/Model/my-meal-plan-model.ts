import {MealDetail} from './meal-detail-model';

export class MyMealPlan {
    Name: string;
    mealDetails: MealDetail[];
    Desc: string;
}