export class Exercise {
    customerId: number;
    exerciseId: number;
    exerciseName: string;
    exerciseType: string;
    superSetId: number;
    setSeqId: number;
    forDay: number;
    param1: string;
    param1Uom: string;
    param2: string;
    param2Uom: string;
    param3: string;
    param3Uom: string;
    scheduledDate?: any;
    completedDate?: any;
    checkedCompleted: string;
}