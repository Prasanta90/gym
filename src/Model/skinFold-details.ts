export class SkinFold {
    mmAbdominal: string;
    mmBicep: string;
    mmCalf: string;
    mmChest: string;
    mmMidaxillary: string;
    mmSkinFoldAverage: string;
    mmSkinFoldTotal: string;
    mmSubscapular: string;
    mmSuoraillac: string;
    mmThigh: string;
    mmTricep: string;
}
