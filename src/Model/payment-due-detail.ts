export class PaymentDueDetail {

    invoiceId: number;
    invoiceDate: string;
    serviceName: string;
    serviceDesc: string;
    serviceDuration: string;
    createdBy: string;
    dueDate: string;
    netAmount: number;
    paidAmount: number;
    balanceDue: number;
    paymentyMode: string;
    checked: boolean;
    serviceType: string;
    start_date: string;
    end_date: string;
}