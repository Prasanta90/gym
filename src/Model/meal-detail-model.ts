
import {FoodItem} from './food-item';

export class MealDetail {
    customerPlanId: number;
    completed_date: string;
    completedFlag: string;
    foodItem: FoodItem;
    mealType: string;
    scheduleDate: string;
    trainerId: string;
    mstCustomer?: any;
    meal_icon: any[] = [];
    }
