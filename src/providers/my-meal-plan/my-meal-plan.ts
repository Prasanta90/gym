import { HttpClient } from '@angular/common/http';
import { Http, RequestOptions, Headers } from "@angular/http";
import { Injectable } from '@angular/core';
import { CommonResponse } from "../../Model/common-response";
import { CommonProvider } from "../common/common";

/*
  Generated class for the MyMealPlanProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyMealPlanProvider {

  public getnutritionmealplandetailsUrl: string = "/getnutritionmealplandetails";
  public getnutritionplanpdfUrl: string = "/getnutritionplanpdf";
  public getdatewisenutritionmealplancountUrl: string = "/getdatewisenutritionmealplancount";

  constructor(public http: Http, public commonProvider: CommonProvider) {
    console.log('Hello MyMealPlanProvider Provider');
  }

  //get meal plan details
  getNutritionMealPlanDetails(username: string, byDate: string) {
    var meal: any;
    meal= { 
        "username":username,
        "byDate":byDate
      };
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl+this.getnutritionmealplandetailsUrl, meal, options).toPromise()
        .then(res => <CommonResponse>res.json());
}

//generate pdf
getPdf(username: string, password: string) {
  var cred: any;
  cred= { 
      "username":username,
      "password":password
    };
  
  // let headers = new Headers({ 'Content-Type': 'application/json' });
  // let options = new RequestOptions({ headers: headers });

  return this.http.get(this.commonProvider.hostUrl+this.getnutritionplanpdfUrl+"?username="+cred.username).toPromise()
      .then(res => <CommonResponse>res.json());
}

//get Total meal count list for current week
getMealCountList(username: string) {
  var meal: any;
  meal= { 
      "username":username
    };
  
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
  return this.http.post(this.commonProvider.hostUrl+this.getdatewisenutritionmealplancountUrl, meal, options).toPromise()
      .then(res => <CommonResponse>res.json());
}

}
