import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MoreInfoAboutExercisePage } from '../more-info-about-exercise/more-info-about-exercise';

/**
 * Generated class for the WorkoutDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-workout-details',
  templateUrl: 'workout-details.html',
})
export class WorkoutDetailsPage {

  workout: any;
  workouts: string = "PREVIEW";
  workoutCategories: Array<any>;
  recordDate: any = new Date().toISOString();
  selectedDate: Date;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.selectedDate=this.navParams.get('selectedDate');
    this.workout = this.navParams.get('workout');
    console.log("I'm in workout-details"+JSON.stringify(this.workout));

    this.workoutCategories = [
      {id: 1, name: "Push up"},
      {id: 2, name: "Pull up"},
      {id: 3, name: "Dips"},
      {id: 4, name: "Rowing"},
      {id: 5, name: "Dumbell"},
      {id: 6, name: "curl"}
    ];
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkoutDetailsPage');
  }

  viewMoreInfoAboutExercise(category){
    console.log("Category details:::::"+JSON.stringify(category));
    this.navCtrl.push(MoreInfoAboutExercisePage,{category: category});
  }

}
