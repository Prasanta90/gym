import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { CommonProvider } from '../../providers/common/common';
import { LoginProvider } from '../../providers/login/login';
import { CommonResponse } from '../../Model/common-response';
import { Myprofile } from '../../Model/my-profile';
import { LoginPage } from '../login/login';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  userid: string;
  forgotPasswordForm: FormGroup;
  isEnteredUserId: boolean = true;
  isOtpPresent: boolean = false;
  isSetPassword: boolean = false;
  user: Myprofile;
  response: CommonResponse;
  username: string;
  loader: any;
  otp: any;

  noPattern: string = "^(0|[1-9][0-9]*)$";
  //passwordPattern: string = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$";
  passwordPattern: string = "^(?=.*?[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$";


  validation_messages = {
    'otp': [
      { type: "required", message: "Please enter the OTP." },
      {
        type: "pattern",
        message: "Phone number should be composed of digits."
      },
      { type: "maxlength", message: "OTP should be 6 digit number." },
      { type: "minlength", message: "OTP should be 6 digit number." }
    ],
    'password': [
      { type: 'required', message: 'Please enter your new password.' },
     { type: 'pattern', message: 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character.' },
     { type: "minlength", message: "Password should contain minimum 8 characters." },
      //{ type: "maxlength", message: "Password should contain maximum 10 characters." }
      //{ type: 'pattern', message: 'Password should be composed of digits.' },
      //{ type: 'minLength', message: 'Password should be 6 digit number.' }
    ],
    'confirmPassword': [
      { type: 'required', message: 'Please enter your new password.' },
     { type: 'pattern', message: 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character.' },
     { type: "minlength", message: "Password should contain minimum 8 characters." },
      //{ type: "maxlength", message: "Password should contain maximum 10 characters." }
      //{ type: 'pattern', message: 'Password should be composed of digits.' },
     // { type: 'minLength', message: 'Password should be 6 digit number.' }
    ]

  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    private menu: MenuController, public loginProvider: LoginProvider, public alertCtrl: AlertController,
    public activityLoader: LoadingController) {
    this.username = this.navParams.get('username');
    console.log("this.username in forgot password::::" + this.username);

    this.otp = this.navParams.get('otp');
    console.log("OTP is::::" + this.otp);


    this.forgotPasswordForm = this.formBuilder.group({
      // 'mobile': ['', [Validators.compose([
      //   Validators.pattern(this.numPattern),
      //   Validators.required
      // ])]],
      'otp': ['', [Validators.required,Validators.pattern(this.noPattern), Validators.maxLength(6),Validators.minLength(6)]],
      'password': ['', [Validators.required, Validators.pattern(this.passwordPattern), Validators.minLength(8)]],
      'confirmPassword': ['', [Validators.required, Validators.pattern(this.passwordPattern), Validators.minLength(8)]],
      //'password': ['', [Validators.required, Validators.pattern(this.noPattern), Validators.minLength(6)]],
      //'confirmPassword': ['', [Validators.required, Validators.pattern(this.noPattern), Validators.minLength(6)]]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
  }

  ionViewWillEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }


  //temporary code



  // sendotp(data) {
  //   console.log("Mobile::::" + JSON.stringify(data));
  //   if (data.userid.length > 0) {
  //     if (data.userid.length == 10) {
  //       this.isEnteredUserId = false;
  //       this.isOtpPresent = true;
  //       this.isSetPassword = false;
  //     }
  //     else {

  //     }
  //   }

  //   }

  verifyOtp(data) {
    console.log("OTP:::::::::" + JSON.stringify(data));
    if (data.otp == this.otp) {
      this.isEnteredUserId = false;
      this.isOtpPresent = false;
      this.isSetPassword = true;
    }
    else {
      this.showAlert("Error", "OTP value is not valid!")
    }
  }

  setNewPassword(data) {

    console.log("Form data:::::" + JSON.stringify(data));


    if (data.password === data.confirmPassword) {
      this.presentLoading();
      this.loginProvider.forgotPassword(this.username, data.password).then(res => {
        this.response = res;
        if (this.response.status == 200 && this.response.message == "success") {
          this.dismissLodaing();
          this.showAlert("Success", this.response.responseObj.resetpasswordonotp)
          //this.dismissLodaing();
          this.navCtrl.push(LoginPage, {

          });
        }

        else {
          this.dismissLodaing();
          this.showAlert("Error", this.response.responseObj.forgotpassword)
        }
      });
    }
    else {

      this.showAlert("Error", "Please enter same password.")
    }




  }

  //Activity Indicator
  presentLoading() {
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }


}
