import { Component } from '@angular/core';
import {
  IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController,
  Loading
} from 'ionic-angular';
import { MoreInfoAboutExerciseNextPage } from '../../pages/more-info-about-exercise-next/more-info-about-exercise-next';
//import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
// import { DomSanitizer } from '@angular/platform-browser';
import { WorkoutProvider } from '../../providers/workout/workout';
import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from '../../Model/common-response';
import { DateWiseExerciseDetail } from '../../Model/date-wise-exercise-detail';
// import { PlayVideoModalPage } from '../../pages/play-video-modal/play-video-modal';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import * as Constant from '../../Constant/YTImgConst';

//import { EmbedVideoService } from 'ngx-embed-video';
/**
 * Generated class for the MoreInfoAboutExercisePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-more-info-about-exercise',
  templateUrl: 'more-info-about-exercise.html',
})
export class MoreInfoAboutExercisePage {

  loader: any;
  loading: Loading;
  username: string;
  exerciseDescription: string = "The biceps is a muscle on the front part of the upper arm. The biceps includes a “short head” and a “long head” that work as a single muscle.The biceps is attached to the arm bones by tough connective tissues called tendons. The tendons that connect the biceps muscle to the shoulder joint in two places are called the proximal biceps tendons. The tendon that attaches the biceps muscle to the forearm bones  (radius and ulna) is called the distal biceps tendon. When the biceps contracts, it pulls the forearm up and rotates it outward";
  category: any;
  exerciseList: any[] = [];
  initialVLink = "https://www.youtube.com/embed/";
  videoLink: any;
  embedLink: any;
  iframe_html: any;
  youtubeId: any;
  exerciseIdDemo: number = 59;
  response: CommonResponse;
  isPresentExerciseParam1: boolean = false;
  //isPresentExerciseType: boolean = false;
  // isPresentMuscleGroup: boolean = false;
  // isPresentEquipmentName: boolean = false;
  // isPresentDifficulty: boolean = false;
  isSuperSet: boolean = false;
  exerciseImageUrl: string;
  defaultImageUrl: string = "/assets/imgs/youtube_img.jpg";
  //imageUrl: string = "/assets/imgs/no-image-found-360x260.png";
  //defaultImageUrl: any;
  exerciseParamUnit: any[] =[];
  isPresentExerciseData: boolean = false;
  paramData: string[] = [];
  imgLinkString: string;
  completeVideoLink: string;
  
 


  dateWiseExerciseDetailObj: DateWiseExerciseDetail = new DateWiseExerciseDetail();


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    // private youtube: YoutubeVideoPlayer, private dom: DomSanitizer, 
    public workoutProvider: WorkoutProvider,
    public commonProvider: CommonProvider, public alertCtrl: AlertController, public modalCtrl: ModalController,
    public activityLoader: LoadingController, private iab: InAppBrowser) {
    this.username = localStorage.getItem('username')
    this.category = this.navParams.get('category');
    console.log("I'm in workout-category-details" + JSON.stringify(this.category));

    if (this.category == undefined) {
      this.isSuperSet = true;
    }

    if(this.category != undefined){
      for(let param of this.category.param.ParamValue){
        console.log("Parameter::::"+JSON.stringify(param));
        let paramData = param.split(":");
        this.paramData.push(paramData[1]);
        
      }
    }

    // this.getExerciseDetails(this.username, this.category.superSetId, this.category.scheduledDate, this.category.setSeqId, this.category.exerciseId);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoreInfoAboutExercisePage');
    
  }

  ionViewWillEnter(){
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
    this.getExerciseDetails(this.username, this.category.superSetId, this.category.scheduledDate, this.category.setSeqId, this.category.exerciseId);
  }

  


  //to get exercise details
  getExerciseDetails(username, superSetId, scheduledDate, setSeqId, exerciseId) {
    //this.presentLoading();
    this.workoutProvider.getDatewiseExerciseDetails(username, superSetId, scheduledDate, setSeqId, exerciseId).then(res => {
      this.response = res;
      //this.dismissLodaing();
      console.log("exercise data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {

        if (typeof this.response.responseObj.datewisegetexercisedetails !== 'undefined' && this.response.responseObj.datewisegetexercisedetails.length > 0) {
          for (let exerciseDtls of this.response.responseObj.datewisegetexercisedetails) {
            this.dateWiseExerciseDetailObj = exerciseDtls;
            console.log("this.dateWiseExerciseDetailObj:::::" + JSON.stringify(this.dateWiseExerciseDetailObj));
            if ((this.dateWiseExerciseDetailObj.exerciseParam1 != null && this.dateWiseExerciseDetailObj.exerciseParam1 != '') || (this.dateWiseExerciseDetailObj.exerciseParam2 != null && this.dateWiseExerciseDetailObj.exerciseParam2 != '') || (this.dateWiseExerciseDetailObj.exerciseParam3 != null && this.dateWiseExerciseDetailObj.exerciseParam3 != '')) {
              this.isPresentExerciseParam1 = true;
            }

            if (this.dateWiseExerciseDetailObj.exerciseImage != null) {
              this.exerciseImageUrl = "data:image/png;base64," + this.dateWiseExerciseDetailObj.exerciseImage;
            }


            //var youtubeId = this.dateWiseExerciseDetailObj.videoLink.split("=");
            this.completeVideoLink = this.dateWiseExerciseDetailObj.videoLink;
            //console.log(youtubeId[1]);
            this.youtubeId = this.dateWiseExerciseDetailObj.ytkey;

            //this.videoLink = this.initialVLink + this.youtubeId;
            this.imgLinkString = "https://img.youtube.com/vi/" + this.youtubeId + Constant.HighQualityImage;
            console.log("Image url link--->" + this.imgLinkString);
            //this.embedLink = this.dom.bypassSecurityTrustResourceUrl(this.videoLink);
            //this.defaultImageUrl = this.dom.bypassSecurityTrustResourceUrl(this.imageUrl);

            // this.loading = this.activityLoader.create({
            //   content: 'Please wait...'
            // });

            // this.loading.present();

          }
          this.isPresentExerciseData = true;
        }

        else {
          this.showAlert("Alert", "Exercise for a current date is not present.");
          this.isPresentExerciseData = false;

          //this.embedLink = "https://youtu.be/Z9bfWGb7uI0";
       

          //this.dismissLodaing();
        }
      }

      // else {
      //   this.showAlert("Alert", "Exercise for a current date is not present.");
      //   //this.dismissLodaing();
      // }
    })
  }

  OpenBrowser() {
    console.log('in the browser function');
    const options: InAppBrowserOptions = {
      zoom: 'no',
      //hidden: 'no',
      //clearcache: 'yes',
      //clearsessioncache: 'yes',
      //toolbar: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      closebuttoncaption: 'Done'


    }

    // Opening a URL and returning an InAppBrowserObject
    const browser = this.iab.create(this.completeVideoLink,'_blank', options);
    browser.show();
  }


  // handleIFrameLoadEvent(): void {
  //   this.loading.dismiss();
  // }
  goToExerciseDetailsNextPage(category) {
    this.navCtrl.push(MoreInfoAboutExerciseNextPage, { category: category });
  }

  //Activity Indicator
  presentLoading() {
    
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }


  // openExerciseVideo() {
  //   this.youtube.openVideo(this.youtubeId);
  // }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
