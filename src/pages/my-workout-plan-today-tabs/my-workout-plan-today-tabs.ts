import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyWorkoutPlanTodayPage } from '../my-workout-plan-today/my-workout-plan-today';
import { TodaySWorkoutNewPage} from '../today-s-workout-new/today-s-workout-new';
import { MyProgressPage } from '../my-progress/my-progress';
/**
 * Generated class for the MyWorkoutPlanTodayTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-workout-plan-today-tabs',
  templateUrl: 'my-workout-plan-today-tabs.html',
})
export class MyWorkoutPlanTodayTabsPage {

  myWorkoutPlanTodayPage = MyWorkoutPlanTodayPage
  todaySWorkoutPage = TodaySWorkoutNewPage
  myProgressPage = MyProgressPage

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyWorkoutPlanTodayTabsPage');
  }

}
