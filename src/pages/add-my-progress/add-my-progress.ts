import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController  } from 'ionic-angular';
// import { jsonpCallbackContext } from '@angular/common/http/src/module';

/**
 * Generated class for the AddMyProgressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-add-my-progress',
  templateUrl: 'add-my-progress.html',
})
export class AddMyProgressPage {

  progressCriteria: any [] =[];
  selectedProgCriteria: any [] = [];
  isChecked: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
    this.progressCriteria = [
      {id: 1, name: 'Weight'},
      {id: 2, name: 'Girth Measurements'},
      {id: 3, name: 'Water Percentage'},
      {id: 4, name: 'Fat Percentage'},
      {id: 5, name: 'BMI'},
      {id: 6, name: 'BMI Classification'},
      {id: 7, name: 'Lean Mass'},
      {id: 8, name: 'Resting Metabolism'},
      {id: 9, name: 'Average Actual Metabolism'},
      {id: 10, name: 'Girth Size'},

    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddMyProgressPage');
  }

  onCheck(selectedCriteria){
    //this.isChecked = true;
    console.log("selectedCriteria:::::"+JSON.stringify(selectedCriteria));
    this.selectedProgCriteria.push(selectedCriteria);
    this.selectedProgCriteria.forEach(sc => {
      console.log(JSON.stringify(sc));
    })
    
  }

  /*deleteCriteria(selectedCriteria){
    console.log("selectedCriteria to delete:::::"+JSON.stringify(selectedCriteria));
    //this.isChecked = false;
    
  }*/

  public closeModal(){
    this.viewCtrl.dismiss(this.selectedProgCriteria);
}

}
