import { Component, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common'
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Slides } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular';
// import { AddMyProgressPage } from '../../pages/add-my-progress/add-my-progress';
import { TrackProgressPage } from "../../pages/track-progress/track-progress";
import { CustomerAssessment } from '../../Model/customer-assessment';
import { Girth } from '../../Model/girth-details';
import { SkinFold } from '../../Model/skinFold-details';

import { CommonResponse } from '../../Model/common-response';
import { MyProgressProvider } from '../../providers/my-progress/my-progress';

/**
 * Generated class for the MyProgressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-progress',
  templateUrl: 'my-progress.html',
})
export class MyProgressPage {

  @ViewChild('swipedTabsSlider') swipedTabsSlider: Slides;
  today: Date;
  currentDate: any;

  //editMyProgressPage: FormGroup;
  selectedSegment: string = "0";
  selectedSegmentName: string = "pastAssessment";
  isGirthAccordionExpanded: boolean = false;
  isSkinFoldsAccordionExpanded: boolean = false;
  isOtherInfoAccordionExpanded: boolean = false;
  isSaveAddAssessment: boolean = false;
  isSavePastAssessment: boolean = true;
  //isSelectPastAssessment: boolean = false;

  username: string;
  customerId: any;
  response: CommonResponse;
  loader: any;
  dateList: any[] = [];


  //fields
  //For assessment
  assessId: number;
  assessDate: any;
  customerWeight: string;
  customerHieght: string;
  customerHieghtFt: string;
  customerHieghtIn: string;
  customerAge: string;
  customer_bp: string;
  customer_bp_upper: string;
  customer_bp_lower: string;

  //for girth
  customerNeck: string;
  customerNeckUnit: string;
  customerShoulder: string;
  customerShoulderUnit: string;
  customerChest: string;
  customerChestUnit: string;
  customerLeftArm: string;
  customerLeftArmUnit: string;
  customerRightArm: string;
  customerRightArmUnit: string;
  customerWaist: string;
  customerWaistUnit: string;
  customerHip: string;
  customerHipUnit: string;
  customerLeftThigh: string;
  customerLeftThighUnit: string;
  customerRightThigh: string;
  customerRightThighUnit: string;
  customerLeftCalf: string;
  customerLeftCalfUnit: string;
  customerRightCalf: string;
  customerRightCalfUnit: string;
  whr: string;
  whrUnit: string;
  total: number = 0;


  //for skin folds
  mmAbdominal: string;
  mmAbdominalUnit: string;
  mmTricep: string;
  mmTricepUnit: string;
  mmBicep: string;
  mmBicepUnit: string;
  mmChest: string;
  mmChestUnit: string;
  mmMidaxillary: string;
  mmMidaxillaryUnit: string;
  mmSubscapular: string;
  mmSubscapularUnit: string;
  mmSuoraillac: string;
  mmSuoraillacUnit: string;
  mmCalf: string;
  mmCalfUnit: string;
  mmThigh: string;
  mmThighUnit: string;
  mmSkinFoldTotal: number = 0;
  mmSkinFoldTotalUnit: string;
  mmSkinFoldAverage: number = 0;
  mmSkinFoldAverageUnit: string;

  //for other information
  bodyFatPercentage: string;
  waterPercentage: string;
  customerBmi: string;
  leanMass: string;
  customer_assessment_note: string;

  //for past assessment
  //fields
  //For assessment
  assessIdForPastAssessment: number;
  assessDateForPastAssessment: any;
  customerWeightForPastAssessment: string;
  customerHieghtForPastAssessment: string;
  customerHieghtForPastAssessmentFt: string;
  customerHieghtForPastAssessmentIn: string;
  customerAgeForPastAssessment: string;
  customer_bpForPastAssessment: string;
  customer_bp_upperForPastAssessment: string;
  customer_bp_lowerForPastAssessment: string;

  //for girth
  customerNeckForPastAssessment: string;
  customerNeckUnitForPastAssessment: string;
  customerShoulderForPastAssessment: string;
  customerShoulderUnitForPastAssessment: string;
  customerChestForPastAssessment: string;
  customerChestUnitForPastAssessment: string;
  customerLeftArmForPastAssessment: string;
  customerLeftArmUnitForPastAssessment: string;
  customerRightArmForPastAssessment: string;
  customerRightArmUnitForPastAssessment: string;
  customerWaistForPastAssessment: string;
  customerWaistUnitForPastAssessment: string;
  customerHipForPastAssessment: string;
  customerHipUnitForPastAssessment: string;
  customerLeftThighForPastAssessment: string;
  customerLeftThighUnitForPastAssessment: string;
  customerRightThighForPastAssessment: string;
  customerRightThighUnitForPastAssessment: string;
  customerLeftCalfForPastAssessment: string;
  customerLeftCalfUnitForPastAssessment: string;
  customerRightCalfForPastAssessment: string;
  customerRightCalfUnitForPastAssessment: string;
  whrForPastAssessment: string;
  whrUnitForPastAssessment: string;
  totalForPastAssessment: number = 0;

  //for skin folds
  mmAbdominalForPastAssessment: string;
  mmAbdominalUnitForPastAssessment: string;
  mmTricepForPastAssessment: string;
  mmTricepUnitForPastAssessment: string;
  mmBicepForPastAssessment: string;
  mmBicepUnitForPastAssessment: string;
  mmChestForPastAssessment: string;
  mmChestUnitForPastAssessment: string;
  mmMidaxillaryForPastAssessment: string;
  mmMidaxillaryUnitForPastAssessment: string;
  mmSubscapularForPastAssessment: string;
  mmSubscapularUnitForPastAssessment: string;
  mmSuoraillacForPastAssessment: string;
  mmSuoraillacUnitForPastAssessment: string;
  mmCalfForPastAssessment: string;
  mmCalfUnitForPastAssessment: string;
  mmThighForPastAssessment: string;
  mmThighUnitForPastAssessment: string;
  mmSkinFoldTotalForPastAssessment: number = 0;
  mmSkinFoldTotalUnitForPastAssessment: string;
  mmSkinFoldAverageForPastAssessment: number = 0;
  mmSkinFoldAverageUnitForPastAssessment: string;

  //for other information
  bodyFatPercentageForPastAssessment: string;
  waterPercentageForPastAssessment: string;
  customerBmiForPastAssessment: string;
  leanMassForPastAssessment: string;
  customer_assessment_noteForPastAssessment: string;

  selectedPassAssessmentDate: Date;

  //show/hide fields for add assessment
  isassessDatePresent: boolean = false;
  isCustomerWeightPresent: boolean = false;
  isCustomerHieghtPresent: boolean = false;
  isCustomerAgePresent: boolean = false;
  isCustomer_bpPresent: boolean = false;

  //for girth
  isCustomerNeckPresent: boolean = false;
  isCustomerShoulderPresent: boolean = false;;
  isCustomerChestPresent: boolean = false;;
  isCustomerLeftArmPresent: boolean = false;;
  isCustomerRightArmPresent: boolean = false;;
  isCustomerWaistPresent: boolean = false;;
  isCustomerHipPresent: boolean = false;;
  isCustomerLeftThighPresent: boolean = false;;
  isCustomerRightThighPresent: boolean = false;;
  isCustomerLeftCalfPresent: boolean = false;;
  isCustomerRightCalfPresent: boolean = false;;
  isWhrPresent: boolean = false;;

  //for skin folds
  ismmAbdominalPresent: boolean = false;;
  ismmTricepPresent: boolean = false;;
  ismmBicepPresent: boolean = false;;
  ismmChestPresent: boolean = false;;
  ismmMidaxillaryPresent: boolean = false;;
  ismmSubscapularPresent: boolean = false;;
  ismmSuoraillacPresent: boolean = false;;
  ismmCalfPresent: boolean = false;;
  ismmThighPresent: boolean = false;;
  ismmSkinFoldTotalPresent: boolean = false;;
  ismmSkinFoldAveragePresent: boolean = false;;

  //for other information
  isbodyFatPercentagePresent: boolean = false;;
  iswaterPercentagePresent: boolean = false;;
  iscustomerBmiPresent: boolean = false;;
  isleanMassPresent: boolean = false;
  is_customer_assessment_note_present: boolean = false;

  //show/hide fields for past assessment
  isassessDatePresentForPastAssessment: boolean = false;
  isCustomerWeightPresentForPastAssessment: boolean = false;
  isCustomerHieghtPresentForPastAssessment: boolean = false;
  isCustomerAgePresentForPastAssessment: boolean = false;
  isCustomer_bpPresentForPastAssessment: boolean = false;

  //for girth
  isCustomerNeckPresentForPastAssessment: boolean = false;
  isCustomerShoulderPresentForPastAssessment: boolean = false;;
  isCustomerChestPresentForPastAssessment: boolean = false;;
  isCustomerLeftArmPresentForPastAssessment: boolean = false;;
  isCustomerRightArmPresentForPastAssessment: boolean = false;;
  isCustomerWaistPresentForPastAssessment: boolean = false;;
  isCustomerHipPresentForPastAssessment: boolean = false;;
  isCustomerLeftThighPresentForPastAssessment: boolean = false;;
  isCustomerRightThighPresentForPastAssessment: boolean = false;;
  isCustomerLeftCalfPresentForPastAssessment: boolean = false;;
  isCustomerRightCalfPresentForPastAssessment: boolean = false;;
  isWhrPresentForPastAssessment: boolean = false;;

  //for skin folds
  ismmAbdominalPresentForPastAssessment: boolean = false;;
  ismmTricepPresentForPastAssessment: boolean = false;;
  ismmBicepPresentForPastAssessment: boolean = false;;
  ismmChestPresentForPastAssessment: boolean = false;;
  ismmMidaxillaryPresentForPastAssessment: boolean = false;;
  ismmSubscapularPresentForPastAssessment: boolean = false;;
  ismmSuoraillacPresentForPastAssessment: boolean = false;;
  ismmCalfPresentForPastAssessment: boolean = false;;
  ismmThighPresentForPastAssessment: boolean = false;;
  ismmSkinFoldTotalPresentForPastAssessment: boolean = false;;
  ismmSkinFoldAveragePresentForPastAssessment: boolean = false;;

  //for other information
  isbodyFatPercentagePresentForPastAssessment: boolean = false;;
  iswaterPercentagePresentForPastAssessment: boolean = false;;
  iscustomerBmiPresentForPastAssessment: boolean = false;;
  isleanMassPresentForPastAssessment: boolean = false;
  is_customer_assessment_note_presentForPastAssessment: boolean = false;


  customerAssessmentObj: CustomerAssessment;
  getcustomerassessment: CustomerAssessment[] = [];
  DateList: string[] = [];
  girthObj: Girth;
  skinFoldObj: SkinFold;
  isDisablePastAssessment: boolean = false;

  selectedDateForPastAssessment: any;
  isShownAddProgressBlankForm: boolean = false;

  //initially blank add assessment fields
  //For assessment
  addAssessDate: any;
  addCustomerWeight: string;
  addCustomerHieght: string;
  addCustomerAge: string;
  add_customer_bp_upper: string;
  add_customer_bp_lower: string;
  add_customer_height_ft: string;
  add_customer_height_in: string;

  //for girth
  addCustomerNeck: string;
  addCustomerChest: string;
  addCustomerShoulder: string;
  addCustomerLeftArm: string;
  addCustomerRightArm: string;
  addCustomerWaist: string;
  addCustomerHip: string;
  addCustomerLeftThigh: string;
  addCustomerRightThigh: string;
  addCustomerLeftCalf: string;
  addCustomerRightCalf: string;
  addWhr: string;
  totalForAddAssessment: number = 0;


  //for skin folds
  addmmAbdominal: string;
  addmmTricep: string;
  addmmBicep: string;
  addmmChest: string;
  addmmMidaxillary: string;
  addmmSubscapular: string;
  addmmSuoraillac: string;
  addmmThigh: string;
  addmmCalf: string;
  addmmSkinFoldTotal: number = 0;
  addmmSkinFoldAverage: number = 0;


  //for other information
  addbodyFatPercentage: string;
  addwaterPercentage: string;
  addcustomerBmi: string;
  addleanMass: string;
  add_customer_assessment_note: string;
  gender: string;

  //for calculation
  weightForAddAssessment: string;
  heightFtForAddAssessment: string;
  heightInForAddAssessment: string;
  heightForAddAssessment: string;
  ageForAddAssessment: string;

  weightForEditAssessment: string;
  heightFtForEditAssessment: string;
  heightInForEditAssessment: string;
  heightForEditAssessment: string;
  ageForEditAssessment: string;

  weightForPastAssessment: string;
  heightFtForPastAssessment: string;
  heightInForPastAssessment: string;
  heightForPastAssessment: string;
  ageForPastAssessment: string;

  heightConvertedInMetersForPastAssessment: number;
  heightConvertedInCentimetersForPastAssessment: number;
  heightConvertedInMetersForEditAssessment: number;
  heightConvertedInCentimetersForEditAssessment: number;
  heightConvertedInMetersForAddAssessment: number;
  heightConvertedInCentimetersForAddAssessment: number;

  totalForNewAssessment: string;
  totalForEditAssessment: string;
  totalForPastEditAssessment: string;
  addSkinFoldTotal: string;
  addSkinFoldAverage: string;
  editSkinFoldTotal: string;
  editSkinFoldAverage: string;
  pastEditSkinFoldTotal: string;
  pastEditSkinFoldAverage: string;
  isAddAssessment: boolean = false;
  deviceTypeName: string;
  isAndroidDevice: boolean = false;
  



  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    public alertCtrl: AlertController, public modalCtrl: ModalController, public datepipe: DatePipe,
    public myProgressProvider: MyProgressProvider, public activityLoader: LoadingController) {

      
      this.isAddAssessment = this.navParams.get('isAddAssessment');
      console.log("Is add assessment selected::::" + this.isAddAssessment);
      if(this.isAddAssessment === true){
        this.isSaveAddAssessment = true;
        this.isSavePastAssessment = false;
        this.isShownAddProgressBlankForm = true;
        this.selectedSegmentName = "addAssessment";
      }
      else if(this.isAddAssessment === false){
        this.isSaveAddAssessment = false;
        this.isSavePastAssessment = true;
        this.isShownAddProgressBlankForm = false;
        this.selectedSegmentName = "pastAssessment";
      }

    // if(this.selectedSegment === "0" && this.selectedSegmentName ==="addAssessment"){
    //   this.showAlert("Alert", "The data populated here is based on your recent assessment. Please update the required fields only.");
    // }

    this.today = new Date();
    console.log("DATEEEEE::::" + this.today);
    this.currentDate = this.datepipe.transform(this.today, 'yyyy-MM-dd');
    console.log("Changed date::::" + this.currentDate);
    this.username = localStorage.getItem('username');
    this.gender = localStorage.getItem('gender');
    console.log("this.gender in my-progress::::" + this.gender);
    this.deviceTypeName = localStorage.getItem('deviceTypeName');
    console.log("this.deviceTypeName in my-progress::::" + this.deviceTypeName);
    if(this.deviceTypeName === 'Android'){
      this.isAndroidDevice = true;
    }
    else{
      this.isAndroidDevice = false;
    }

    this.getPassAssessmentData(this.username);

    this.myProgressProvider.getPastAssessmentdateList(this.username).then(res => {
      this.dateList = [];
      this.response = res;
      //if (this.response.responseObj.getcustomerassessment.length > 0) {
      console.log("In constructor datessssss::::" + JSON.stringify(this.response));
      for (let dataList of this.response.responseObj.getcustomerassessment) {
        console.log("In function::::" + JSON.stringify(dataList));
        // this.dateList = dataList;
        this.dateList.push(dataList);
      }
      // }
      // else {
      //   this.showAlert("Alert", "Past assessments are not present.");
      // }

    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyProgressPage');
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
  }

  //Other information calculation
  getData(event, field) {
    //console.log("Value:::::"+JSON.stringify(event.target.value));
    if (this.isShownAddProgressBlankForm === true && this.selectedSegmentName === "addAssessment") {
      console.log("I'm in if");
      if (field === 'weight') {
        this.weightForAddAssessment = event.target.value;
        this.addCustomerWeight = this.weightForAddAssessment;
        console.log("this.addCustomerWeight value:::::" + this.addCustomerWeight);
        console.log("Weight value:::::" + this.weightForAddAssessment);
        console.log("field name:::::" + field);
      }
      if (field === 'heightFt') {
        this.heightFtForAddAssessment = event.target.value;
        this.add_customer_height_ft = this.heightFtForAddAssessment;
        console.log("Ft value:::::" + this.heightFtForAddAssessment);
        console.log("field name:::::" + field);
      }
      if (field === 'heightIn') {
        this.heightInForAddAssessment = event.target.value;
        this.add_customer_height_in = this.heightInForAddAssessment;
        console.log("In value:::::" + this.heightInForAddAssessment);
        console.log("field name:::::" + field);
      }

      if (field === 'age') {
        this.ageForAddAssessment = event.target.value;
        this.addCustomerAge = this.ageForAddAssessment;
        console.log("age value:::::" + this.ageForAddAssessment);
      }

      //for assessment calculation
      if ((this.heightFtForAddAssessment != null || this.heightFtForAddAssessment != '') && (this.heightInForAddAssessment != null || this.heightInForAddAssessment != '')) {
        this.heightConvertedInMetersForAddAssessment = ((Number(this.heightFtForAddAssessment) * 0.3048) + (Number(this.heightInForAddAssessment) * 0.0254));
        this.heightConvertedInCentimetersForAddAssessment = ((Number(this.heightFtForAddAssessment) * 30.48) + (Number(this.heightInForAddAssessment) * 2.54));
      }

      //BMI calculation
      if ((this.weightForAddAssessment != null || this.weightForAddAssessment != '') && ((this.heightFtForAddAssessment != null || this.heightFtForAddAssessment != '') && (this.heightInForAddAssessment != null || this.heightInForAddAssessment != ''))) {
        var BmiValue = Number(this.weightForAddAssessment) / (this.heightConvertedInMetersForAddAssessment * this.heightConvertedInMetersForAddAssessment);
        this.addcustomerBmi = String(BmiValue.toFixed(1))
        //this.customerAssessmentObj.customerBmi = this.addcustomerBmi;
      }

      //bodyfat calculation
      if ((this.ageForAddAssessment != null || this.ageForAddAssessment != '') && Number(this.ageForAddAssessment) > 0) {
        var bfp = -1;

        if (Number(this.ageForAddAssessment) < 18 && this.gender === 'Male') {
          let S = 1;
          bfp = (1.51 * Number(this.addcustomerBmi)) - (0.70 * Number(this.ageForAddAssessment)) - (3.6 * S) + 1.4;
        }
        else if (Number(this.ageForAddAssessment) < 18 && this.gender === 'Female') {
          let S = 0;
          bfp = (1.51 * Number(this.addcustomerBmi)) - (0.70 * Number(this.ageForAddAssessment)) - (3.6 * S) + 1.4;
        }
        else if (Number(this.ageForAddAssessment) > 18 && this.gender === 'Male') {
          let S = 1;
          bfp = (1.20 * Number(this.addcustomerBmi)) + (0.23 * Number(this.ageForAddAssessment)) - (10.8 * S) - 5.4;
          console.log(" bfp value for male::::::"+bfp);
        }
        else if (Number(this.ageForAddAssessment) > 18 && this.gender === 'Female') {
          let S = 0;
          bfp = (1.20 * Number(this.addcustomerBmi)) + (0.23 * Number(this.ageForAddAssessment)) - (10.8 * S) - 5.4;
        }

        this.addbodyFatPercentage = String(bfp.toFixed(1));
        //this.customerAssessmentObj.bodyFatPercentage = this.addbodyFatPercentage;

      }

      //Lean mass calculation
      if ((this.weightForAddAssessment != null || this.weightForAddAssessment != '')) {
        let lbm = -1;
        if (this.gender === 'Male') {
          lbm = (1.1 * Number(this.weightForAddAssessment)) - 128 * ((Number(this.weightForAddAssessment) * Number(this.weightForAddAssessment)) / (this.heightConvertedInCentimetersForAddAssessment * this.heightConvertedInCentimetersForAddAssessment));
        }
        else if (this.gender === 'Female') {
          lbm = (1.07 * Number(this.weightForAddAssessment)) - 148 * ((Number(this.weightForAddAssessment) * Number(this.weightForAddAssessment)) / (this.heightConvertedInCentimetersForAddAssessment * this.heightConvertedInCentimetersForAddAssessment));
        }
        this.addleanMass = String(lbm.toFixed(3));
        // this.customerAssessmentObj.leanMass = this.addleanMass;
      }

      //water percentage calculation
      if ((this.ageForAddAssessment != null || this.ageForAddAssessment != '') && (this.weightForAddAssessment != null || this.weightForAddAssessment != '')) {
        var watePercentageForAddAssessment = -1;
        if (this.gender === 'Male') {
          watePercentageForAddAssessment = 2.447 - (0.09156 * Number(this.ageForAddAssessment)) + (0.1074 * this.heightConvertedInCentimetersForAddAssessment) + (0.3362 * Number(this.weightForAddAssessment));
        } else {
          watePercentageForAddAssessment = -2.097 + (0.1069 * this.heightConvertedInCentimetersForAddAssessment) + (0.2466 * Number(this.weightForAddAssessment));
        }
        this.addwaterPercentage = String(watePercentageForAddAssessment.toFixed(1));
        // this.customerAssessmentObj.waterPercentage = this.addwaterPercentage;
      }

    }
    else if (this.isShownAddProgressBlankForm === false && this.selectedSegmentName === "addAssessment") {
      console.log("I'm in else-if");
      //console.log("this.isShownAddProgressBlankForm::::" + this.isShownAddProgressBlankForm);
      if (field === 'weight' && event.target.value != undefined) {
        this.weightForEditAssessment = event.target.value;
        this.customerWeight = this.weightForEditAssessment;
        console.log("Weight value:::::" + this.weightForEditAssessment);
        console.log("field name:::::" + field);
      }
      else {
        this.weightForEditAssessment = this.customerWeight;
      }

      if (field === 'heightFt' && event.target.value != undefined) {
        this.heightFtForEditAssessment = event.target.value;
        this.customerHieghtFt = this.heightFtForEditAssessment;
        console.log("Ft value:::::" + this.heightFtForEditAssessment);
        console.log("field name:::::" + field);
      }
      else {
        this.heightFtForEditAssessment = this.customerHieghtFt;
      }
      if (field === 'heightIn' && event.target.value != undefined) {
        this.heightInForEditAssessment = event.target.value;
        this.customerHieghtIn = this.heightInForEditAssessment;
        console.log("In value:::::" + this.heightInForEditAssessment);
        console.log("field name:::::" + field);
      }
      else {
        this.heightInForEditAssessment = this.customerHieghtIn;
      }

      if (field === 'age' && event.target.value != undefined) {
        this.ageForEditAssessment = event.target.value;
        this.customerAge = this.ageForEditAssessment;
        console.log("age value:::::" + this.ageForEditAssessment);
      }
      else {
        this.ageForEditAssessment = this.customerAge;
      }


      //for assessment calculation
      if ((this.heightFtForEditAssessment != null || this.heightFtForEditAssessment != '') && (this.heightInForEditAssessment != null || this.heightInForEditAssessment != '')) {
        this.heightConvertedInMetersForEditAssessment = ((Number(this.heightFtForEditAssessment) * 0.3048) + (Number(this.heightInForEditAssessment) * 0.0254));
        this.heightConvertedInCentimetersForEditAssessment = ((Number(this.heightFtForEditAssessment) * 30.48) + (Number(this.heightInForEditAssessment) * 2.54));
      }

      //BMI calculation
      if ((this.weightForEditAssessment != null || this.weightForEditAssessment != '') && (this.heightFtForEditAssessment != null || this.heightFtForEditAssessment != '') && (this.heightInForEditAssessment != null || this.heightInForEditAssessment != '')) {
        var BmiValueForUpdateAssessment = Number(this.weightForEditAssessment) / (this.heightConvertedInMetersForEditAssessment * this.heightConvertedInMetersForEditAssessment);
        this.customerBmi = String(BmiValueForUpdateAssessment.toFixed(1))
        // this.customerAssessmentObj.customerBmi = this.customerBmi;
      }

      //bodyfat calculation
      if ((this.ageForEditAssessment != null || this.ageForEditAssessment != '') && Number(this.ageForEditAssessment) > 0) {
        let bfpForUpdate = -1;

        if (Number(this.ageForEditAssessment) < 18 && this.gender === 'Male') {
          let S = 1;
          bfpForUpdate = (1.51 * Number(this.customerBmi)) - (0.70 * Number(this.ageForEditAssessment)) - (3.6 * S) + 1.4;
        }
        else if (Number(this.ageForEditAssessment) < 18 && this.gender === 'Female') {
          let S = 0;
          bfpForUpdate = (1.51 * Number(this.customerBmi)) - (0.70 * Number(this.ageForEditAssessment)) - (3.6 * S) + 1.4;
        }
        else if (Number(this.ageForEditAssessment) > 18 && this.gender === 'Male') {
          let S = 1;
          bfpForUpdate = (1.20 * Number(this.customerBmi)) + (0.23 * Number(this.ageForEditAssessment)) - (10.8 * S) - 5.4;
        }
        else if (Number(this.ageForEditAssessment) > 18 && this.gender === 'Female') {
          let S = 0;
          bfpForUpdate = (1.20 * Number(this.customerBmi)) + (0.23 * Number(this.ageForEditAssessment)) - (10.8 * S) - 5.4;
        }

        this.bodyFatPercentage = String(bfpForUpdate.toFixed(1));
        // this.customerAssessmentObj.bodyFatPercentage = this.bodyFatPercentage;

      }

      //Lean mass calculation
      if ((this.weightForEditAssessment != null || this.weightForEditAssessment != '')) {
        let lbmUpdate = -1;
        if (this.gender === 'Male') {
          lbmUpdate = (1.1 * Number(this.weightForEditAssessment)) - 128 * ((Number(this.weightForEditAssessment) * Number(this.weightForEditAssessment)) / (this.heightConvertedInCentimetersForEditAssessment * this.heightConvertedInCentimetersForEditAssessment));
        }
        else if (this.gender === 'Female') {
          lbmUpdate = (1.07 * Number(this.customerWeight)) - 148 * ((Number(this.customerWeight) * Number(this.customerWeight)) / (this.heightConvertedInCentimetersForEditAssessment * this.heightConvertedInCentimetersForEditAssessment));
        }
        this.leanMass = String(lbmUpdate.toFixed(3));
        // this.customerAssessmentObj.leanMass = this.leanMass;
      }

      //water percentage calculation
      if ((this.ageForEditAssessment != null || this.ageForEditAssessment != '') && (this.weightForEditAssessment != null || this.weightForEditAssessment != '')) {
        var watePercentageForUpdateAssessment = -1;
        if (this.gender === 'Male') {
          watePercentageForUpdateAssessment = 2.447 - (0.09156 * Number(this.ageForEditAssessment)) + (0.1074 * this.heightConvertedInCentimetersForEditAssessment) + (0.3362 * Number(this.weightForEditAssessment));
        } else {
          watePercentageForUpdateAssessment = -2.097 + (0.1069 * this.heightConvertedInCentimetersForEditAssessment) + (0.2466 * Number(this.weightForEditAssessment));
        }
        this.waterPercentage = String(watePercentageForUpdateAssessment.toFixed(1));
        // this.customerAssessmentObj.waterPercentage = this.waterPercentage;
      }
      //}


    }

    else if (this.selectedSegmentName === "pastAssessment") {
      console.log("I'm in else");

      if (field === 'weight' && event.target.value != undefined) {
        this.weightForPastAssessment = event.target.value;
        this.customerWeightForPastAssessment = this.weightForPastAssessment;
        console.log("Weight value:::::" + this.customerWeightForPastAssessment);
        console.log("field name:::::" + field);
      }
      else {
        this.weightForPastAssessment = this.customerWeightForPastAssessment;
      }

      if (field === 'heightFt' && event.target.value != undefined) {
        this.heightFtForPastAssessment = event.target.value;
        this.customerHieghtForPastAssessmentFt = this.heightFtForPastAssessment;
        console.log("Ft value:::::" + this.heightFtForPastAssessment);
        console.log("field name:::::" + field);
      }
      else {
        this.heightFtForPastAssessment = this.customerHieghtForPastAssessmentFt;
      }
      if (field === 'heightIn' && event.target.value != undefined) {
        this.heightInForPastAssessment = event.target.value;
        this.customerHieghtForPastAssessmentIn = this.heightInForPastAssessment;
        console.log("In value:::::" + this.heightInForPastAssessment);
        console.log("field name:::::" + field);
      }
      else {
        this.heightInForPastAssessment = this.customerHieghtForPastAssessmentIn;
      }

      if (field === 'age' && event.target.value != undefined) {
        this.ageForPastAssessment = event.target.value;
        this.customerAgeForPastAssessment = this.ageForPastAssessment;
        console.log("age value:::::" + this.ageForPastAssessment);
      }
      else {
        this.ageForPastAssessment = this.customerAgeForPastAssessment;
      }

      //for assessment calculation
      if ((this.heightFtForPastAssessment != null || this.heightFtForPastAssessment != '') && (this.heightInForPastAssessment != null || this.heightInForPastAssessment != '')) {
        this.heightConvertedInMetersForPastAssessment = ((Number(this.heightFtForPastAssessment) * 0.3048) + (Number(this.heightInForPastAssessment) * 0.0254));
        this.heightConvertedInCentimetersForPastAssessment = ((Number(this.heightFtForPastAssessment) * 30.48) + (Number(this.heightInForPastAssessment) * 2.54));
      }

      //BMI calculation
      if ((this.weightForPastAssessment != null || this.weightForPastAssessment != '') && (this.heightFtForPastAssessment != null || this.heightFtForPastAssessment != '') && (this.heightInForPastAssessment != null || this.heightInForPastAssessment != '')) {
        var BmiValueForUpdatePastAssessment = Number(this.weightForPastAssessment) / (this.heightConvertedInMetersForPastAssessment * this.heightConvertedInMetersForPastAssessment);
        this.customerBmiForPastAssessment = String(BmiValueForUpdatePastAssessment.toFixed(1));
        console.log("this.customerBmiForPastAssessment:::::" + this.customerBmiForPastAssessment);
        // this.customerAssessmentObj.customerBmi = this.customerBmiForPastAssessment;
      }

      //bodyfat calculation
      if ((this.ageForPastAssessment != null || this.ageForPastAssessment != '') && Number(this.ageForPastAssessment) > 0) {
        let bfpForPastAssessment = -1;
        console.log("this.ageForPastAssessment:::::" + this.ageForPastAssessment);
        if (Number(this.ageForPastAssessment) < 18 && this.gender === 'Male') {
          let S = 1;
          bfpForPastAssessment = (1.51 * Number(this.customerBmiForPastAssessment)) - (0.70 * Number(this.ageForPastAssessment)) - (3.6 * S) + 1.4;
        }
        else if (Number(this.ageForPastAssessment) < 18 && this.gender === 'Female') {
          let S = 0;
          bfpForPastAssessment = (1.51 * Number(this.customerBmiForPastAssessment)) - (0.70 * Number(this.ageForPastAssessment)) - (3.6 * S) + 1.4;
        }
        else if ((Number(this.ageForPastAssessment) > 18)) {
          let S = 1;
          bfpForPastAssessment = (1.20 * Number(this.customerBmiForPastAssessment)) + (0.23 * Number(this.ageForPastAssessment)) - (10.8 * S) - 5.4;
          console.log("bfpForPastAssessment:::::" + bfpForPastAssessment);
         
        }
        else if (Number(this.ageForPastAssessment) > 18 && this.gender === 'Female') {
          let S = 0;
          bfpForPastAssessment = (1.20 * Number(this.customerBmiForPastAssessment)) + (0.23 * Number(this.ageForPastAssessment)) - (10.8 * S) - 5.4;
        }

        this.bodyFatPercentageForPastAssessment = String(bfpForPastAssessment.toFixed(1));
        console.log("this.bodyFatPercentageForPastAssessment:::::" + this.bodyFatPercentageForPastAssessment);
        // this.customerAssessmentObj.bodyFatPercentage = this.bodyFatPercentageForPastAssessment;

      }

      //Lean mass calculation
      if ((this.weightForPastAssessment != null || this.weightForPastAssessment != '')) {
        let lbmPastUpdate = -1;
        if (this.gender === 'Male') {
          lbmPastUpdate = (1.1 * Number(this.weightForPastAssessment)) - 128 * ((Number(this.weightForPastAssessment) * Number(this.weightForPastAssessment)) / (this.heightConvertedInCentimetersForPastAssessment * this.heightConvertedInCentimetersForPastAssessment));
          console.log("lbmPastUpdate:::::" + lbmPastUpdate);
        }
        else if (this.gender === 'Female') {
          lbmPastUpdate = (1.07 * Number(this.weightForPastAssessment)) - 148 * ((Number(this.weightForPastAssessment) * Number(this.weightForPastAssessment)) / (this.heightConvertedInCentimetersForPastAssessment * this.heightConvertedInCentimetersForPastAssessment));
        }
        this.leanMassForPastAssessment = String(lbmPastUpdate.toFixed(3));
        // this.customerAssessmentObj.leanMass = this.leanMassForPastAssessment;
        console.log("this.leanMassForPastAssessment:::::" + this.leanMassForPastAssessment);
      }

      //water percentage calculation
      if ((this.ageForPastAssessment != null || this.ageForPastAssessment != '') && (this.weightForPastAssessment != null || this.weightForPastAssessment != '')) {
        var watePercentageForPastAssessment = -1;
        if (this.gender === 'Male') {
          watePercentageForPastAssessment = 2.447 - (0.09156 * Number(this.ageForPastAssessment)) + (0.1074 * this.heightConvertedInCentimetersForPastAssessment) + (0.3362 * Number(this.weightForPastAssessment));
        } else {
          watePercentageForPastAssessment = -2.097 + (0.1069 * this.heightConvertedInCentimetersForPastAssessment) + (0.2466 * Number(this.weightForPastAssessment));
        }
        this.waterPercentageForPastAssessment = String(watePercentageForPastAssessment.toFixed(1));
        // this.customerAssessmentObj.waterPercentage = this.waterPercentageForPastAssessment;
        console.log("this.waterPercentageForPastAssessment:::::" + this.waterPercentageForPastAssessment);
      }

    }

  }


  //Girth total claculation

  getGirthData(event, field) {
    if (this.isShownAddProgressBlankForm === true && this.selectedSegmentName === "addAssessment") {
    
      if (field === 'neck') {
        this.totalForAddAssessment += Number(event.target.value);

      }
      if (field === 'shoulder') {
        this.totalForAddAssessment += Number(event.target.value);
      }
      if (field === 'chest') {
        this.totalForAddAssessment += Number(event.target.value);
      }

      if (field === 'leftArm') {
        this.totalForAddAssessment += Number(event.target.value);
      }
      if (field === 'rightArm') {
        this.totalForAddAssessment += Number(event.target.value);

      }
      if (field === 'waist') {
        this.totalForAddAssessment += Number(event.target.value);
      }
      if (field === 'hip') {
        this.totalForAddAssessment += Number(event.target.value);
      }

      if (field === 'leftThigh') {
        this.totalForAddAssessment += Number(event.target.value);
      }
      if (field === 'rightThigh') {
        this.totalForAddAssessment += Number(event.target.value);
      }
      if (field === 'leftCalf') {
        this.totalForAddAssessment += Number(event.target.value);
      }

      if (field === 'rightCalf') {
        this.totalForAddAssessment += Number(event.target.value);
      }

      if(this.isCustomerWaistPresent === true && this.isCustomerHipPresent === true){
        this.addWhr = (Number(this.addCustomerWaist)/Number(this.addCustomerHip)).toFixed(2);
      }

      this.totalForAddAssessment  = this.totalForAddAssessment;
      console.log("totalForAddAssessment::::::"+this.totalForAddAssessment.toFixed(3));
      this.totalForNewAssessment = this.totalForAddAssessment.toFixed(3);
    }
    else if (this.isShownAddProgressBlankForm === false && this.selectedSegmentName === "addAssessment") {
      let neck = 0;
      let sh = 0;
      let chest = 0;
      let leftArm = 0;
      let rightArm = 0;
      let waist = 0;
      let hip = 0;
      let leftThigh = 0;
      let rightThigh = 0;
      let leftCalf = 0;
      let rightCalf = 0;

      if (this.isCustomerNeckPresent) {
        neck = Number(this.customerNeck);
      }
      if (this.isCustomerShoulderPresent){
        sh = Number(this.customerShoulder);
      }
      if (this.isCustomerChestPresent) {
        chest = Number(this.customerChest);
      }
      if (this.isCustomerLeftArmPresent){
        leftArm = Number(this.customerLeftArm);
      }
      if (this.isCustomerRightArmPresent) {
        rightArm = Number(this.customerRightArm);
      }
      if (this.isCustomerWaistPresent) {
        waist = Number(this.customerWaist);
      }
      if (this.isCustomerHipPresent) {
        hip = Number(this.customerHip);
      }
      if (this.isCustomerLeftThighPresent) {
        leftThigh = Number(this.customerLeftThigh);
      }
      if (this.isCustomerRightThighPresent) {
        rightThigh = Number(this.customerRightThigh);
      }
      if (this.isCustomerLeftCalfPresent) {
        leftCalf = Number(this.customerLeftCalf);
      }
      if (this.isCustomerRightCalfPresent) {
        rightCalf = Number(this.customerRightCalf);
      }

      if(this.isCustomerWaistPresent === true && this.isCustomerHipPresent === true){
        this.whr = (Number(this.customerWaist)/Number(this.customerHip)).toFixed(2);
      }

      let total = neck + sh + chest + leftArm + rightArm + waist + hip + leftThigh + rightThigh + leftCalf + rightCalf;
      this.total = this.total;
      this.totalForEditAssessment = total.toFixed(3);

      console.log("this.total::::" + this.total);
    

    }
    else if (this.selectedSegmentName === "pastAssessment") {

      let neck = 0;
      let sh = 0;
      let chest = 0;
      let leftArm = 0;
      let rightArm = 0;
      let waist = 0;
      let hip = 0;
      let leftThigh = 0;
      let rightThigh = 0;
      let leftCalf = 0;
      let rightCalf = 0;

      if (this.isCustomerNeckPresentForPastAssessment) {
        neck = Number(this.customerNeckForPastAssessment);
      }
      if (this.isCustomerShoulderPresentForPastAssessment){
        sh = Number(this.customerShoulderForPastAssessment);
      }
      if (this.isCustomerChestPresentForPastAssessment) {
        chest = Number(this.customerChestForPastAssessment);
      }
      if (this.isCustomerLeftArmPresentForPastAssessment){
        leftArm = Number(this.customerLeftArmForPastAssessment);
      }
      if (this.isCustomerRightArmPresentForPastAssessment) {
        rightArm = Number(this.customerRightArmForPastAssessment);
      }
      if (this.isCustomerWaistPresentForPastAssessment) {
        waist = Number(this.customerWaistForPastAssessment);
      }
      if (this.isCustomerHipPresentForPastAssessment) {
        hip = Number(this.customerHipForPastAssessment);
      }
      if (this.isCustomerLeftThighPresentForPastAssessment) {
        leftThigh = Number(this.customerLeftThighForPastAssessment);
      }
      if (this.isCustomerRightThighPresentForPastAssessment) {
        rightThigh = Number(this.customerRightThighForPastAssessment);
      }
      if (this.isCustomerLeftCalfPresentForPastAssessment) {
        leftCalf = Number(this.customerLeftCalfForPastAssessment);
      }
      if (this.isCustomerRightCalfPresentForPastAssessment) {
        rightCalf = Number(this.customerRightCalfForPastAssessment);
      }
      if(this.isCustomerWaistPresentForPastAssessment === true && this.isCustomerHipPresentForPastAssessment === true){
        this.whrForPastAssessment = (Number(this.customerWaistForPastAssessment)/Number(this.customerHipForPastAssessment)).toFixed(2);
      }

      let totalForPastAssessment = neck + sh + chest + leftArm + rightArm + waist + hip + leftThigh + rightThigh + leftCalf + rightCalf;
      this.totalForPastAssessment = totalForPastAssessment;
      this.totalForPastEditAssessment = totalForPastAssessment.toFixed(3);

      console.log("this.totalForPastAssessment::::" + this.totalForPastAssessment);
    


    }
  }








  //Skinfolds calculation

  getSkinFoldData(event, field) {
    if (this.isShownAddProgressBlankForm === true && this.selectedSegmentName === "addAssessment") {

      if (field === 'abdominal') {
        this.addmmSkinFoldTotal += Number(event.target.value);
        //this.addmmSkinFoldAverage = String(event.target.value/9.0);

      }
      if (field === 'tricep') {
        this.addmmSkinFoldTotal += Number(event.target.value);
      }
      if (field === 'bicep') {
        this.addmmSkinFoldTotal += Number(event.target.value);
      }

      if (field === 'mmchest') {
        this.addmmSkinFoldTotal += Number(event.target.value);
      }
      if (field === 'mmMidaxillary') {
        this.addmmSkinFoldTotal += Number(event.target.value);

      }
      if (field === 'mmSubscapular') {
        this.addmmSkinFoldTotal += Number(event.target.value);
      }
      if (field === 'mmSuoraillac') {
        this.addmmSkinFoldTotal += Number(event.target.value);
      }

      if (field === 'mmThigh') {
        this.addmmSkinFoldTotal += Number(event.target.value);
      }
      if (field === 'mmCalf') {
        this.addmmSkinFoldTotal += Number(event.target.value);
      }

      this.addmmSkinFoldAverage = this.addmmSkinFoldTotal / 9.0;
      this.addSkinFoldTotal = this.addmmSkinFoldTotal.toFixed(3);
      this.addSkinFoldAverage = this.addmmSkinFoldAverage.toFixed(3);

    }
    else if (this.isShownAddProgressBlankForm === false && this.selectedSegmentName === "addAssessment") {

      let abdominal = 0;
      let tricep = 0;
      let bicep = 0;
      let mmchest = 0;
      let mmMidaxillary = 0;
      let mmSubscapular = 0;
      let mmSuoraillac = 0;
      let mmThigh = 0;
      let mmCalf = 0;
      

      if (this.ismmAbdominalPresent) {
        abdominal = Number(this.mmAbdominal);
      }
      if (this.ismmTricepPresent){
        tricep = Number(this.mmTricep);
      }
      if (this.ismmBicepPresent) {
        bicep = Number(this.mmBicep);
      }
      if (this.ismmChestPresent){
        mmchest = Number(this.mmChest);
      }
      if (this.ismmMidaxillaryPresent) {
        mmMidaxillary = Number(this.mmMidaxillary);
      }
      if (this.ismmSubscapularPresent) {
        mmSubscapular = Number(this.mmSubscapular);
      }
      if (this.ismmSuoraillacPresent) {
        mmSuoraillac = Number(this.mmSuoraillac);
      }
      if (this.ismmThighPresent) {
        mmThigh = Number(this.mmThigh);
      }
      if (this.ismmCalfPresent) {
        mmCalf = Number(this.mmCalf);
      }
     

      let mmSkinFoldTotal = abdominal + tricep + bicep + mmchest + mmMidaxillary + mmSubscapular + mmSuoraillac + mmThigh + mmCalf;
      this.mmSkinFoldTotal = mmSkinFoldTotal;
      this.mmSkinFoldAverage = this.mmSkinFoldTotal / 9.0;
      this.editSkinFoldTotal = this.mmSkinFoldTotal.toFixed(3);
      this.editSkinFoldAverage = this.mmSkinFoldAverage.toFixed(3);


    }
    else if (this.selectedSegmentName === "pastAssessment") {
      let abdominal = 0;
      let tricep = 0;
      let bicep = 0;
      let mmchest = 0;
      let mmMidaxillary = 0;
      let mmSubscapular = 0;
      let mmSuoraillac = 0;
      let mmThigh = 0;
      let mmCalf = 0;
      

      if (this.ismmAbdominalPresentForPastAssessment) {
        abdominal = Number(this.mmAbdominalForPastAssessment);
      }
      if (this.ismmTricepPresentForPastAssessment){
        tricep = Number(this.mmTricepForPastAssessment);
      }
      if (this.ismmBicepPresentForPastAssessment) {
        bicep = Number(this.mmBicepForPastAssessment);
      }
      if (this.ismmChestPresentForPastAssessment){
        mmchest = Number(this.mmChestForPastAssessment);
      }
      if (this.ismmMidaxillaryPresentForPastAssessment) {
        mmMidaxillary = Number(this.mmMidaxillaryForPastAssessment);
      }
      if (this.ismmSubscapularPresentForPastAssessment) {
        mmSubscapular = Number(this.mmSubscapularForPastAssessment);
      }
      if (this.ismmSuoraillacPresentForPastAssessment) {
        mmSuoraillac = Number(this.mmSuoraillacForPastAssessment);
      }
      if (this.ismmThighPresentForPastAssessment) {
        mmThigh = Number(this.mmThighForPastAssessment);
      }
      if (this.ismmCalfPresentForPastAssessment) {
        mmCalf = Number(this.mmCalfForPastAssessment);
      }
     

      this.mmSkinFoldTotalForPastAssessment = abdominal + tricep + bicep + mmchest + mmMidaxillary + mmSubscapular + mmSuoraillac + mmThigh + mmCalf;
      this.mmSkinFoldAverageForPastAssessment = this.mmSkinFoldTotalForPastAssessment / 9.0;
      this.pastEditSkinFoldTotal = this.mmSkinFoldTotalForPastAssessment.toFixed(3);
      this.pastEditSkinFoldAverage = this.mmSkinFoldAverageForPastAssessment.toFixed(3);
    }
  }





  toViewLastAssessment() {
    //this.showAlert("Alert", "The data populated here is based on your recent assessment. Please update the required fields only.");
    this.getPassAssessmentData(this.username);
    this.isShownAddProgressBlankForm = false
  }


  //swiped tabs
  selectTab(index) {
    this.swipedTabsSlider.slideTo(index, 500);
    console.log("selectedSEgment in selectTab() is::::" + this.selectedSegment);
    console.log("Index is::::" + index);
    console.log("type of index:::::" + typeof index);
    if(this.isAddAssessment === true){
      if (index === 0) {
        //this.showAlert("Alert", "The data populated here is based on your recent assessment. Please update the required fields only.");
        this.selectedSegmentName = "addAssessment";
        //this.selectedSegmentName = "pastAssessment";
        // this.isSaveAddAssessment = false;
        // this.isSavePastAssessment = true;
        this.isSaveAddAssessment = true;
        this.isSavePastAssessment = false;
        this.isShownAddProgressBlankForm = true;
      }
      else if (index === 1) {
        // this.selectedSegmentName = "addAssessment";
        // this.isSaveAddAssessment = true;
        // this.isSavePastAssessment = false;
        this.selectedSegmentName = "pastAssessment";
        this.isSaveAddAssessment = false;
        this.isSavePastAssessment = true;
        this.isShownAddProgressBlankForm = false;
      }
    }
    else if(this.isAddAssessment === false || this.isAddAssessment === undefined){
    if (index === 0) {
      //this.showAlert("Alert", "The data populated here is based on your recent assessment. Please update the required fields only.");
      //this.selectedSegmentName = "addAssessment";
      this.selectedSegmentName = "pastAssessment";
      this.isSaveAddAssessment = false;
      this.isSavePastAssessment = true;
      this.isShownAddProgressBlankForm = false;
      //this.isSaveAddAssessment = true;
      //this.isSavePastAssessment = false;
    }
    else if (index === 1) {
      this.selectedSegmentName = "addAssessment";
      this.isSaveAddAssessment = true;
      this.isSavePastAssessment = false;
      this.isShownAddProgressBlankForm = true;
      // this.selectedSegmentName = "pastAssessment";
      // this.isSaveAddAssessment = false;
      // this.isSavePastAssessment = true;
    }
  }
}

  moveButton($event) {
    this.selectedSegment = $event._snapIndex.toString();

    console.log("I'm in movebutton(), selected segment is:::::" + this.selectedSegment);
    console.log("I'm in movebutton(), type of selected segment is:::::" + typeof this.selectedSegment);
    console.log("######" + this.selectedSegment);
    console.log("************"+this.isShownAddProgressBlankForm);
    if(this.isAddAssessment === true){
      if (this.selectedSegment === "0") {
        // this.selectedSegmentName = "pastAssessment";
        // this.isSaveAddAssessment = false;
        // this.isSavePastAssessment = true;
        this.selectedSegmentName = "addAssessment";
        this.isSaveAddAssessment = true;
        this.isSavePastAssessment = false;
        this.isShownAddProgressBlankForm = true;
        console.log("************######@@@@"+this.isShownAddProgressBlankForm);
        this.getPassAssessmentData(this.username);
      }
      else if (this.selectedSegment === "1") {
        this.selectedSegmentName = "pastAssessment";
        this.isSaveAddAssessment = false;
        this.isSavePastAssessment = true;
        console.log("************######%%%%%"+this.isShownAddProgressBlankForm);
        this.isShownAddProgressBlankForm = false;
      }
    }
    else if(this.isAddAssessment === false || this.isAddAssessment === undefined){
      if (this.selectedSegment === "0") {
        this.selectedSegmentName = "pastAssessment";
        this.isSaveAddAssessment = false;
        this.isSavePastAssessment = true;
        this.isShownAddProgressBlankForm = false;
        console.log("************######"+this.isShownAddProgressBlankForm);
        // this.selectedSegmentName = "addAssessment";
        // this.isSaveAddAssessment = true;
        // this.isSavePastAssessment = false;
        
      }
      else if (this.selectedSegment === "1") {
        this.selectedSegmentName = "addAssessment";
        this.isSaveAddAssessment = true;
        this.isSavePastAssessment = false;
        this.isShownAddProgressBlankForm = true;
        console.log("************@@@@@@@"+this.isShownAddProgressBlankForm);
        this.getPassAssessmentData(this.username);
      }
    }
    
  }

  //get selected date
  selectedDate(sDate) {
    console.log("sDate::::" + sDate.date);
    this.selectedDateForPastAssessment = sDate.date;

    this.getPassAssessmentDataForSelectedDate(this.username, sDate.date);

  }

  //get past assessment data for selected date
  getPassAssessmentDataForSelectedDate(username, sDate) {
    //this.presentLoading();
    //this.totalForPastAssessment = 0;
    this.mmSkinFoldTotalForPastAssessment = 0;
    this.mmSkinFoldAverageForPastAssessment = 0;
    this.getcustomerassessment = [];

    this.customerWeightForPastAssessment = "";
    this.customerHieghtForPastAssessment = "";
    this.customerHieghtForPastAssessmentFt = "";
    this.customerHieghtForPastAssessmentIn = "";
    this.customerAgeForPastAssessment = "";
    this.customer_bpForPastAssessment = "";
    this.customer_bp_upperForPastAssessment = "";
    this.customer_bp_lowerForPastAssessment = "";

    //for girth
    this.customerNeckForPastAssessment = "";
    this.customerNeckUnitForPastAssessment = "";
    this.customerShoulderForPastAssessment = "";
    this.customerShoulderUnitForPastAssessment = "";
    this.customerChestForPastAssessment = "";
    this.customerChestUnitForPastAssessment = "";
    this.customerLeftArmForPastAssessment = "";
    this.customerLeftArmUnitForPastAssessment = "";
    this.customerRightArmForPastAssessment = "";
    this.customerRightArmUnitForPastAssessment = "";
    this.customerWaistForPastAssessment = "";
    this.customerWaistUnitForPastAssessment = "";
    this.customerHipForPastAssessment = "";
    this.customerHipUnitForPastAssessment = "";
    this.customerLeftThighForPastAssessment = "";
    this.customerLeftThighUnitForPastAssessment = "";
    this.customerRightThighForPastAssessment = "";
    this.customerRightThighUnitForPastAssessment = "";
    this.customerLeftCalfForPastAssessment = "";
    this.customerLeftCalfUnitForPastAssessment = "";
    this.customerRightCalfForPastAssessment = "";
    this.customerRightCalfUnitForPastAssessment = "";
    this.whrForPastAssessment = "";
    this.whrUnitForPastAssessment = "";
    this.totalForPastAssessment = 0;
    this.totalForPastEditAssessment = "";

    //for skin folds
    this.mmAbdominalForPastAssessment = "";
    this.mmAbdominalUnitForPastAssessment = "";
    this.mmTricepForPastAssessment = "";
    this.mmTricepUnitForPastAssessment = "";
    this.mmBicepForPastAssessment = "";
    this.mmBicepUnitForPastAssessment = "";
    this.mmChestForPastAssessment = "";
    this.mmChestUnitForPastAssessment = "";
    this.mmMidaxillaryForPastAssessment = "";
    this.mmMidaxillaryUnitForPastAssessment = "";
    this.mmSubscapularForPastAssessment = "";
    this.mmSubscapularUnitForPastAssessment = "";
    this.mmSuoraillacForPastAssessment = "";
    this.mmSuoraillacUnitForPastAssessment = "";
    this.mmCalfForPastAssessment = "";
    this.mmCalfUnitForPastAssessment = "";
    this.mmThighForPastAssessment = "";
    this.mmThighUnitForPastAssessment = "";
    this.mmSkinFoldTotalForPastAssessment = 0;
    this.mmSkinFoldTotalUnitForPastAssessment = "";
    this.mmSkinFoldAverageForPastAssessment = 0;
    this.mmSkinFoldAverageUnitForPastAssessment = "";

    //for other information
    this.bodyFatPercentageForPastAssessment = "";
    this.waterPercentageForPastAssessment = "";
    this.customerBmiForPastAssessment = "";
    this.leanMassForPastAssessment = "";
    this.customer_assessment_noteForPastAssessment = "";


    this.myProgressProvider.getLastAssessmentForSelectedDate(username, sDate).then(res => {
      this.response = res;
      console.log("past assessment data for selected data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200) {
        this.getcustomerassessment = this.response.responseObj.getcustomerassessment;

        for (let assessmentData of this.getcustomerassessment) {

          this.assessIdForPastAssessment = assessmentData.assessId;

          if (assessmentData.customerId !== undefined) {
            localStorage.setItem('customerId', String(assessmentData.customerId));
          }

          this.customerId = localStorage.getItem('customerId');
          console.log("Customer Id in past assessment is:::::" + this.customerId);

          if (assessmentData.assessDate != null && assessmentData.assessDate != "") {
            this.assessDateForPastAssessment = assessmentData.assessDate;
            this.isassessDatePresentForPastAssessment = true;
            // let newDate = new Date(assessmentData.assessDate);
            // this.assessDate = newDate;
          }

          if (assessmentData.customerWeight != null && assessmentData.customerWeight != "") {
            let weight = assessmentData.customerWeight.split(" ");
            this.customerWeightForPastAssessment = weight[0];
            this.isCustomerWeightPresentForPastAssessment = true;
          }

          if (assessmentData.customerHieght != null && assessmentData.customerHieght != "") {
            //this.customerHieghtForPastAssessment = assessmentData.customerHieght;
            let customerHieghtForPastAssessment = assessmentData.customerHieght.split(".");
            if (customerHieghtForPastAssessment[0] != "") {
              this.customerHieghtForPastAssessmentFt = customerHieghtForPastAssessment[0];
            }
            if (customerHieghtForPastAssessment[1] != "") {
              this.customerHieghtForPastAssessmentIn = customerHieghtForPastAssessment[1];
            }
            this.isCustomerHieghtPresentForPastAssessment = true;
          }

          if (assessmentData.customerAge != null && assessmentData.customerAge != "") {
            this.customerAgeForPastAssessment = assessmentData.customerAge;
            this.isCustomerAgePresentForPastAssessment = true;
          }

          if (assessmentData.customer_bp != null && assessmentData.customer_bp != "") {
            let customer_bpForPastAssessment = assessmentData.customer_bp.split("/");
            if (customer_bpForPastAssessment[0] != "") {
              this.customer_bp_upperForPastAssessment = customer_bpForPastAssessment[0];
            }
            if (customer_bpForPastAssessment[1] != "") {
              this.customer_bp_lowerForPastAssessment = customer_bpForPastAssessment[1];
            }
            //this.isCustomer_bpPresent = true;

            //this.customer_bpForPastAssessment = assessmentData.customer_bp;
            this.isCustomer_bpPresentForPastAssessment = true;
          }



          //for girth

          if (assessmentData.girth.customerNeck != null && assessmentData.girth.customerNeck != "") {

            let neck = assessmentData.girth.customerNeck.split(" ");
            console.log("neck[0]****" + neck[0]);
            if (neck[0] != "" && neck[0] != 0) {
              this.customerNeckForPastAssessment = neck[0];
              this.isCustomerNeckPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerNeckForPastAssessment);
            }
            console.log("this.customerNeckForPastAssessment::::" + this.customerNeckForPastAssessment);
            console.log("this.customerNeckForPastAssessment22222::::" + assessmentData.girth.customerNeck);
            this.customerNeckUnitForPastAssessment = neck[1];

          }

          if (assessmentData.girth.customerShoulder != null && assessmentData.girth.customerShoulder != "") {
            let shoulder = assessmentData.girth.customerShoulder.split(" ");
            if (shoulder[0] != "" && shoulder[0] != 0) {
              this.customerShoulderForPastAssessment = shoulder[0];
              this.isCustomerShoulderPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerShoulderForPastAssessment);
            }

            this.customerShoulderUnitForPastAssessment = shoulder[1];
            this.customerShoulder = assessmentData.girth.customerShoulder;

          }

          if (assessmentData.girth.customerChest != null && assessmentData.girth.customerChest != "") {
            let chest = assessmentData.girth.customerChest.split(" ");
            if (chest[0] != "" && chest[0] != 0) {
              this.customerChestForPastAssessment = chest[0];
              this.isCustomerChestPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerChestForPastAssessment);
            }

            this.customerChestUnitForPastAssessment = chest[1];
            //this.customerChest = assessmentData.girth.customerChest;

          }

          if (assessmentData.girth.customerLeftArm != null && assessmentData.girth.customerLeftArm != "") {
            let leftArm = assessmentData.girth.customerLeftArm.split(" ");
            if (leftArm[0] != "" && leftArm[0] != 0) {
              this.customerLeftArmForPastAssessment = leftArm[0];
              this.isCustomerLeftArmPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerLeftArmForPastAssessment);
            }

            this.customerLeftArmUnitForPastAssessment = leftArm[1];
            //this.customerLeftArm = assessmentData.girth.customerLeftArm;

          }

          if (assessmentData.girth.customerRightArm != null && assessmentData.girth.customerRightArm != "") {
            let rightArm = assessmentData.girth.customerRightArm.split(" ");
            if (rightArm[0] != "" && rightArm[0] != 0) {
              this.customerRightArmForPastAssessment = rightArm[0];
              this.isCustomerRightArmPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerRightArmForPastAssessment);
            }

            this.customerRightArmUnitForPastAssessment = rightArm[1];
            //this.customerRightArm = assessmentData.girth.customerRightArm;

          }

          if (assessmentData.girth.customerWaist != null && assessmentData.girth.customerWaist != "") {
            let waist = assessmentData.girth.customerWaist.split(" ");
            if (waist[0] != "" && waist[0] != 0) {
              this.customerWaistForPastAssessment = waist[0];
              this.isCustomerWaistPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerWaistForPastAssessment);
            }

            this.customerWaistUnitForPastAssessment = waist[1];
            //this.customerWaist = assessmentData.girth.customerWaist;

          }

          if (assessmentData.girth.customerHip != null && assessmentData.girth.customerHip != "") {
            let hip = assessmentData.girth.customerHip.split(" ");
            if (hip[0] != "" && hip[0] != 0) {
              this.customerHipForPastAssessment = hip[0];
              this.isCustomerHipPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerHipForPastAssessment);
            }

            this.customerHipUnitForPastAssessment = hip[1];
            //this.customerHip = assessmentData.girth.customerHip;

          }

          if (assessmentData.girth.customerLeftThigh != null && assessmentData.girth.customerLeftThigh != "") {
            let leftThigh = assessmentData.girth.customerLeftThigh.split(" ");
            if (leftThigh[0] != "" && leftThigh[0] != 0) {
              this.customerLeftThighForPastAssessment = leftThigh[0];
              this.isCustomerLeftThighPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerLeftThighForPastAssessment);
            }

            this.customerLeftThighUnitForPastAssessment = leftThigh[1];
            //this.customerLeftThigh = assessmentData.girth.customerLeftThigh;

          }

          if (assessmentData.girth.customerRightThigh != null && assessmentData.girth.customerRightThigh != "") {
            let rightThigh = assessmentData.girth.customerRightThigh.split(" ");
            if (rightThigh[0] != "" && rightThigh[0] != 0) {
              this.customerRightThighForPastAssessment = rightThigh[0];
              this.isCustomerRightThighPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerRightThighForPastAssessment);
            }

            this.customerRightThighUnitForPastAssessment = rightThigh[1];
            //this.customerRightThigh = assessmentData.girth.customerRightThigh;

          }

          if (assessmentData.girth.customerLeftCalf != null && assessmentData.girth.customerLeftCalf != "") {
            let leftCalf = assessmentData.girth.customerLeftCalf.split(" ");
            if (leftCalf[0] != "" && leftCalf[0] != 0) {
              this.customerLeftCalfForPastAssessment = leftCalf[0];
              this.isCustomerLeftCalfPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerLeftCalfForPastAssessment);
            }

            this.customerLeftCalfUnitForPastAssessment = leftCalf[1];
            //this.customerLeftCalf = assessmentData.girth.customerLeftCalf;

          }

          if (assessmentData.girth.customerRightCalf != null && assessmentData.girth.customerRightCalf != "") {
            let rightCalf = assessmentData.girth.customerRightCalf.split(" ");
            if (rightCalf[0] != "" && rightCalf[0] != 0) {
              this.customerRightCalfForPastAssessment = rightCalf[0];
              this.isCustomerRightCalfPresentForPastAssessment = true;
              this.totalForPastAssessment += Number(this.customerRightCalfForPastAssessment);
            }

            this.customerRightCalfUnitForPastAssessment = rightCalf[1];
            // this.customerRightCalf = assessmentData.girth.customerRightCalf;

          }

          if (assessmentData.girth.whr != null && assessmentData.girth.whr != "") {
            let whrData = assessmentData.girth.whr.split(" ");
            if (whrData[0] != "" && whrData[0] != 0) {
              this.whrForPastAssessment = whrData[0];
              this.isWhrPresentForPastAssessment = true;
              //this.totalForPastAssessment += Number(this.whrForPastAssessment);
            }



            this.whrUnitForPastAssessment = whrData[1];
            //this.whr = assessmentData.girth.whr;

          }

          this.totalForPastEditAssessment = this.totalForPastAssessment.toFixed(3);

          //for skin folds
          if (assessmentData.skinFold.mmAbdominal != null && assessmentData.skinFold.mmAbdominal != "") {
            let mmAbdominal = assessmentData.skinFold.mmAbdominal.split(" ");
            if (mmAbdominal[0] != "") {
              this.mmAbdominalForPastAssessment = mmAbdominal[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmAbdominalForPastAssessment);
              this.ismmAbdominalPresentForPastAssessment = true;
            }

            this.mmAbdominalUnitForPastAssessment = mmAbdominal[1];
            //this.mmAbdominal = assessmentData.skinFold.mmAbdominal;

          }

          if (assessmentData.skinFold.mmTricep != null && assessmentData.skinFold.mmTricep != "") {
            let mmTricep = assessmentData.skinFold.mmTricep.split(" ");
            if (mmTricep[0] != "") {
              this.mmTricepForPastAssessment = mmTricep[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmTricepForPastAssessment);
              this.ismmTricepPresentForPastAssessment = true;
            }

            this.mmTricepUnitForPastAssessment = mmTricep[1];
            //this.mmTricep = assessmentData.skinFold.mmTricep;

          }

          if (assessmentData.skinFold.mmBicep != null && assessmentData.skinFold.mmBicep != "") {
            let mmBicep = assessmentData.skinFold.mmBicep.split(" ");
            if (mmBicep[0] != "") {
              this.mmBicepForPastAssessment = mmBicep[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmBicepForPastAssessment);
              this.ismmBicepPresentForPastAssessment = true;
            }


            this.mmBicepUnitForPastAssessment = mmBicep[1];
            //this.mmBicep = assessmentData.skinFold.mmBicep;

          }

          if (assessmentData.skinFold.mmChest != null && assessmentData.skinFold.mmChest != "") {
            let mmChest = assessmentData.skinFold.mmChest.split(" ");
            if (mmChest[0] != "") {
              this.mmChestForPastAssessment = mmChest[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmChestForPastAssessment);
              this.ismmChestPresentForPastAssessment = true;
            }

            this.mmChestUnitForPastAssessment = mmChest[1];
            //this.mmChest = assessmentData.skinFold.mmChest;

          }

          if (assessmentData.skinFold.mmMidaxillary != null && assessmentData.skinFold.mmMidaxillary != "") {
            let mmMidaxillary = assessmentData.skinFold.mmMidaxillary.split(" ");
            if (mmMidaxillary[0] != "") {
              this.mmMidaxillaryForPastAssessment = mmMidaxillary[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmMidaxillaryForPastAssessment);
              this.ismmMidaxillaryPresentForPastAssessment = true;
            }

            this.mmMidaxillaryUnitForPastAssessment = mmMidaxillary[1];
            //this.mmMidaxillary = assessmentData.skinFold.mmMidaxillary;

          }

          if (assessmentData.skinFold.mmSubscapular != null && assessmentData.skinFold.mmSubscapular != "") {
            let mmSubscapular = assessmentData.skinFold.mmSubscapular.split(" ");
            if (mmSubscapular[0] != "") {
              this.mmSubscapularForPastAssessment = mmSubscapular[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmSubscapularForPastAssessment);
              this.ismmSubscapularPresentForPastAssessment = true;
            }

            this.mmSubscapularUnitForPastAssessment = mmSubscapular[1];
            //this.mmSubscapular = assessmentData.skinFold.mmSubscapular;

          }

          if (assessmentData.skinFold.mmSuoraillac != null && assessmentData.skinFold.mmSuoraillac != "") {
            let mmSuoraillac = assessmentData.skinFold.mmSuoraillac.split(" ");
            if (mmSuoraillac[0] != "") {
              this.mmSuoraillacForPastAssessment = mmSuoraillac[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmSuoraillacForPastAssessment);
              this.ismmSuoraillacPresentForPastAssessment = true;
            }

            this.mmSuoraillacUnitForPastAssessment = mmSuoraillac[1];
            //this.mmSuoraillac = assessmentData.skinFold.mmSuoraillac;

          }

          if (assessmentData.skinFold.mmCalf != null && assessmentData.skinFold.mmCalf != "") {
            let mmCalf = assessmentData.skinFold.mmCalf.split(" ");
            if (mmCalf[0] != "") {
              this.mmCalfForPastAssessment = mmCalf[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmCalfForPastAssessment);
              this.ismmCalfPresentForPastAssessment = true;
            }

            this.mmCalfUnitForPastAssessment = mmCalf[1];
            //this.mmCalf = assessmentData.skinFold.mmCalf;

          }


          if (assessmentData.skinFold.mmThigh != null && assessmentData.skinFold.mmThigh != "") {
            let mmThigh = assessmentData.skinFold.mmThigh.split(" ");
            if (mmThigh[0] != "") {
              this.mmThighForPastAssessment = mmThigh[0];
              this.mmSkinFoldTotalForPastAssessment += Number(this.mmThighForPastAssessment);
              this.ismmThighPresentForPastAssessment = true;
            }

            this.mmThighUnitForPastAssessment = mmThigh[1];
            //this.mmThigh = assessmentData.skinFold.mmThigh;

          }

          if (assessmentData.skinFold.mmSkinFoldTotal != null && assessmentData.skinFold.mmSkinFoldTotal != "") {
            let mmSkinFoldTotal = assessmentData.skinFold.mmSkinFoldTotal.split(" ");
            // if (mmSkinFoldTotal[0] != "") {
            //   this.mmSkinFoldTotalForPastAssessment = mmSkinFoldTotal[0];
            //   this.ismmSkinFoldTotalPresentForPastAssessment = true;
            // }

            this.mmSkinFoldTotalUnitForPastAssessment = mmSkinFoldTotal[1];
            //this.mmSkinFoldTotal = assessmentData.skinFold.mmSkinFoldTotal;

          }

          if (assessmentData.skinFold.mmSkinFoldAverage != null && assessmentData.skinFold.mmSkinFoldAverage != "") {
            let mmSkinFoldAverage = assessmentData.skinFold.mmSkinFoldAverage.split(" ");
            // if (mmSkinFoldAverage[0] != "") {
            //   this.mmSkinFoldAverageForPastAssessment = mmSkinFoldAverage[0];
            //   this.ismmSkinFoldAveragePresentForPastAssessment = true;
            // }

            this.mmSkinFoldAverageUnitForPastAssessment = mmSkinFoldAverage[1];
            //this.mmSkinFoldAverage = assessmentData.skinFold.mmSkinFoldAverage;

          }
          this.mmSkinFoldAverageForPastAssessment = this.mmSkinFoldTotalForPastAssessment / 9.0;
          this.pastEditSkinFoldTotal = this.mmSkinFoldTotalForPastAssessment.toFixed(3);
          this.pastEditSkinFoldAverage = this.mmSkinFoldAverageForPastAssessment.toFixed(3);


          //for other information

          if (assessmentData.bodyFatPercentage != null && assessmentData.bodyFatPercentage != "") {
            this.bodyFatPercentageForPastAssessment = assessmentData.bodyFatPercentage;
            this.isbodyFatPercentagePresentForPastAssessment = true;
          }

          if (assessmentData.waterPercentage != null && assessmentData.waterPercentage != "") {
            this.waterPercentageForPastAssessment = assessmentData.waterPercentage;
            this.iswaterPercentagePresentForPastAssessment = true;
          }

          if (assessmentData.customerBmi != null && assessmentData.customerBmi != "") {
            this.customerBmiForPastAssessment = assessmentData.customerBmi;
            this.iscustomerBmiPresentForPastAssessment = true;
          }

          if (assessmentData.leanMass != null && assessmentData.leanMass != "" && assessmentData.leanMass != "0") {
            this.leanMassForPastAssessment = assessmentData.leanMass;
            this.isleanMassPresentForPastAssessment = true;
          }

          console.log("assessmentData.customer_assessment_note outer if:::" + assessmentData.customer_assessment_note);
          if (assessmentData.customer_assessment_note != "" && assessmentData.customer_assessment_note != null) {
            let assessment_note = assessmentData.customer_assessment_note.split(" ");
            if (assessment_note[0] != "") {
              this.customer_assessment_noteForPastAssessment = assessmentData.customer_assessment_note;
              this.is_customer_assessment_note_presentForPastAssessment = true;
            }
            console.log("assessmentData.customer_assessment_note:::" + typeof assessmentData.customer_assessment_note);
            console.log("this.is_customer_assessment_note_presentForPastAssessment&&&&" + this.is_customer_assessment_note_presentForPastAssessment);
          }
          else {
            this.customer_assessment_noteForPastAssessment = "";
            this.is_customer_assessment_note_presentForPastAssessment = false;
          }

        }

      }




    })
    //this.dismissLodaing();
  }

  //get last assessment data for add assessment
  getPassAssessmentData(username) {
    this.total = 0;
    this.mmSkinFoldTotal = 0;
    this.mmSkinFoldAverage = 0;
    //this.presentLoading();
    this.myProgressProvider.getLastAssessment(username).then(res => {
      this.response = res;
      console.log("assessment data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message.toLowerCase() === "success") {
        this.getcustomerassessment = this.response.responseObj.getcustomerassessment;
        if (this.response.responseObj.getcustomerassessment.length > 0) {
          for (let assessmentData of this.getcustomerassessment) {

            this.assessId = assessmentData.assessId;

            if (assessmentData.customerId !== undefined) {
              localStorage.setItem('customerId', String(assessmentData.customerId));
            }

            this.customerId = localStorage.getItem('customerId');
            console.log("Customer Id is:::::" + this.customerId);

            if (assessmentData.assessDate != null && assessmentData.assessDate != "") {
              this.assessDate = assessmentData.assessDate;
              // let newDate = new Date(assessmentData.assessDate);
              // this.assessDate = newDate;
            }

            if (assessmentData.customerWeight != null && assessmentData.customerWeight != "") {
              let weight = assessmentData.customerWeight.split(" ");
              if (weight[0] != "") {
                this.customerWeight = weight[0];
                this.isCustomerWeightPresent = true;
              }
            }

            if (assessmentData.customerHieght != null && assessmentData.customerHieght != "") {
              //this.customerHieght = assessmentData.customerHieght;
              let customerHieght = assessmentData.customerHieght.split(".");
              if (customerHieght[0] != "") {
                this.customerHieghtFt = customerHieght[0];
              }
              if (customerHieght[1] != "") {
                this.customerHieghtIn = customerHieght[1];
              }
              this.isCustomerHieghtPresent = true;
            }

            if (assessmentData.customerAge != null && assessmentData.customerAge != "") {
              this.customerAge = assessmentData.customerAge;
              this.isCustomerAgePresent = true;
            }

            if (assessmentData.customer_bp != null && assessmentData.customer_bp != "") {
              let customer_bp = assessmentData.customer_bp.split("/");
              if (customer_bp[0] != "") {
                this.customer_bp_upper = customer_bp[0];
              }
              if (customer_bp[1] != "") {
                this.customer_bp_lower = customer_bp[1];
              }

              this.isCustomer_bpPresent = true;
              console.log(this.customer_bp_upper);
              console.log(this.customer_bp_lower)
            }



            //for girth

            if (assessmentData.girth.customerNeck != null && assessmentData.girth.customerNeck != "") {
              let neck = assessmentData.girth.customerNeck.split(" ");
              if (neck[0] != "") {
                this.customerNeck = neck[0];
                this.isCustomerNeckPresent = true;
                this.total += Number(this.customerNeck);
              }
              this.customerNeckUnit = neck[1];
            }

            if (assessmentData.girth.customerShoulder != null && assessmentData.girth.customerShoulder != "") {
              let shoulder = assessmentData.girth.customerShoulder.split(" ");
              if (shoulder[0] != "") {
                this.customerShoulder = shoulder[0];
                this.isCustomerShoulderPresent = true;
                this.total += Number(this.customerShoulder);
              }
              this.customerShoulderUnit = shoulder[1];
              //this.customerShoulder = assessmentData.girth.customerShoulder;
            }

            if (assessmentData.girth.customerChest != null && assessmentData.girth.customerChest != "") {
              let chest = assessmentData.girth.customerChest.split(" ");
              if (chest[0] != "") {
                this.customerChest = chest[0];
                this.isCustomerChestPresent = true;
                this.total += Number(this.customerChest);
              }
              this.customerChestUnit = chest[1];
              //this.customerChest = assessmentData.girth.customerChest;


            }

            if (assessmentData.girth.customerLeftArm != null && assessmentData.girth.customerLeftArm != "") {
              let leftArm = assessmentData.girth.customerLeftArm.split(" ");
              if (leftArm[0] != "") {
                this.customerLeftArm = leftArm[0];
                this.isCustomerLeftArmPresent = true;
                this.total += Number(this.customerLeftArm);
              }
              this.customerLeftArmUnit = leftArm[1];
              //this.customerLeftArm = assessmentData.girth.customerLeftArm;

            }

            if (assessmentData.girth.customerRightArm != null && assessmentData.girth.customerRightArm != "") {
              let rightArm = assessmentData.girth.customerRightArm.split(" ");
              if (rightArm[0] != "") {
                this.customerRightArm = rightArm[0];
                this.isCustomerRightArmPresent = true;
                this.total += Number(this.customerRightArm);
              }

              this.customerRightArmUnit = rightArm[1];
              //this.customerRightArm = assessmentData.girth.customerRightArm;

            }

            if (assessmentData.girth.customerWaist != null && assessmentData.girth.customerWaist != "") {
              let waist = assessmentData.girth.customerWaist.split(" ");
              if (waist[0] != "") {
                this.customerWaist = waist[0];
                this.isCustomerWaistPresent = true;
                this.total += Number(this.customerWaist);
              }

              this.customerWaistUnit = waist[1];
              //this.customerWaist = assessmentData.girth.customerWaist;

            }

            if (assessmentData.girth.customerHip != null && assessmentData.girth.customerHip != "") {
              let hip = assessmentData.girth.customerHip.split(" ");
              if (hip[0] != "") {
                this.customerHip = hip[0];
                this.isCustomerHipPresent = true;
                this.total += Number(this.customerHip);
              }

              this.customerHipUnit = hip[1];
              //this.customerHip = assessmentData.girth.customerHip;

            }

            if (assessmentData.girth.customerLeftThigh != null && assessmentData.girth.customerLeftThigh != "") {
              let leftThigh = assessmentData.girth.customerLeftThigh.split(" ");
              if (leftThigh[0] != "") {
                this.customerLeftThigh = leftThigh[0];
                this.isCustomerLeftThighPresent = true;
                this.total += Number(this.customerLeftThigh);
              }

              this.customerLeftThighUnit = leftThigh[1];
              //this.customerLeftThigh = assessmentData.girth.customerLeftThigh;

            }

            if (assessmentData.girth.customerRightThigh != null && assessmentData.girth.customerRightThigh != "") {
              let rightThigh = assessmentData.girth.customerRightThigh.split(" ");
              if (rightThigh[0] != "") {
                this.customerRightThigh = rightThigh[0];
                this.isCustomerRightThighPresent = true;
                this.total += Number(this.customerRightThigh);
              }

              this.customerRightThighUnit = rightThigh[1];
              //this.customerRightThigh = assessmentData.girth.customerRightThigh;

            }

            if (assessmentData.girth.customerLeftCalf != null && assessmentData.girth.customerLeftCalf != "") {
              let leftCalf = assessmentData.girth.customerLeftCalf.split(" ");
              if (leftCalf[0] != "") {
                this.customerLeftCalf = leftCalf[0];
                this.isCustomerLeftCalfPresent = true;
                this.total += Number(this.customerLeftCalf);
              }

              this.customerLeftCalfUnit = leftCalf[1];
              //this.customerLeftCalf = assessmentData.girth.customerLeftCalf;

            }

            if (assessmentData.girth.customerRightCalf != null && assessmentData.girth.customerRightCalf != "") {
              let rightCalf = assessmentData.girth.customerRightCalf.split(" ");
              if (rightCalf[0] != "") {
                this.customerRightCalf = rightCalf[0];
                this.isCustomerRightCalfPresent = true;
                this.total += Number(this.customerRightCalf);
              }

              this.customerRightCalfUnit = rightCalf[1];
              // this.customerRightCalf = assessmentData.girth.customerRightCalf;

            }

            if (assessmentData.girth.whr != null && assessmentData.girth.whr != "") {
              let whrData = assessmentData.girth.whr.split(" ");
              if (whrData[0] != "") {
                this.whr = whrData[0];
                this.isWhrPresent = true;
                //this.total += Number(this.whr);
              }

              this.whrUnit = whrData[1];
              //this.whr = assessmentData.girth.whr;

            }

            this.totalForEditAssessment = this.total.toFixed(3);

            //for skin folds
            if (assessmentData.skinFold.mmAbdominal != null && assessmentData.skinFold.mmAbdominal != "") {
              let mmAbdominal = assessmentData.skinFold.mmAbdominal.split(" ");
              if (mmAbdominal[0] != "") {
                this.mmAbdominal = mmAbdominal[0];
                this.mmSkinFoldTotal += Number(this.mmAbdominal);
                this.ismmAbdominalPresent = true;
              }

              this.mmAbdominalUnit = mmAbdominal[1];
              //this.mmAbdominal = assessmentData.skinFold.mmAbdominal;

            }

            if (assessmentData.skinFold.mmTricep != null && assessmentData.skinFold.mmTricep != "") {
              let mmTricep = assessmentData.skinFold.mmTricep.split(" ");
              if (mmTricep[0] != "") {
                this.mmTricep = mmTricep[0];
                this.mmSkinFoldTotal += Number(this.mmTricep);
                this.ismmTricepPresent = true;
              }

              this.mmTricepUnit = mmTricep[1];
              //this.mmTricep = assessmentData.skinFold.mmTricep;

            }

            if (assessmentData.skinFold.mmBicep != null && assessmentData.skinFold.mmBicep != "") {
              let mmBicep = assessmentData.skinFold.mmBicep.split(" ");
              if (mmBicep[0] != "") {
                this.mmBicep = mmBicep[0];
                this.mmSkinFoldTotal += Number(this.mmBicep);
                this.ismmBicepPresent = true;
              }

              this.mmBicepUnit = mmBicep[1];
              //this.mmBicep = assessmentData.skinFold.mmBicep;

            }

            if (assessmentData.skinFold.mmChest != null && assessmentData.skinFold.mmChest != "") {
              let mmChest = assessmentData.skinFold.mmChest.split(" ");
              if (mmChest[0] != "") {
                this.mmChest = mmChest[0];
                this.mmSkinFoldTotal += Number(this.mmChest);
                this.ismmChestPresent = true;
              }

              this.mmChestUnit = mmChest[1];
              //this.mmChest = assessmentData.skinFold.mmChest;
            }

            if (assessmentData.skinFold.mmMidaxillary != null && assessmentData.skinFold.mmMidaxillary != "") {
              let mmMidaxillary = assessmentData.skinFold.mmMidaxillary.split(" ");
              if (mmMidaxillary[0] != "") {
                this.mmMidaxillary = mmMidaxillary[0];
                this.mmSkinFoldTotal += Number(this.mmMidaxillary);
                this.ismmMidaxillaryPresent = true;
              }

              this.mmMidaxillaryUnit = mmMidaxillary[1];
              //this.mmMidaxillary = assessmentData.skinFold.mmMidaxillary;

            }

            if (assessmentData.skinFold.mmSubscapular != null && assessmentData.skinFold.mmSubscapular != "") {
              let mmSubscapular = assessmentData.skinFold.mmSubscapular.split(" ");
              if (mmSubscapular[0] != "") {
                this.mmSubscapular = mmSubscapular[0];
                this.mmSkinFoldTotal += Number(this.mmSubscapular);
                this.ismmSubscapularPresent = true;
              }

              this.mmSubscapularUnit = mmSubscapular[1];
              //this.mmSubscapular = assessmentData.skinFold.mmSubscapular;

            }

            if (assessmentData.skinFold.mmSuoraillac != null && assessmentData.skinFold.mmSuoraillac != "") {
              let mmSuoraillac = assessmentData.skinFold.mmSuoraillac.split(" ");
              if (mmSuoraillac[0] != "") {
                this.mmSuoraillac = mmSuoraillac[0];
                this.mmSkinFoldTotal += Number(this.mmSuoraillac);
                this.ismmSuoraillacPresent = true;
              }

              this.mmSuoraillacUnit = mmSuoraillac[1];
              //this.mmSuoraillac = assessmentData.skinFold.mmSuoraillac;

            }

            if (assessmentData.skinFold.mmCalf != null && assessmentData.skinFold.mmCalf != "") {
              let mmCalf = assessmentData.skinFold.mmCalf.split(" ");
              if (mmCalf[0] != "") {
                this.mmCalf = mmCalf[0];
                this.mmSkinFoldTotal += Number(this.mmCalf);
                this.ismmCalfPresent = true;
              }
              this.mmCalfUnit = mmCalf[1];
              //this.mmCalf = assessmentData.skinFold.mmCalf;

            }


            if (assessmentData.skinFold.mmThigh != null && assessmentData.skinFold.mmThigh != "") {
              let mmThigh = assessmentData.skinFold.mmThigh.split(" ");
              if (mmThigh[0] != "") {
                this.mmThigh = mmThigh[0];
                this.mmSkinFoldTotal += Number(this.mmThigh);
                this.ismmThighPresent = true;
              }

              this.mmThighUnit = mmThigh[1];
              //this.mmThigh = assessmentData.skinFold.mmThigh;

            }

            if (assessmentData.skinFold.mmSkinFoldTotal != null && assessmentData.skinFold.mmSkinFoldTotal != "") {
              let mmSkinFoldTotal = assessmentData.skinFold.mmSkinFoldTotal.split(" ");
              // if (mmSkinFoldTotal[0] != "") {
              //   this.mmSkinFoldTotal = Number(mmSkinFoldTotal[0]);
              //    this.ismmSkinFoldTotalPresent = true;
              //  }
              this.mmSkinFoldTotalUnit = mmSkinFoldTotal[1];
              //this.mmSkinFoldTotal = assessmentData.skinFold.mmSkinFoldTotal;

            }

            if (assessmentData.skinFold.mmSkinFoldAverage != null && assessmentData.skinFold.mmSkinFoldAverage != "") {
              let mmSkinFoldAverage = assessmentData.skinFold.mmSkinFoldAverage.split(" ");
              //  if (mmSkinFoldAverage[0] != "") {
              //    this.mmSkinFoldAverage = Number(mmSkinFoldAverage[0]);
              //    this.ismmSkinFoldAveragePresent = true;
              //  }

              this.mmSkinFoldAverageUnit = mmSkinFoldAverage[1];
              //this.mmSkinFoldAverage = assessmentData.skinFold.mmSkinFoldAverage;

            }
            this.mmSkinFoldAverage = this.mmSkinFoldTotal / 9.0;
            this.editSkinFoldTotal = this.mmSkinFoldTotal.toFixed(3);
            this.editSkinFoldAverage = this.mmSkinFoldAverage.toFixed(3);


            //for other information

            if (assessmentData.bodyFatPercentage != null && assessmentData.bodyFatPercentage != "") {
              this.bodyFatPercentage = assessmentData.bodyFatPercentage;
              this.isbodyFatPercentagePresent = true;
            }

            if (assessmentData.waterPercentage != null && assessmentData.waterPercentage != "") {
              this.waterPercentage = assessmentData.waterPercentage;
              this.iswaterPercentagePresent = true;
            }

            if (assessmentData.customerBmi != null && assessmentData.customerBmi != "") {
              this.customerBmi = assessmentData.customerBmi;
              this.iscustomerBmiPresent = true;
            }

            if (assessmentData.leanMass != null && assessmentData.leanMass != "") {
              this.leanMass = assessmentData.leanMass;
              this.isleanMassPresent = true;
            }

            if (assessmentData.customer_assessment_note != null && assessmentData.customer_assessment_note != "") {
              this.customer_assessment_note = assessmentData.customer_assessment_note;
              this.is_customer_assessment_note_present = true;
            }

          }
          this.isDisablePastAssessment = false;
        }
        else {
          this.showAlert("Alert", "Past assessment information is not present.");
          this.isDisablePastAssessment = true;
        }

        console.log("this.isDisablePastAssessment::::" + this.isDisablePastAssessment);

      }




    })
    //this.dismissLodaing();

    //this.myProgressProvider.getPastAssessmentdateList();
  }



  //to save past Assessment data
  savePastAssessmentPage() {
    
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();



    this.customerAssessmentObj.assessId = this.assessIdForPastAssessment;
    this.customerAssessmentObj.customerId = this.customerId;
    this.customerAssessmentObj.assessDate = this.assessDateForPastAssessment;
    // if (this.isbodyFatPercentagePresentForPastAssessment === true) {
    //   this.customerAssessmentObj.bodyFatPercentage = this.bodyFatPercentageForPastAssessment;
    // }

    //set all calculated fields
    if (this.customerBmiForPastAssessment != null || this.customerBmiForPastAssessment != '') {
      this.customerAssessmentObj.customerBmi = this.customerBmiForPastAssessment;
    }
    if (this.bodyFatPercentageForPastAssessment != null || this.bodyFatPercentageForPastAssessment != '') {
      this.customerAssessmentObj.bodyFatPercentage = this.bodyFatPercentageForPastAssessment;
    }
    if (this.leanMassForPastAssessment != null || this.leanMassForPastAssessment != '') {
      this.customerAssessmentObj.leanMass = this.leanMassForPastAssessment;
    }
    if (this.waterPercentageForPastAssessment != null || this.waterPercentageForPastAssessment != '') {
      this.customerAssessmentObj.waterPercentage = this.waterPercentageForPastAssessment;
    }

    if (this.isCustomerAgePresentForPastAssessment === true) {
      this.customerAssessmentObj.customerAge = this.customerAgeForPastAssessment;
    }
    // if (this.iscustomerBmiPresentForPastAssessment === true) {
    //   this.customerAssessmentObj.customerBmi = this.customerBmiForPastAssessment;
    // }

    if (this.isCustomerWeightPresentForPastAssessment === true) {
      this.customerAssessmentObj.customerWeight = this.customerWeightForPastAssessment;
      console.log("weight is::::" + this.customerAssessmentObj.customerWeight);
    }
    // if (this.isleanMassPresentForPastAssessment === true) {
    //   this.customerAssessmentObj.leanMass = this.leanMassForPastAssessment;
    // }

    //if (this.is_customer_assessment_note_presentForPastAssessment === true) {
    this.customerAssessmentObj.customer_assessment_note = this.customer_assessment_noteForPastAssessment;
    //}
    // if (this.iswaterPercentagePresentForPastAssessment === true) {
    //   this.customerAssessmentObj.waterPercentage = this.waterPercentageForPastAssessment;
    //}

    if (this.isCustomer_bpPresentForPastAssessment === true) {
      this.customer_bpForPastAssessment = this.customer_bp_upperForPastAssessment.concat("/").concat(this.customer_bp_lowerForPastAssessment);
      this.customerAssessmentObj.customer_bp = this.customer_bpForPastAssessment;
    }
    if (this.isCustomerHieghtPresentForPastAssessment === true) {
      this.customerHieghtForPastAssessment = this.customerHieghtForPastAssessmentFt.concat(".").concat(this.customerHieghtForPastAssessmentIn);
      this.customerAssessmentObj.customerHieght = this.customerHieghtForPastAssessment;
    }
    // if(this.isCustomerHieghtPresent === true){
    //   this.customerAssessmentObj.customerHieght = this.customerHieght;
    // }

    //for girth
    if (this.isCustomerNeckPresentForPastAssessment === true) {
      let neckStr = this.customerNeckForPastAssessment.concat(" ").concat(this.customerNeckUnitForPastAssessment);
      this.girthObj.customerNeck = neckStr;
    }
    if (this.isCustomerShoulderPresentForPastAssessment === true) {
      let shoulderStr = this.customerShoulderForPastAssessment.concat(" ").concat(this.customerShoulderUnitForPastAssessment);
      this.girthObj.customerShoulder = shoulderStr;
    }
    if (this.isCustomerChestPresentForPastAssessment === true) {
      let chestStr = this.customerChestForPastAssessment.concat(" ").concat(this.customerChestUnitForPastAssessment);
      this.girthObj.customerChest = chestStr;
    }
    if (this.isCustomerLeftArmPresentForPastAssessment === true) {
      let leftArmStr = this.customerLeftArmForPastAssessment.concat(" ").concat(this.customerLeftArmUnitForPastAssessment);
      this.girthObj.customerLeftArm = leftArmStr;
    }
    if (this.isCustomerRightArmPresentForPastAssessment === true) {
      let rightArmStr = this.customerRightArmForPastAssessment.concat(" ").concat(this.customerRightArmUnitForPastAssessment);
      this.girthObj.customerRightArm = rightArmStr;
    }
    if (this.isCustomerWaistPresentForPastAssessment === true) {
      let waistStr = this.customerWaistForPastAssessment.concat(" ").concat(this.customerWaistUnitForPastAssessment);
      this.girthObj.customerWaist = waistStr;
    }
    if (this.isCustomerHipPresentForPastAssessment === true) {
      let hipStr = this.customerHipForPastAssessment.concat(" ").concat(this.customerHipUnitForPastAssessment);
      this.girthObj.customerHip = hipStr;
    }
    if (this.isCustomerLeftThighPresentForPastAssessment === true) {
      let leftThighStr = this.customerLeftThighForPastAssessment.concat(" ").concat(this.customerLeftThighUnitForPastAssessment);
      this.girthObj.customerLeftThigh = leftThighStr;
    }
    if (this.isCustomerRightThighPresentForPastAssessment === true) {
      let rightThighStr = this.customerRightThighForPastAssessment.concat(" ").concat(this.customerRightThighUnitForPastAssessment);
      this.girthObj.customerRightThigh = rightThighStr;
    }
    if (this.isCustomerLeftCalfPresentForPastAssessment === true) {
      let leftCalfStr = this.customerLeftCalfForPastAssessment.concat(" ").concat(this.customerLeftCalfUnitForPastAssessment);
      this.girthObj.customerLeftCalf = leftCalfStr;
    }
    if (this.isCustomerRightCalfPresentForPastAssessment === true) {
      let rightCalfStr = this.customerRightCalfForPastAssessment.concat(" ").concat(this.customerRightCalfUnitForPastAssessment);
      this.girthObj.customerRightCalf = rightCalfStr;
    }
    if (this.isWhrPresentForPastAssessment === true) {
      let whrStr = this.whrForPastAssessment.concat(" ").concat(this.whrUnitForPastAssessment);
      this.girthObj.whr = whrStr;
    }


    //for skinfold
    if (this.ismmAbdominalPresentForPastAssessment === true) {
      let mmAbdominalStr = this.mmAbdominalForPastAssessment.concat(" ").concat(this.mmAbdominalUnitForPastAssessment);
      this.skinFoldObj.mmAbdominal = mmAbdominalStr;
    }
    if (this.ismmBicepPresentForPastAssessment === true) {
      let mmBicepStr = this.mmBicepForPastAssessment.concat(" ").concat(this.mmBicepUnitForPastAssessment);
      this.skinFoldObj.mmBicep = mmBicepStr;
    }
    if (this.ismmCalfPresentForPastAssessment === true) {
      let mmCalfStr = this.mmCalfForPastAssessment.concat(" ").concat(this.mmCalfUnitForPastAssessment);
      this.skinFoldObj.mmCalf = mmCalfStr;
    }
    if (this.ismmChestPresentForPastAssessment === true) {
      let mmChestStr = this.mmChestForPastAssessment.concat(" ").concat(this.mmChestUnitForPastAssessment);
      this.skinFoldObj.mmChest = mmChestStr;
    }
    if (this.ismmMidaxillaryPresentForPastAssessment === true) {
      let mmMidaxillaryStr = this.mmMidaxillaryForPastAssessment.concat(" ").concat(this.mmMidaxillaryUnitForPastAssessment);
      this.skinFoldObj.mmMidaxillary = mmMidaxillaryStr;
    }
    // if (this.ismmSkinFoldAveragePresentForPastAssessment === true) {
    //   let mmSkinFoldAverageStr = String(this.mmSkinFoldAverageForPastAssessment).concat(" ").concat(this.mmSkinFoldAverageUnitForPastAssessment);
    //   this.skinFoldObj.mmSkinFoldAverage = mmSkinFoldAverageStr;
    // }
    // if (this.ismmSkinFoldTotalPresentForPastAssessment === true) {
    //   let mmSkinFoldTotalStr = String(this.mmSkinFoldTotalForPastAssessment).concat(" ").concat(this.mmSkinFoldTotalUnitForPastAssessment);
    //   this.skinFoldObj.mmSkinFoldTotal = mmSkinFoldTotalStr;
    // }
    if (this.ismmSubscapularPresentForPastAssessment === true) {
      let mmSubscapularStr = this.mmSubscapularForPastAssessment.concat(" ").concat(this.mmSubscapularUnitForPastAssessment);
      this.skinFoldObj.mmSubscapular = mmSubscapularStr;
    }
    if (this.ismmSuoraillacPresentForPastAssessment === true) {
      let mmSuoraillacStr = this.mmSuoraillacForPastAssessment.concat(" ").concat(this.mmSuoraillacUnitForPastAssessment);
      this.skinFoldObj.mmSuoraillac = mmSuoraillacStr;
    }
    if (this.ismmThighPresentForPastAssessment === true) {
      let mmThighStr = this.mmThighForPastAssessment.concat(" ").concat(this.mmThighUnitForPastAssessment);
      this.skinFoldObj.mmThigh = mmThighStr;
    }
    if (this.ismmTricepPresentForPastAssessment === true) {
      let mmTricepStr = this.mmTricepForPastAssessment.concat(" ").concat(this.mmTricepUnitForPastAssessment);
      this.skinFoldObj.mmTricep = mmTricepStr;
    }


    this.customerAssessmentObj.girth = this.girthObj;
    this.customerAssessmentObj.skinFold = this.skinFoldObj;

    //this.getcustomerassessment.push(this.customerAssessmentObj);



    console.log("updated past assessment data to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.presentConfirmForPastAssessment();

    //this.postAssessmentInfo();

  }



  //to save Assessment data
  saveAddAssessmentPage() {
    console.log("isShownAddProgressBlankForm is::::" + this.isShownAddProgressBlankForm);
    if (this.isShownAddProgressBlankForm === true && this.selectedSegmentName === "addAssessment") {
      this.customerAssessmentObj = new CustomerAssessment();
      this.girthObj = new Girth();
      this.skinFoldObj = new SkinFold();

      //for add assessment calculation
      let heightConvertedInMetersForAddAssessment = ((Number(this.add_customer_height_ft) * 0.3048) + (Number(this.add_customer_height_in) * 0.0254));
      let heightConvertedInCentimetersForAddAssessment = ((Number(this.add_customer_height_ft) * 30.48) + (Number(this.add_customer_height_in) * 2.54));

      this.customerAssessmentObj.assessId = this.assessId;
      this.customerAssessmentObj.customerId = this.customerId;
      this.customerAssessmentObj.assessDate = this.addAssessDate;


      // if ((this.isbodyFatPercentagePresent === true) && (this.addbodyFatPercentage != null || this.addbodyFatPercentage != undefined)) {
      //   this.customerAssessmentObj.bodyFatPercentage = this.addbodyFatPercentage;
      // }

      // set all calculated fields
      if (this.addcustomerBmi != null || this.addcustomerBmi != '') {
        this.customerAssessmentObj.customerBmi = this.addcustomerBmi;
      }
      if (this.addbodyFatPercentage != null || this.addbodyFatPercentage != '') {
        this.customerAssessmentObj.bodyFatPercentage = this.addbodyFatPercentage;
      }
      if (this.addleanMass != null || this.addleanMass != '') {
        this.customerAssessmentObj.leanMass = this.addleanMass;
      }
      if (this.addwaterPercentage != null || this.addwaterPercentage != '') {
        this.customerAssessmentObj.waterPercentage = this.addwaterPercentage;
      }


      if ((this.isCustomerAgePresent === true) && (this.addCustomerAge != null || this.addCustomerAge != undefined)) {
        this.customerAssessmentObj.customerAge = this.addCustomerAge;
      }
      //if ((this.iscustomerBmiPresent === true) && (this.addcustomerBmi != null || this.addcustomerBmi != undefined)) {



      //}
      if ((this.isCustomerWeightPresent === true) && (this.addCustomerWeight != null || this.addCustomerWeight != undefined)) {
        this.customerAssessmentObj.customerWeight = this.addCustomerWeight;
        console.log("weight is::::" + this.customerAssessmentObj.customerWeight);
      }

      // if ((this.isleanMassPresent === true) && (this.addleanMass != null || this.addleanMass != undefined)) {
      //   this.customerAssessmentObj.leanMass = this.addleanMass;
      // }


      // if (this.is_customer_assessment_note_present === true) {
      this.customerAssessmentObj.customer_assessment_note = this.add_customer_assessment_note;
      // }
      // if ((this.iswaterPercentagePresent === true) && (this.addwaterPercentage != null || this.addwaterPercentage != undefined)) {
      //   this.customerAssessmentObj.waterPercentage = this.addwaterPercentage;
      // }

      if ((this.isCustomer_bpPresent === true) && (this.add_customer_bp_upper != null || this.add_customer_bp_upper != undefined) && (this.add_customer_bp_lower != null || this.add_customer_bp_lower != undefined)) {
        this.customer_bp = this.add_customer_bp_upper.concat("/").concat(this.add_customer_bp_lower);
        this.customerAssessmentObj.customer_bp = this.customer_bp;
      }
      if ((this.isCustomerHieghtPresent === true) && (this.add_customer_height_ft != null || this.add_customer_height_ft != undefined) && (this.add_customer_height_in != null || this.add_customer_height_in != undefined)) {
        this.addCustomerHieght = this.add_customer_height_ft.concat(".").concat(this.add_customer_height_in);
        this.customerAssessmentObj.customerHieght = this.addCustomerHieght;
      }
      // if(this.isCustomerHieghtPresent === true){
      //   this.customerAssessmentObj.customerHieght = this.customerHieght;
      // }

      //for girth
      if ((this.isCustomerNeckPresent === true) && (this.addCustomerNeck != null || this.addCustomerNeck != undefined)) {
        let neckStr = this.addCustomerNeck.concat(" ").concat(this.customerNeckUnit);
        this.girthObj.customerNeck = neckStr;
        if (this.addCustomerNeck != "") {
          //this.totalForAddAssessment += Number(this.addCustomerNeck);
        }
      }
      if ((this.isCustomerShoulderPresent === true) && (this.addCustomerShoulder != null || this.addCustomerShoulder != undefined)) {
        let shoulderStr = this.addCustomerShoulder.concat(" ").concat(this.customerShoulderUnit);
        this.girthObj.customerShoulder = shoulderStr;
        if (this.addCustomerShoulder != "") {
          //this.totalForAddAssessment += Number(this.addCustomerShoulder);
        }
      }
      if ((this.isCustomerChestPresent === true) && (this.addCustomerChest != null || this.addCustomerChest != undefined)) {
        let chestStr = this.addCustomerChest.concat(" ").concat(this.customerChestUnit);
        this.girthObj.customerChest = chestStr;
        if (this.addCustomerChest != "") {
          //this.totalForAddAssessment += Number(this.addCustomerChest);
        }
      }
      if ((this.isCustomerLeftArmPresent === true) && (this.addCustomerLeftArm != null || this.addCustomerLeftArm != undefined)) {
        let leftArmStr = this.addCustomerLeftArm.concat(" ").concat(this.customerLeftArmUnit);
        this.girthObj.customerLeftArm = leftArmStr;
        if (this.addCustomerLeftArm != "") {
          //this.totalForAddAssessment += Number(this.addCustomerLeftArm);
        }
      }
      if ((this.isCustomerRightArmPresent === true) && (this.addCustomerRightArm != null || this.addCustomerRightArm != undefined)) {
        let rightArmStr = this.addCustomerRightArm.concat(" ").concat(this.customerRightArmUnit);
        this.girthObj.customerRightArm = rightArmStr;
        if (this.addCustomerRightArm != "") {
          //this.totalForAddAssessment += Number(this.addCustomerRightArm);
        }
      }
      if ((this.isCustomerWaistPresent === true) && (this.addCustomerWaist != null || this.addCustomerWaist != undefined)) {
        let waistStr = this.addCustomerWaist.concat(" ").concat(this.customerWaistUnit);
        this.girthObj.customerWaist = waistStr;
        if (this.addCustomerWaist != "") {
          //this.totalForAddAssessment += Number(this.addCustomerWaist);
        }
      }
      if ((this.isCustomerHipPresent === true) && (this.addCustomerHip != null || this.addCustomerHip != undefined)) {
        let hipStr = this.addCustomerHip.concat(" ").concat(this.customerHipUnit);
        this.girthObj.customerHip = hipStr;
        if (this.addCustomerHip != "") {
          //this.totalForAddAssessment += Number(this.addCustomerHip);
        }
      }
      if ((this.isCustomerLeftThighPresent === true) && (this.addCustomerLeftThigh != null || this.addCustomerLeftThigh != undefined)) {
        let leftThighStr = this.addCustomerLeftThigh.concat(" ").concat(this.customerLeftThighUnit);
        this.girthObj.customerLeftThigh = leftThighStr;
        if (this.addCustomerLeftThigh != "") {
          //this.totalForAddAssessment += Number(this.addCustomerLeftThigh);
        }
      }
      if ((this.isCustomerRightThighPresent === true) && (this.addCustomerRightThigh != null || this.addCustomerRightThigh != undefined)) {
        let rightThighStr = this.addCustomerRightThigh.concat(" ").concat(this.customerRightThighUnit);
        this.girthObj.customerRightThigh = rightThighStr;
        if (this.addCustomerRightThigh != "") {
          //this.totalForAddAssessment += Number(this.addCustomerRightThigh);
        }
      }
      if ((this.isCustomerLeftCalfPresent === true) && (this.addCustomerLeftCalf != null || this.addCustomerLeftCalf != undefined)) {
        let leftCalfStr = this.addCustomerLeftCalf.concat(" ").concat(this.customerLeftCalfUnit);
        this.girthObj.customerLeftCalf = leftCalfStr;
        if (this.addCustomerLeftCalf != "") {
          //this.totalForAddAssessment += Number(this.addCustomerLeftCalf);
        }
      }
      if ((this.isCustomerRightCalfPresent === true) && (this.addCustomerRightCalf != null || this.addCustomerRightCalf != undefined)) {
        let rightCalfStr = this.addCustomerRightCalf.concat(" ").concat(this.customerRightCalfUnit);
        this.girthObj.customerRightCalf = rightCalfStr;
        if (this.addCustomerRightCalf != "") {
          //this.totalForAddAssessment += Number(this.addCustomerRightCalf);
        }
      }
      if ((this.isWhrPresent === true) && (this.addWhr != null || this.addWhr != undefined)) {
        let whrStr = this.addWhr.concat(" ").concat(this.whrUnit);
        this.girthObj.whr = whrStr;
        if (this.addWhr != "") {
          //this.totalForAddAssessment += Number(this.addWhr);
        }
      }


      //for skinfold
      if ((this.ismmAbdominalPresent === true) && (this.addmmAbdominal != null || this.addmmAbdominal != undefined)) {
        let mmAbdominalStr = this.addmmAbdominal.concat(" ").concat(this.mmAbdominalUnit);
        this.skinFoldObj.mmAbdominal = mmAbdominalStr;
      }
      if ((this.ismmBicepPresent === true) && (this.addmmBicep != null || this.addmmBicep != undefined)) {
        let mmBicepStr = this.addmmBicep.concat(" ").concat(this.mmBicepUnit);
        this.skinFoldObj.mmBicep = mmBicepStr;
      }
      if ((this.ismmCalfPresent === true) && (this.addmmCalf != null || this.addmmCalf != undefined)) {
        let mmCalfStr = this.addmmCalf.concat(" ").concat(this.mmCalfUnit);
        this.skinFoldObj.mmCalf = mmCalfStr;
      }
      if ((this.ismmChestPresent === true) && (this.addmmChest != null || this.addmmChest != undefined)) {
        let mmChestStr = this.addmmChest.concat(" ").concat(this.mmChestUnit);
        this.skinFoldObj.mmChest = mmChestStr;
      }
      if ((this.ismmMidaxillaryPresent === true) && (this.addmmMidaxillary != null || this.addmmMidaxillary != undefined)) {
        let mmMidaxillaryStr = this.addmmMidaxillary.concat(" ").concat(this.mmMidaxillaryUnit);
        this.skinFoldObj.mmMidaxillary = mmMidaxillaryStr;
      }
      if ((this.ismmSkinFoldAveragePresent === true) && (this.addmmSkinFoldAverage != null || this.addmmSkinFoldAverage != undefined)) {
        let mmSkinFoldAverageStr = String(this.addmmSkinFoldAverage).concat(" ").concat(this.mmSkinFoldAverageUnit);
        this.skinFoldObj.mmSkinFoldAverage = mmSkinFoldAverageStr;
      }
      // if ((this.ismmSkinFoldTotalPresent === true) && (this.addmmSkinFoldTotal != null || this.addmmSkinFoldTotal != undefined)) {
      //   let mmSkinFoldTotalStr = String(this.addmmSkinFoldTotal).concat(" ").concat(this.mmSkinFoldTotalUnit);
      //   this.skinFoldObj.mmSkinFoldTotal = mmSkinFoldTotalStr;
      // }
      if ((this.ismmSubscapularPresent === true) && (this.addmmSubscapular != null || this.addmmSubscapular != undefined)) {
        let mmSubscapularStr = this.addmmSubscapular.concat(" ").concat(this.mmSubscapularUnit);
        this.skinFoldObj.mmSubscapular = mmSubscapularStr;
      }
      if ((this.ismmSuoraillacPresent === true) && (this.addmmSuoraillac != null || this.addmmSuoraillac != undefined)) {
        let mmSuoraillacStr = this.addmmSuoraillac.concat(" ").concat(this.mmSuoraillacUnit);
        this.skinFoldObj.mmSuoraillac = mmSuoraillacStr;
      }
      if ((this.ismmThighPresent === true) && (this.addmmThigh != null || this.addmmThigh != undefined)) {
        let mmThighStr = this.addmmThigh.concat(" ").concat(this.mmThighUnit);
        this.skinFoldObj.mmThigh = mmThighStr;
      }
      if ((this.ismmTricepPresent === true) && (this.addmmTricep != null || this.addmmTricep != undefined)) {
        let mmTricepStr = this.addmmTricep.concat(" ").concat(this.mmTricepUnit);
        this.skinFoldObj.mmTricep = mmTricepStr;
      }
      this.customerAssessmentObj.girth = this.girthObj;
      this.customerAssessmentObj.skinFold = this.skinFoldObj;

      //this.getcustomerassessment.push(this.customerAssessmentObj);



      console.log("getcustomerassessment FOR add assessment (to send):::::" + JSON.stringify(this.customerAssessmentObj));

      this.presentConfirm();
      //this.postAssessmentInfo();

    }

    else if(this.isShownAddProgressBlankForm === false && this.selectedSegmentName === "addAssessment") {
      this.customerAssessmentObj = new CustomerAssessment();
      this.girthObj = new Girth();
      this.skinFoldObj = new SkinFold();

      //for update assessment calculation
      let heightConvertedInMetersForUpdateAssessment = ((Number(this.customerHieghtFt) * 0.3048) + (Number(this.customerHieghtIn) * 0.0254));
      let heightConvertedInCentimetersForUpdateAssessment = ((Number(this.customerHieghtFt) * 30.48) + (Number(this.customerHieghtIn) * 2.54));
      this.customerAssessmentObj.assessId = this.assessId;
      this.customerAssessmentObj.customerId = this.customerId;
      this.customerAssessmentObj.assessDate = this.assessDate;
      // if (this.isbodyFatPercentagePresent === true) {
      //   this.customerAssessmentObj.bodyFatPercentage = this.bodyFatPercentage;
      // }


      //set all calculated fields
      if (this.customerBmi != null || this.customerBmi != '') {
        this.customerAssessmentObj.customerBmi = this.customerBmi;
      }
      if (this.bodyFatPercentage != null || this.bodyFatPercentage != '') {
        this.customerAssessmentObj.bodyFatPercentage = this.bodyFatPercentage;
      }
      if (this.leanMass != null || this.leanMass != '') {
        this.customerAssessmentObj.leanMass = this.leanMass;
      }
      if (this.waterPercentage != null || this.waterPercentage != '') {
        this.customerAssessmentObj.waterPercentage = this.waterPercentage;
      }



      if (this.isCustomerAgePresent === true) {
        this.customerAssessmentObj.customerAge = this.customerAge;
      }
      // if (this.iscustomerBmiPresent === true) {
      //   this.customerAssessmentObj.customerBmi = this.customerBmi;
      // }

      if (this.isCustomerWeightPresent === true) {
        this.customerAssessmentObj.customerWeight = this.customerWeight;
        console.log("weight is::::" + this.customerAssessmentObj.customerWeight);
      }
      // if (this.isleanMassPresent === true) {
      //   this.customerAssessmentObj.leanMass = this.leanMass;
      // }

      // if (this.is_customer_assessment_note_present === true) {
      this.customerAssessmentObj.customer_assessment_note = this.customer_assessment_note;
      // }
      // if (this.iswaterPercentagePresent === true) {
      //   this.customerAssessmentObj.waterPercentage = this.waterPercentage;
      // }

      if (this.isCustomer_bpPresent === true) {
        this.customer_bp = this.customer_bp_upper.concat("/").concat(this.customer_bp_lower);
        this.customerAssessmentObj.customer_bp = this.customer_bp;
      }
      if (this.isCustomerHieghtPresent === true) {
        this.customerHieght = this.customerHieghtFt.concat(".").concat(this.customerHieghtIn);
        this.customerAssessmentObj.customerHieght = this.customerHieght;
      }
      // if(this.isCustomerHieghtPresent === true){
      //   this.customerAssessmentObj.customerHieght = this.customerHieght;
      // }

      //for girth
      if (this.isCustomerNeckPresent === true) {
        let neckStr = this.customerNeck.concat(" ").concat(this.customerNeckUnit);
        this.girthObj.customerNeck = neckStr;
      }
      if (this.isCustomerShoulderPresent === true) {
        let shoulderStr = this.customerShoulder.concat(" ").concat(this.customerShoulderUnit);
        this.girthObj.customerShoulder = shoulderStr;
      }
      if (this.isCustomerChestPresent === true) {
        let chestStr = this.customerChest.concat(" ").concat(this.customerChestUnit);
        this.girthObj.customerChest = chestStr;
      }
      if (this.isCustomerLeftArmPresent === true) {
        let leftArmStr = this.customerLeftArm.concat(" ").concat(this.customerLeftArmUnit);
        this.girthObj.customerLeftArm = leftArmStr;
      }
      if (this.isCustomerRightArmPresent === true) {
        let rightArmStr = this.customerRightArm.concat(" ").concat(this.customerRightArmUnit);
        this.girthObj.customerRightArm = rightArmStr;
      }
      if (this.isCustomerWaistPresent === true) {
        let waistStr = this.customerWaist.concat(" ").concat(this.customerWaistUnit);
        this.girthObj.customerWaist = waistStr;
      }
      if (this.isCustomerHipPresent === true) {
        let hipStr = this.customerHip.concat(" ").concat(this.customerHipUnit);
        this.girthObj.customerHip = hipStr;
      }
      if (this.isCustomerLeftThighPresent === true) {
        let leftThighStr = this.customerLeftThigh.concat(" ").concat(this.customerLeftThighUnit);
        this.girthObj.customerLeftThigh = leftThighStr;
      }
      if (this.isCustomerRightThighPresent === true) {
        let rightThighStr = this.customerRightThigh.concat(" ").concat(this.customerRightThighUnit);
        this.girthObj.customerRightThigh = rightThighStr;
      }
      if (this.isCustomerLeftCalfPresent === true) {
        let leftCalfStr = this.customerLeftCalf.concat(" ").concat(this.customerLeftCalfUnit);
        this.girthObj.customerLeftCalf = leftCalfStr;
      }
      if (this.isCustomerRightCalfPresent === true) {
        let rightCalfStr = this.customerRightCalf.concat(" ").concat(this.customerRightCalfUnit);
        this.girthObj.customerRightCalf = rightCalfStr;
      }
      if (this.isWhrPresent === true) {
        let whrStr = this.whr.concat(" ").concat(this.whrUnit);
        this.girthObj.whr = whrStr;
      }


      //for skinfold
      if (this.ismmAbdominalPresent === true) {
        let mmAbdominalStr = this.mmAbdominal.concat(" ").concat(this.mmAbdominalUnit);
        this.skinFoldObj.mmAbdominal = mmAbdominalStr;
      }
      if (this.ismmBicepPresent === true) {
        let mmBicepStr = this.mmBicep.concat(" ").concat(this.mmBicepUnit);
        this.skinFoldObj.mmBicep = mmBicepStr;
      }
      if (this.ismmCalfPresent === true) {
        let mmCalfStr = this.mmCalf.concat(" ").concat(this.mmCalfUnit);
        this.skinFoldObj.mmCalf = mmCalfStr;
      }
      if (this.ismmChestPresent === true) {
        let mmChestStr = this.mmChest.concat(" ").concat(this.mmChestUnit);
        this.skinFoldObj.mmChest = mmChestStr;
      }
      if (this.ismmMidaxillaryPresent === true) {
        let mmMidaxillaryStr = this.mmMidaxillary.concat(" ").concat(this.mmMidaxillaryUnit);
        this.skinFoldObj.mmMidaxillary = mmMidaxillaryStr;
      }
      if (this.ismmSkinFoldAveragePresent === true) {
        let mmSkinFoldAverageStr = String(this.mmSkinFoldAverage).concat(" ").concat(this.mmSkinFoldAverageUnit);
        this.skinFoldObj.mmSkinFoldAverage = mmSkinFoldAverageStr;
      }
      // if (this.ismmSkinFoldTotalPresent === true) {
      //   let mmSkinFoldTotalStr = String(this.mmSkinFoldTotal).concat(" ").concat(this.mmSkinFoldTotalUnit);
      //   this.skinFoldObj.mmSkinFoldTotal = mmSkinFoldTotalStr;
      // }
      if (this.ismmSubscapularPresent === true) {
        let mmSubscapularStr = this.mmSubscapular.concat(" ").concat(this.mmSubscapularUnit);
        this.skinFoldObj.mmSubscapular = mmSubscapularStr;
      }
      if (this.ismmSuoraillacPresent === true) {
        let mmSuoraillacStr = this.mmSuoraillac.concat(" ").concat(this.mmSuoraillacUnit);
        this.skinFoldObj.mmSuoraillac = mmSuoraillacStr;
      }
      if (this.ismmThighPresent === true) {
        let mmThighStr = this.mmThigh.concat(" ").concat(this.mmThighUnit);
        this.skinFoldObj.mmThigh = mmThighStr;
      }
      if (this.ismmTricepPresent === true) {
        let mmTricepStr = this.mmTricep.concat(" ").concat(this.mmTricepUnit);
        this.skinFoldObj.mmTricep = mmTricepStr;
      }
      this.customerAssessmentObj.girth = this.girthObj;
      this.customerAssessmentObj.skinFold = this.skinFoldObj;

      //this.getcustomerassessment.push(this.customerAssessmentObj);



      console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

      this.presentConfirm();
      //this.postAssessmentInfo();

    }

  }

  //Confirmation alert
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Submit',
      message: 'Do you want to save this assessment?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.getPassAssessmentData(this.username);
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.postAssessmentInfo();

          }
        }
      ]
    });
    alert.present();
  }

  presentConfirmForPastAssessment() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Submit',
      message: 'Do you want to save this assessment?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.getPassAssessmentDataForSelectedDate(this.username, this.selectedDateForPastAssessment);
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.postAssessmentInfo();

          }
        }
      ]
    });
    alert.present();
  }

  postAssessmentInfo() {
    //this.presentLoading();
    this.myProgressProvider.saveAssessmentInfo(this.username, this.customerAssessmentObj).then(res => {
      this.response = res;
      if (this.response.status == 200 && this.response.message === "1") {
        //this.dismissLodaing();
        //this.total = 0;
        this.showAlert("Success", this.response.responseObj.savecustomerassessment)

        this.myProgressProvider.getPastAssessmentdateList(this.username).then(res => {
          //this.presentLoading();
          this.dateList = [];
          this.response = res;
          //if (this.response.responseObj.getcustomerassessment.length > 0) {
          console.log("datessssss::::" + JSON.stringify(this.response));
          for (let dataList of this.response.responseObj.getcustomerassessment) {
            console.log("In function::::" + JSON.stringify(dataList));
            // this.dateList = dataList;
            this.dateList.push(dataList);
          }
          //}
          // else {
          //   this.showAlert("Alert", "Past assessments are not present.");
          // }

        })

        this.getPassAssessmentData(this.username);

      }
      else if (this.response.status == 200 && this.response.message === "2") {
        //this.dismissLodaing();
        this.showAlert("Success", this.response.responseObj.savecustomerassessment)
        //this.total = 0;
        this.myProgressProvider.getPastAssessmentdateList(this.username).then(res => {
          //this.presentLoading();
          this.dateList = [];
          this.response = res;
          //if (this.response.responseObj.getcustomerassessment.length > 0) {
          console.log("datessssss::::" + JSON.stringify(this.response));
          for (let dataList of this.response.responseObj.getcustomerassessment) {
            console.log("In function::::" + JSON.stringify(dataList));
            // this.dateList = dataList;
            this.dateList.push(dataList);
          }
          //}
          // else {
          //   this.showAlert("Alert", "Past assessments are not present.");
          // }
        })

        this.getPassAssessmentData(this.username);

      }

      else if (this.response.status == 200 && this.response.message === "0") {
        //this.dismissLodaing();
        this.showAlert("Success", this.response.responseObj.savecustomerassessment)

        this.myProgressProvider.getPastAssessmentdateList(this.username).then(res => {
          //this.presentLoading();
          this.dateList = [];
          this.response = res;
          //if (this.response.responseObj.getcustomerassessment.length > 0) {
          console.log("datessssss::::" + JSON.stringify(this.response));
          for (let dataList of this.response.responseObj.getcustomerassessment) {
            console.log("In function::::" + JSON.stringify(dataList));
            // this.dateList = dataList;
            this.dateList.push(dataList);
          }
          //}
          // else {
          //   this.showAlert("Alert", "Past assessments are not present.");
          // }
        })

        this.getPassAssessmentData(this.username);



      }
      else {
        this.dismissLodaing();
        this.showAlert("Error", this.response.responseObj.savecustomerassessment)
      }
    })
  }

  //Basic Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }



  //Navigate to Graph page
  goToGraphPage() {
    this.navCtrl.push(TrackProgressPage, {
      assessment: this.getcustomerassessment

    });
  }

  toggleGirthAccordion() {
    this.isGirthAccordionExpanded = !this.isGirthAccordionExpanded;
  }

  toggleSkinFoldsAccordion() {
    this.isSkinFoldsAccordionExpanded = !this.isSkinFoldsAccordionExpanded;
  }

  toggleOtherInfoAccordion() {
    this.isOtherInfoAccordionExpanded = !this.isOtherInfoAccordionExpanded;
  }

  goToPage() {
    console.log("goToPage()");
  }


  //Activity Indicator
  presentLoading() {
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

}
