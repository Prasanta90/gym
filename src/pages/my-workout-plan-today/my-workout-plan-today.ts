import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';
import { MoreInfoAboutExercisePage } from '../more-info-about-exercise/more-info-about-exercise';
// import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from '../../Model/common-response';
import { WorkoutProvider } from '../../providers/workout/workout';
import { UpdateCustomerExerciseList } from '../../Model/update-customer-exercise-list';
import { UpdatedCustomerExercise } from '../../Model/updated-customer-exercise-list';
import { WorkoutPlanCount } from '../../Model/workout-plan-count';
import { HometabsPage } from '../hometabs/hometabs';


/**
 * Generated class for the MyWorkoutPlanTodayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-workout-plan-today',
  templateUrl: 'my-workout-plan-today.html',
})
export class MyWorkoutPlanTodayPage {

  // selectedDate: Date;
  // workouts: Array<any>;
  // supersetWorkouts: any;
  // isChecked: boolean = false;
  // previousDate: any;
  // isCheckedSingleCard: boolean = true;
  // countTotalChecked: number = 0;
  // menuTitle: string;
  // username: string;

  menuTitle: string;
  loader: any;
  response: CommonResponse;
  username: string;
  selectedDate: Date;
  byDate: string;
  workouts: any[] = [];
  supersetWorkouts: any[] = [];
  isChecked: boolean = false;
  previousDate: any;
  isCheckedSingleCard: boolean = true;
  countTotalChecked: number = 0;
  singleChecked: number = 0;
  keySets = [];
  exerciseList: any[] = [];
  isCheckedSuperset: boolean = false;
  isCheckedSingle: boolean = false;
  countSingleExercise: number = 0;
  countSetExercise: number = 0;
  updateCustomerExerciseObj: UpdateCustomerExerciseList;
  updateCustomerExerciseObjSingle: UpdateCustomerExerciseList;
  updateCustomerExerciseObjMultiple: UpdateCustomerExerciseList;
  updateCustomerExerciseList: UpdateCustomerExerciseList[] = [];
  updateCustomerExerciseListSingle: UpdateCustomerExerciseList[] = [];
  updateCustomerExerciseListMultiple: UpdateCustomerExerciseList[] = [];
  updatedCustomerExercise: UpdatedCustomerExercise = new UpdatedCustomerExercise();
  selectAllTotalList: any[] = [];

  workoutImages: Array<any>;
  //i: number = 0;
  WorkoutPlanCountList: WorkoutPlanCount[] = [];
  isNavigateFromCalendar: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public workoutProvider: WorkoutProvider, public alertCtrl: AlertController,
    public activityLoader: LoadingController, public events: Events) {

    this.username = localStorage.getItem('username')
    this.menuTitle = navParams.get('menuTitle');
    console.log("menu title:::::" + this.menuTitle);
    this.selectedDate = navParams.get('selectedDate');
    console.log("this.selectedDatemy-workout-plan-today:::::"+this.selectedDate);

    if(this.selectedDate == undefined || this.selectedDate == null){
      this.selectedDate = new Date();
    }
    this.isNavigateFromCalendar = navParams.get('isNavigateFromCalendar');


    if(this.isNavigateFromCalendar === true){
      this.byDate = String(this.selectedDate);
    }
    else{
      this.byDate = this.formatDate(this.selectedDate);
    }
    
    console.log("selected date::::" + this.byDate);

    this.getCustomerWorkoutPlan(this.username, this.byDate);

   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyWorkoutPlanTodayPage');
  }

  //to convert date to string format
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  //get my workout plan list
  getCustomerWorkoutPlan(username, selectedDate) {
    this.countSetExercise = 0;
    this.countSingleExercise = 0;
    this.countTotalChecked = 0;
    var map1 = new Map();
    this.workoutProvider.getCustomerWorkoutPlan(username, selectedDate).then(res => {
      this.response = res;
      console.log("workout list data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {

        if (typeof this.response.responseObj.customerworkoutplan.ListOfSet !== 'undefined') {
          var keyObj = this.response.responseObj.customerworkoutplan.ListOfSet;
          this.keySets = Object.keys(keyObj);
          console.log('obj contains ' + this.keySets.length + ' keys: ' + this.keySets);
          this.exerciseList = [];
          for (let key of this.keySets) {
            console.log(key)
            console.log(JSON.stringify(this.response.responseObj.customerworkoutplan.ListOfSet[key]));
            this.exerciseList.push(this.response.responseObj.customerworkoutplan.ListOfSet[key]);
          }
          console.log("this.exerciseList length>>>><<<<" + JSON.stringify(this.exerciseList.length));
          console.log("this.exerciseList>>>><<<<" + JSON.stringify(this.exerciseList));

          if (this.exerciseList.length > 0) {
            this.supersetWorkouts = [];
            this.workouts = [];
            let i = 0;
            for (let exercise of this.exerciseList) {
              if (exercise.length > 0) {
                console.log("exercise***>>>***<<<<<" + JSON.stringify(exercise));
                //this.supersetWorkouts = exercise;
                this.supersetWorkouts[i] = exercise;
                i++;
                // for(let i=0; i<exercise.length; i++){
                //   this.supersetWorkouts[i] = exercise;
                // }
                //Array.prototype.push.apply(this.supersetWorkouts,exercise);
                this.isCheckedSuperset = exercise[0].checkedCompleted;
                console.log("exercise.checkedCompleted" + exercise[0].checkedCompleted);
                if (this.isCheckedSuperset) {
                  this.countSetExercise = this.countSetExercise + 1;
                  console.log("this.countExercise for superset::::" + this.countSetExercise);
                }
              }
              else {
                for (let exe of exercise) {
                  console.log("iiiiii" + JSON.stringify(exe));
                  this.workouts.push(exe);
                  this.isCheckedSingle = exe.checkedCompleted;
                  if (this.isCheckedSingle) {
                    this.countSingleExercise = this.countSingleExercise + 1;
                    console.log("this.countExercise for single set::::" + this.countSingleExercise);
                  }
                }

              }


            }
            console.log(" this.supersetWorkouts::::888&&&%%%" + JSON.stringify(this.supersetWorkouts));
            console.log("i::::" + i);
            this.countTotalChecked = this.countSetExercise + this.countSingleExercise;

          }
          else {
            this.supersetWorkouts = [];
            this.workouts = [];
            this.showAlert("Alert", "Workout plan for current date is not added by your trainer. Kindly reach out to our helpdesk.");
          }

        }
      }
    })
  }



  swipeEvent(e) {
    if (e.direction == 2) {
      console.log('swiped left');
      this.nextButton(this.selectedDate)

    } else if (e.direction == 4) {
      console.log('swiped right');
      this.previousButton(this.selectedDate);

    }

  }

  //to count checked workout
  checkedCount(exercise) {
    //console.log("exercise.scheduleDate########"+exercise.scheduleDate);
    this.updateCustomerExerciseObj = new UpdateCustomerExerciseList();
    this.updateCustomerExerciseObj.customerId = exercise.customerId;
    this.updateCustomerExerciseObj.superSetId = exercise.superSetId;
    this.updateCustomerExerciseObj.scheduleDate = exercise.scheduledDate;
    this.updateCustomerExerciseObj.forDay = exercise.forDay;
    this.updateCustomerExerciseObj.checkedCompleted = exercise.checkedCompleted;
    //this.updateCustomerExerciseObj.username = this.username;
    this.updateCustomerExerciseList.push(this.updateCustomerExerciseObj);
    console.log("exercise details when checked::::" + JSON.stringify(exercise));
    let isCheckedSingle = exercise.checkedCompleted;
    if (isCheckedSingle) {
      this.countSingleExercise = this.countSingleExercise + 1;
      this.countTotalChecked = this.countSetExercise + this.countSingleExercise;
      console.log("this.countExercise for single set &&&&::::" + this.countSingleExercise);

    }
    else if (isCheckedSingle === false) {
      this.countSingleExercise = this.countSingleExercise - 1;
      this.countTotalChecked = this.countSetExercise + this.countSingleExercise;
    }

  }

  checkedCountForSuperSet(supersetWorkouts, isCheckedSuperset) {
    console.log("isCheckedSuperset****" + isCheckedSuperset);
    this.updateCustomerExerciseObj = new UpdateCustomerExerciseList();
    for (let setWorkout of supersetWorkouts) {
      this.updateCustomerExerciseObj.customerId = setWorkout.customerId;
      this.updateCustomerExerciseObj.superSetId = setWorkout.superSetId;
      this.updateCustomerExerciseObj.scheduleDate = setWorkout.scheduledDate;
      this.updateCustomerExerciseObj.forDay = setWorkout.forDay;
      this.updateCustomerExerciseObj.checkedCompleted = isCheckedSuperset;
      //this.updateCustomerExerciseObj.username = this.username;
    }

    this.updateCustomerExerciseList.push(this.updateCustomerExerciseObj);
    console.log("supersetWorkouts::::" + JSON.stringify(supersetWorkouts));
    if (isCheckedSuperset) {
      this.countSetExercise = this.countSetExercise + 1;
      this.countTotalChecked = this.countSetExercise + this.countSingleExercise;
    }
    else if (isCheckedSuperset === false) {
      this.countSetExercise = this.countSetExercise - 1;
      this.countTotalChecked = this.countSetExercise + this.countSingleExercise;
    }
  }

  viewMoreInfoAboutExerciseSuperset(exercise, isCheckedSuperset) {
    //console.log(isCheckedSuperset)
    console.log("exercise for superset******" + JSON.stringify(exercise));
    this.navCtrl.push(MoreInfoAboutExercisePage, { category: exercise });

  }

  //get previous date
  previousButton(selectedDate) {
    var previousDay: any;
    console.log("selectedDate for previous button:::::"+selectedDate);
    
    if(this.isNavigateFromCalendar === true){
      var splitDate = selectedDate.split("-");
      this.selectedDate = splitDate[0].concat("-").concat(splitDate[1]).concat("-").concat(splitDate[2] - 1);
      console.log("selected date::::" + this.selectedDate);
      this.getCustomerWorkoutPlan(this.username, this.selectedDate);
    //this.byDate = String(this.selectedDate);
    }
    else{
      var day = new Date(selectedDate);
      console.log("selectedDate::::" + day);
  
      previousDay = new Date(day);
      previousDay.setDate(day.getDate() - 1);
      this.selectedDate = previousDay;
      console.log("Previous day in else::::"+previousDay);
      this.byDate = this.formatDate(this.selectedDate);
      console.log("selected date::::" + this.byDate);
      this.getCustomerWorkoutPlan(this.username, this.byDate);
    }
    
  }

  //get next date
  nextButton(selectedDate) {
    console.log("selectedDate for next button:::::"+selectedDate);
    
    if(this.isNavigateFromCalendar === true){
      var splitDate = selectedDate.split("-");
      var nextDate = Number(splitDate[2]) + 1;
      console.log("next date:::::"+nextDate);
      this.selectedDate = splitDate[0].concat("-").concat(splitDate[1]).concat("-").concat(nextDate);
      console.log("selected date::::" + this.selectedDate);
      this.getCustomerWorkoutPlan(this.username, this.selectedDate);
    //this.byDate = String(this.selectedDate);
    }
    else{
      var day = new Date(selectedDate);
      console.log("selectedDate::::" + day);
  
      var nextDay = new Date(day);
      nextDay.setDate(day.getDate() + 1);
      console.log("next day of the selected day" + nextDay);
      //let date = new Date();
      //console.log(date.setDate(selectedDate.getDate() + 1));
      this.selectedDate = nextDay;
  
      this.byDate = this.formatDate(this.selectedDate);
      console.log("selected date::::" + this.byDate);
  
      this.getCustomerWorkoutPlan(this.username, this.byDate);
    }
  }

  onCheckMultiple(selectedAll) {
    console.log(JSON.stringify(selectedAll));
    if (selectedAll != null && selectedAll != 0) {
      console.log("selected");
      this.isChecked = true;
      this.countTotalChecked = this.workouts.length + this.supersetWorkouts.length;
    }
    //  let updateSingleCustomerExerciseList: UpdateCustomerExerciseList[] =[];

    for (let selectedWorkout of this.workouts) {
      this.updateCustomerExerciseObjSingle = new UpdateCustomerExerciseList();
      this.updateCustomerExerciseObjSingle.customerId = selectedWorkout.customerId;
      this.updateCustomerExerciseObjSingle.superSetId = selectedWorkout.superSetId;
      this.updateCustomerExerciseObjSingle.scheduleDate = selectedWorkout.scheduledDate;
      this.updateCustomerExerciseObjSingle.forDay = selectedWorkout.forDay;
      this.updateCustomerExerciseObjSingle.checkedCompleted = "true";
      //this.updateCustomerExerciseObj.username = this.username;
      this.updateCustomerExerciseListSingle.push(this.updateCustomerExerciseObjSingle);
      console.log("SELECT ALLLLLL LIst::::" + JSON.stringify(this.updateCustomerExerciseListSingle));
    }


    for (let selectedWorkout of this.supersetWorkouts) {
      for (let workout of selectedWorkout) {
        this.updateCustomerExerciseObjMultiple = new UpdateCustomerExerciseList();
        this.updateCustomerExerciseObjMultiple.customerId = workout.customerId;
        this.updateCustomerExerciseObjMultiple.superSetId = workout.superSetId;
        this.updateCustomerExerciseObjMultiple.scheduleDate = workout.scheduledDate;
        this.updateCustomerExerciseObjMultiple.forDay = workout.forDay;
        this.updateCustomerExerciseObjMultiple.checkedCompleted = "true";
        //this.updateCustomerExerciseObj.username = this.username;
        this.updateCustomerExerciseListMultiple.push(this.updateCustomerExerciseObjMultiple);
        console.log("SELECT ALLLLLL LIst::::" + JSON.stringify(this.updateCustomerExerciseListMultiple));
      }
    }

    this.updateCustomerExerciseList = this.updateCustomerExerciseListSingle.concat(this.updateCustomerExerciseListMultiple);
    console.log("SELECT ALLLLLL::::" + JSON.stringify(this.updateCustomerExerciseList));


  }

  viewMoreInfoAboutExercise(category) {
    console.log("Category details:::::" + JSON.stringify(category));
    this.navCtrl.push(MoreInfoAboutExercisePage, { category: category });
  }

  saveLog() {
    this.updatedCustomerExercise.updateCustomerExerciseList = this.updateCustomerExerciseList
    console.log("Save changed exercise::::::" + JSON.stringify(this.updatedCustomerExercise));
    this.presentLoading();
    this.workoutProvider.postUpdatedCustomerExercise(this.updatedCustomerExercise).then(res => {
      this.response = res;
      console.log("this.response*****" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message == "success") {
        //this.dismissLodaing();
        //this.showAlert("Success", this.response.responseObj.updatecustomerexercise)
        //this.getWorkoutDetailByDate(this.username, this.selectedDate);
        this.getWorkoutList(this.username);
        this.dismissLodaing();
        const alert = this.alertCtrl.create({
          title: "Success",
          subTitle: this.response.responseObj.updatecustomerexercise,
          buttons: [
            {
              text: 'OK',
              handler: () =>{
                if(this.countTotalChecked == this.exerciseList.length){
                  this.navCtrl.push(HometabsPage);
                }
                
                this.events.publish('selectedDate', this.selectedDate);
              }
            }
          ]
        });
        alert.present();

      }
      else {
        this.dismissLodaing();
        this.showAlert("Error", this.response.responseObj.updatecustomerexercise)
      }
    })
  }

  getWorkoutList(username) {
    this.workoutProvider.getWorkoutPlanDetails(username).then(res => {
      this.response = res;
      console.log("workout data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        console.log("this.response in my-workout-plan:::::" + JSON.stringify(this.response));
        this.events.publish("shareObject", this.response);
      }
    })
  }



  //Activity Indicator
  presentLoading() {
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }


}
