import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyCalenderResponseModel, CalenderEventArrayModel } from '../../Model/myCalendarModel';
//Ipsita
import { MoreInfoAboutExercisePage } from '../more-info-about-exercise/more-info-about-exercise';
import { JsonPipe } from '@angular/common';
import { ModalController } from 'ionic-angular';
import { WorkoutDetailsPage } from '../../pages/workout-details/workout-details';
//@IonicPage()
@Component({
  selector: 'page-today-s-workout',
  templateUrl: 'today-s-workout.html',
})
export class TodaySWorkoutPage {

  workouts: Array<any>;
  currentEvents: any[] = [];
  todaysDate: Date = new Date();




  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.currentEvents = [
      {
        year: 2019,
        month: 0,
        date: 12,

      },
      {
        year: 2019,
        month: 0,
        date: 15,

      },
      {
        year: 2019,
        month: 0,
        date: 20,

      }
    ];
  }

  ionViewDidLoad() {

    //Ipsita
    this.workouts = [
      { id: 1, name: "Workout I", description:"Number of exercise - 06" },
      { id: 2, name: "Workout II", description:"Number of exercise - 06"},
      { id: 3, name: "Workout III", description:"Number of exercise - 06"},
      { id: 4, name: "Workout IV", description:"Number of exercise - 06" },
      { id: 5, name: "Workout V", description:"Number of exercise - 06" },
      { id: 6, name: "Workout VI", description:"Number of exercise - 06"}];
  }


  //Ipsita
  onDaySelect(event) {
    console.log("Date selected::::" + JSON.stringify(event));
    console.log("Date clicked");
    this.currentEvents.push({
      year: event.year,
      month: event.month,
      date: event.date
    });
    for (let event of this.currentEvents) {
      console.log("event" + JSON.stringify(event));
    }
  }

  openNavDetailsPage(workoutDetails) {
    console.log("workoutDetails:::::" + JSON.stringify(workoutDetails));
    this.navCtrl.push(WorkoutDetailsPage, { workout: workoutDetails });
    
  }

  



}
