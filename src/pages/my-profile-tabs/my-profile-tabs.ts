import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { MyProfilePage } from '../my-profile/my-profile';
//import { InboxPage } from '../inbox/inbox';
import { NotificationsPage } from '../notifications/notifications';
import { HealthInfoPage } from '../health-info/health-info';
import { PaymentsDuesPage } from '../payments-dues/payments-dues';
import { TodaySWorkoutNewPage} from '../today-s-workout-new/today-s-workout-new';

/**
 * Generated class for the MyProfileTabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-profile-tabs',
  templateUrl: 'my-profile-tabs.html'
})
export class MyProfileTabsPage {

  profileRoot = MyProfilePage
  //inboxRoot = InboxPage
  notificationsRoot = NotificationsPage
  healthInfoRoot = HealthInfoPage
  paymentsDuesRoot = PaymentsDuesPage
  todaySWorkoutPage = TodaySWorkoutNewPage


  constructor(public navCtrl: NavController) {}

}
