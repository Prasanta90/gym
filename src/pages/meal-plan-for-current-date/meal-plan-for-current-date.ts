import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { CommonResponse } from '../../Model/common-response';
import { MyMealPlanProvider } from '../../providers/my-meal-plan/my-meal-plan';
import { MealDetail } from '../../Model/meal-detail-model';
import { MyMealPlan } from '../../Model/my-meal-plan-model';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { InAppBrowser } from '@ionic-native/in-app-browser';
/**
 * Generated class for the MealPlanForCurrentDatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-meal-plan-for-current-date',
  templateUrl: 'meal-plan-for-current-date.html',
})
export class MealPlanForCurrentDatePage {

  selectedSegment: string = "plan";
  // nutritionPlans: Array<any>;
  selectedDate: Date;
  byDate: string;
  username: string;
  password: string;
  response: CommonResponse;
  loader: any;
  myMealPlan: MyMealPlan;
  Name: string;
  Desc: string;
  mealDetails: MealDetail[] = [];
  pdfPath: string;
  defaultPath: string = "http://182.73.216.93:8091/gymadminrest";
  meal_icon: any[] = [];
  mealDetailObj: MealDetail = new MealDetail();
  isNavigateFromCalendar: boolean;


  // currDate: Date = new Date();

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public myMealPlanProvider: MyMealPlanProvider, public activityLoader: LoadingController, public alertCtrl: AlertController,
    private fileOpener: FileOpener, private document: DocumentViewer, private file: File, private transfer: FileTransfer,
    private platform: Platform, private iab: InAppBrowser) {

    this.meal_icon = [
      { "mealImg": "/assets/icon/mealPlanIcon/icon1.png" },
      { "mealImg": "/assets/icon/mealPlanIcon/icon2.png" },
      { "mealImg": "/assets/icon/mealPlanIcon/icon3.png" },
      { "mealImg": "/assets/icon/mealPlanIcon/icon2.png" },
      { "mealImg": "/assets/icon/mealPlanIcon/icon3.png" },
      { "mealImg": "/assets/icon/mealPlanIcon/icon1.png" },
      { "mealImg": "/assets/icon/mealPlanIcon/icon2.png" },
      { "mealImg": "/assets/icon/mealPlanIcon/icon3.png" }

    ]

    this.selectedDate = navParams.get('selectedDate');
    console.log("this.selectedDatemy-meal-plan-today:::::" + this.selectedDate);
    if (this.selectedDate == undefined || this.selectedDate == null) {
      this.selectedDate = new Date();
    }

    this.isNavigateFromCalendar = navParams.get('isNavigateFromCalendar');

    if(this.isNavigateFromCalendar === true){
      this.byDate = String(this.selectedDate);
    }
    else{
      this.byDate = this.formatDate(this.selectedDate);
    }

    //this.byDate = this.formatDate(this.selectedDate);
    console.log("selected date::::" + this.byDate);

    this.username = localStorage.getItem('username');
    console.log("this.username in nutrition plan::::" + this.username);

    this.password = localStorage.getItem('password');
    console.log("this.password in nutrition plan::::" + this.password);

    this.getMyPlanDetail(this.username, this.byDate);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NutritionPlanPage');
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
  }

  swipeEvent(e) {
    if (e.direction == 2) {
      console.log('swiped left');
      this.nextButton(this.selectedDate)

    } else if (e.direction == 4) {
      console.log('swiped right');
      this.previousButton(this.selectedDate);

    }

  }

  //to convert date to string format
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  //get previous date
  previousButton(selectedDate) {

    var previousDay: any;
    console.log("selectedDate for previous button:::::"+selectedDate);
    
    if(this.isNavigateFromCalendar === true){
      var splitDate = selectedDate.split("-");
      this.selectedDate = splitDate[0].concat("-").concat(splitDate[1]).concat("-").concat(splitDate[2] - 1);
      console.log("selected date::::" + this.selectedDate);
      this.getMyPlanDetail(this.username, this.selectedDate);
    //this.byDate = String(this.selectedDate);
    }
    else{
      var day = new Date(selectedDate);
    console.log("selectedDate::::" + day);

    previousDay = new Date(day);
    previousDay.setDate(day.getDate() - 1);
    console.log("previous day of the selected day" + previousDay);
    //let date = new Date();
    //console.log(date.setDate(selectedDate.getDate() + 1));
    this.selectedDate = previousDay;
    this.byDate = this.formatDate(this.selectedDate);
    this.getMyPlanDetail(this.username, this.byDate);
    }

    
  }

  //get next date
  nextButton(selectedDate) {
    console.log("selectedDate for next button:::::"+selectedDate);
    
    if(this.isNavigateFromCalendar === true){
      var splitDate = selectedDate.split("-");
      var nextDate = Number(splitDate[2]) + 1;
      console.log("next date:::::"+nextDate);
      this.selectedDate = splitDate[0].concat("-").concat(splitDate[1]).concat("-").concat(nextDate);
      console.log("selected date::::" + this.selectedDate);
      this.getMyPlanDetail(this.username, this.selectedDate);
    //this.byDate = String(this.selectedDate);
    }
    else{
      var day = new Date(selectedDate);
      console.log("selectedDate::::" + day);
  
      var nextDay = new Date(day);
      nextDay.setDate(day.getDate() + 1);
      console.log("next day of the selected day" + nextDay);
      //let date = new Date();
      //console.log(date.setDate(selectedDate.getDate() + 1));
      this.selectedDate = nextDay;
  
      this.byDate = this.formatDate(this.selectedDate);
      console.log("selected date::::" + this.byDate);
  
      this.getMyPlanDetail(this.username, this.byDate);
    }

    // var day = new Date(selectedDate);
    // console.log("selectedDate::::" + day);

    // var nextDay = new Date(day);
    // nextDay.setDate(day.getDate() + 1);
    // console.log("next day of the selected day" + nextDay);
    // //let date = new Date();
    // //console.log(date.setDate(selectedDate.getDate() + 1));
    // this.selectedDate = nextDay;
    // this.byDate = this.formatDate(this.selectedDate);
    // this.getMyPlanDetail(this.username, this.byDate);
  }

  

  getMyPlanDetail(username, byDate) {
    //this.presentLoading();
    this.myMealPlanProvider.getNutritionMealPlanDetails(username, byDate).then(res => {
      this.response = res;
      console.log("assessment data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        //this.dismissLodaing();
        if (this.response.responseObj.getnutritionmealplandetails !== null && this.response.responseObj.getnutritionmealplandetails.mealDetails.length > 0) {
          console.log("this.response.responseObj.getnutritionmealplandetails::::" + JSON.stringify(this.response.responseObj.getnutritionmealplandetails));
          this.myMealPlan = this.response.responseObj.getnutritionmealplandetails;
          this.Name = this.myMealPlan.Name;
          console.log("this.Name::::" + this.Name);
          this.Desc = this.myMealPlan.Desc;
          if (this.myMealPlan.mealDetails.length > 0) {
            //this.foodItemToView = new ;
            //this.mealDetailsObj = new MealDetail();
            this.mealDetails = [];
            for (let mealDtl of this.myMealPlan.mealDetails) {

              this.mealDetails.push(mealDtl);
              //console.log("outer this.mealDetails"+JSON.stringify(this.mealDetails));

            }
          }


        }
        else {
          this.response.responseObj.getnutritionmealplandetails == null;
          this.Name = null;
          this.Desc = null;
          this.mealDetails = [];

          this.showAlert("Alert", " Meal plan for current date is not added by your trainer. Kindly reach out to our helpdesk.");
        }
      }
      else {
        this.showAlert("Error", "Sorry! Server side error has occurred.");
      }
    })

  }

  generatePDF() {
    this.myMealPlanProvider.getPdf(this.username, this.password).then(res => {
      this.response = res;
      console.log("assessment data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        //this.pdfPath =  this.response.responseObj.getnutritionplanpdf;

        this.pdfPath = this.defaultPath.concat(this.response.responseObj.getnutritionplanpdf)
        console.log("this.pdfPath::::" + this.pdfPath);

        let url = encodeURIComponent(this.pdfPath);
        this.iab.create('https://docs.google.com/viewer?url=' + url);


      }
    })

  }




  //Activity Indicator
  presentLoading() {
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
