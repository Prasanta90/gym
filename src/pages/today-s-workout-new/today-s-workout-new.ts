import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events, Slides, ModalController } from 'ionic-angular';
import { MyCalenderPage } from '../../pages/my-calender/my-calender';
import { NutritionPlanPage } from '../../pages/nutrition-plan/nutrition-plan';
import { WorkoutDetailNewPage } from '../../pages/workout-detail-new/workout-detail-new';
import { MyWorkoutPlanPage } from '../../pages/my-workout-plan/my-workout-plan';
import { NotificationsPage } from '../../pages/notifications/notifications';
import { Chart } from 'chart.js';
import { Myprofile } from '../../Model/my-profile';
import { MyApp } from '../../app/app.component';
import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from '../../Model/common-response';
import { MyMealPlanProvider } from '../../providers/my-meal-plan/my-meal-plan';
import { TopPicksDetailPage } from '../../pages/top-picks-detail/top-picks-detail';
import { Workout } from '../../Model/workout';
import { WorkoutProvider } from '../../providers/workout/workout';
import { WorkoutPlanCount } from '../../Model/workout-plan-count';
import { ClubWorkoutPlan } from '../../Model/club-wowkout-plan';
import { SwipeSegmentDirective } from '../directives/swipe-segment/swipe-segment';
import { ClubWorkoutDescriptionPage } from '../../pages/club-workout-description/club-workout-description';
import { MyProgressPage } from '../../pages/my-progress/my-progress';
import { MealPlanForCurrentDatePage } from '../../pages/meal-plan-for-current-date/meal-plan-for-current-date';
import { MyWorkoutPlanTodayPage } from '../../pages/my-workout-plan-today/my-workout-plan-today';


/**
 * Generated class for the TodaySWorkoutNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-today-s-workout-new',
  templateUrl: 'today-s-workout-new.html',
})
export class TodaySWorkoutNewPage {

  @ViewChild('doughnutChart') doughnutChart;
  @ViewChild('swipedTabsSlider') swipedTabsSlider: Slides;

  SwipedTabsIndicator: any = null;
  //selectedSegment: string = "workout";
  selectedSegment: string;
  tabs: Array<string> = ['workout', 'nutrition', 'top picks']
  menuTitle: string;
  loader: any;
  isSelected: boolean = false;
  cDate: Date = new Date();
  //loadProgress: number = 0;
  workout: Workout;
  workoutList: Workout[] = [];
  dateArray = new Array();
  totalWorkouts: number = 60;
  workoutCompleted: number = 20;
  totalMeal: number = 20;
  name: string = " ";
  response: CommonResponse;
  max = 100;
  current = 90;
  performancePrecentage: any;
  performanceName: string;

  user: Myprofile;
  username: string;
  firstNm: string;
  lastNm: string;
  defaultImageUrl: string = "/assets/imgs/64-64.png";
  customerImageUrl: string;

  ScheduleDateWiseMealount: any[] = [];
  WorkoutPlanCountList: WorkoutPlanCount[] = [];
  clubWorkoutPlanList: ClubWorkoutPlan[];

  //for demo
  topPicksImages: Array<any>;
  workoutImages: any[];
  customerGoal: any;



  constructor(public navCtrl: NavController, public navParams: NavParams,
    public commonProvider: CommonProvider, public activityLoader: LoadingController,
    public myMealPlanProvider: MyMealPlanProvider, public alertCtrl: AlertController,
    public events: Events, public workoutProvider: WorkoutProvider, public modalCtrl: ModalController) {
      // if (this.menuTitle === "Fitness Arena") {
      //   this.getCustomerPerformanceForWeek(this.username);
      // }

  }

  ionViewDidLoad() {
    this.menuTitle = this.navParams.get('menuTitle');
    console.log("menu title:::::" + JSON.stringify(this.menuTitle));
    
    
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter TodaySWorkoutNewPage');
    
    // if (this.menuTitle === "Club Workouts") {
    //   this.getClubWorkoutPlanList(this.username);
    // }
    this.workoutImages = [];
    this.workoutImages = [
      { id: 1, imageurl: "../../assets/imgs/WorkoutImages/5.png" },
      { id: 2, imageurl: "../../assets/imgs/WorkoutImages/6.png" },
      { id: 3, imageurl: "../../assets/imgs/WorkoutImages/7.png" },
      { id: 4, imageurl: "../../assets/imgs/WorkoutImages/5.png" },
      { id: 5, imageurl: "../../assets/imgs/WorkoutImages/6.png" },
      { id: 6, imageurl: "../../assets/imgs/WorkoutImages/7.png" },
      { id: 7, imageurl: "../../assets/imgs/WorkoutImages/5.png" }
    ]



    this.username = localStorage.getItem('username');
    console.log("SElected segment:::::" + this.selectedSegment);

    if (this.menuTitle !== undefined) {
      var selectedSegment: number;
      if (this.menuTitle === "Fitness Arena") {
        this.selectedSegment = "0";
        selectedSegment = 0;
        this.getWorkoutList(this.username);
        this.selectTab(selectedSegment);
      }

      else if (this.menuTitle === "Club Workouts") {
        this.selectedSegment = "2";
        selectedSegment = 2;
        //this.getClubWorkoutPlanList(this.username);
        this.selectTab(selectedSegment);
      }
      else {
        this.selectedSegment = "1";
        selectedSegment = 1;
        //this.getClubWorkoutPlanList(this.username);
        this.selectTab(selectedSegment);
      }
    }
    else {
      this.selectedSegment = "0";
      this.getWorkoutList(this.username);
    }
    if (this.menuTitle === "Fitness Arena") {
        this.getCustomerPerformanceForWeek(this.username);
        console.log("I'm in ionViewWillEnter()::::");
       }
       else
       {
        this.getCustomerPerformanceForWeek(this.username);
        console.log("I'm in ionViewWillEnter()::::");
       }
    this.getCustomerInfo(this.username);
   
    this.getGoalName(this.username);
    



  }


  //Get goal name
  getGoalName(username) {
    this.workoutProvider.getGoalName(username).then(res => {
      this.response = res;
      console.log("Goal Name::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        this.customerGoal = this.response.responseObj.getCustomerGoal;
      }


    })
  }


  //swiped tabs
  selectTab(index) {
    this.swipedTabsSlider.slideTo(index, 500);
    console.log("selectedSEgment in selectTab() is::::" + this.selectedSegment);
    console.log("Index is::::" + index);
    console.log("type of index:::::" + typeof index);
    if (index === 0) {
      console.log("I'm in if, index no::::" + index);
      this.menuTitle = "Fitness Arena";

      // this.getWorkoutList(this.username);

      console.log("Im in selectTab(), getWorkoutList() is");
    }
    else if (index === 1) {
      console.log("I'm in else if, index no::::" + index);
      this.menuTitle = "My Meal Plan";
      //  this.getMealList(this.username);
      console.log("Im in selectTab(), getMealList() is");
    }
    else if (index === 2) {
      console.log("I'm in else if, index no::::" + index);
      this.menuTitle = "Club Workouts";
      // this.getClubWorkoutPlanList(this.username);  
    }
  }

  moveButton($event) {
    this.selectedSegment = $event._snapIndex.toString();

    console.log("I'm in movebutton(), selected segment is:::::" + this.selectedSegment);
    console.log("I'm in movebutton(), type of selected segment is:::::" + typeof this.selectedSegment);
    console.log("######" + this.selectedSegment)
    if (this.selectedSegment === "0") {
      //   this.selectTab(this.selectedSegment);
      this.menuTitle = "Fitness Arena";
      this.getWorkoutList(this.username);
      console.log("Im in moveButton(), getWorkoutList() is");
    }
    else if (this.selectedSegment === "1") {
      //  this.selectTab(this.selectedSegment);
      this.menuTitle = "My Meal Plan";
      this.getMealList(this.username);
      console.log("Im in moveButton(), getMealList() is");
    }
    else if (this.selectedSegment === "2") {
      //   this.selectTab(this.selectedSegment);
      this.menuTitle = "Club Workouts";
      this.getClubWorkoutPlanList(this.username);
    }

  }



  //To get club workout details
  getClubWorkoutPlanList(username) {
    this.workoutProvider.getClubWorkoutPlanList(username).then(res => {
      this.response = res;
      console.log("club workout list data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        if (typeof this.response.responseObj.getClubWorkoutPlans !== 'undefined' && this.response.responseObj.getClubWorkoutPlans.length > 0) {
          let i = 0;
          this.clubWorkoutPlanList = [];
          for (let plan of this.response.responseObj.getClubWorkoutPlans) {
            let clubWorkoutPlanObj = new ClubWorkoutPlan();
            clubWorkoutPlanObj.planId = plan.planId;
            clubWorkoutPlanObj.workoutDesc = plan.workoutDesc;
            clubWorkoutPlanObj.workoutName = plan.workoutName;
            //clubWorkoutPlanObj.img = this.topPicksImages[i].imageurl;
            this.clubWorkoutPlanList.push(clubWorkoutPlanObj);
            i++;

          }
          console.log("clubWorkoutList>>>>" + JSON.stringify(this.clubWorkoutPlanList));

        }
      }


    })
  }


  //to get customer Performance Percentage For month
  getCustomerPerformanceForWeek(username) {
    //this.ççƒ√ = 0;
    this.commonProvider.getCustomerPerformanceForWeek(username).then(res => {
      this.response = res;
      console.log("Performance percentage:::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message.toLowerCase() === "success") {
        if (typeof this.response.responseObj.getcustomerperformanceforweek !== 'undefined' && this.response.responseObj.getcustomerperformanceforweek.length > 0) {
          for (let performanceList of this.response.responseObj.getcustomerperformanceforweek) {
            console.log("performanceList.completionPercentage:::" + performanceList.completionPercentage);
            this.performancePrecentage = performanceList.completionPercentage;
            console.log("performancePrecentage:::" + this.performancePrecentage);
            if (this.performancePrecentage > 80) {
              this.performanceName = "Excellent";
            }
            else if (this.performancePrecentage >= 60 && this.performancePrecentage <= 80) {
              this.performanceName = "Good";
            }
            else {
              this.performanceName = "Marginal";
            }
          }
        }
      }
    })
  }


  //to get customer information
  getCustomerInfo(username) {
    //this.presentLoading();
    this.commonProvider.getCustomerDetails(username).then(res => {
      this.response = res;
      console.log(JSON.stringify(this.response));
      this.user = this.response.responseObj.myprofile;
      console.log("After login: " + JSON.stringify(res));
      if (this.user != null && this.response.status == 200) {
        this.events.publish('user:created', this.user);
        this.firstNm = this.user.firstNm;
        this.lastNm = this.user.lastNm;
        if (this.firstNm != null && this.lastNm != null) {
          this.name = this.firstNm.concat(' ').concat(this.lastNm);
        }
        if (this.user.customerImage != null) {
          this.customerImageUrl = "data:image/png;base64," + this.user.customerImage;
        }
        //this.dismissLodaing();
      }
    })
  }

  getWorkoutList(username) {


    this.workoutProvider.getWorkoutPlanDetails(username).then(res => {
      this.response = res;
      console.log("workout data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message.toLowerCase() === "success") {

        if (typeof this.response.responseObj.getworkoutplandetails.WorkoutPlanCount !== 'undefined' && this.response.responseObj.getworkoutplandetails.WorkoutPlanCount.length > 0) {
          //this.WorkoutPlanCountList = this.response.responseObj.getworkoutplandetails.WorkoutPlanCount;

          this.WorkoutPlanCountList = [];
          let i = 0;
          for (let workout of this.response.responseObj.getworkoutplandetails.WorkoutPlanCount) {

            let workoutPlanCountObj = new WorkoutPlanCount();
            workoutPlanCountObj.scheduleDate = workout.scheduleDate;
            workoutPlanCountObj.totalWorkoutPlan = workout.totalWorkoutPlan;
            workoutPlanCountObj.completedWorkoutPlan = workout.completedWorkoutPlan;
            workoutPlanCountObj.completionPercentage = workout.completionPercentage;

            console.log("this.workoutImages[i].imageurl::::" + this.workoutImages[i].imageurl);
            workoutPlanCountObj.workoutImg = this.workoutImages[i].imageurl;
            i++;
            this.WorkoutPlanCountList.push(workoutPlanCountObj);
            console.log("WorkoutPlanCountList::::" + JSON.stringify(this.WorkoutPlanCountList));
          }

        }
        else {
          this.showAlert("Alert", "Workout Plan for a current week is not present.");
        }

      }

    })

  }

  getMealList(username) {
    this.myMealPlanProvider.getMealCountList(username).then(res => {
      this.response = res;
      console.log("meal plan data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {

        if (typeof this.response.responseObj.getdatewisenutritionmealplancount.ScheduleDateWiseMealCount !== 'undefined' && this.response.responseObj.getdatewisenutritionmealplancount.ScheduleDateWiseMealCount.length > 0) {
          this.ScheduleDateWiseMealount = this.response.responseObj.getdatewisenutritionmealplancount.ScheduleDateWiseMealCount;
          console.log("ScheduleDateWiseMealount::::" + this.ScheduleDateWiseMealount)
        }
        else {
          this.showAlert("Alert", "Meal Plan for a current week is not present.");
        }

      }
    })
  }

  openViewMoreModal(topPicks) {
    console.log("clicked openViewMoreModal()");
    let modal = this.modalCtrl.create(ClubWorkoutDescriptionPage, topPicks);
    modal.present();
  }
  goToNotificationPage() {
    this.navCtrl.push(NotificationsPage, {

    });
  }

  sideNav() {
    console.log("I'm in side nav*******");
    this.navCtrl.push(MyApp, {
      username: this.username
    });

  }



  goToWorkoutDetailPage(workout) {
    console.log("Selected date is:::::" + workout.scheduleDate);
    //this.navCtrl.push(WorkoutDetailNewPage, { selectedDate: date });
    this.navCtrl.push(MyWorkoutPlanPage, { selectedDate: workout.scheduleDate });

  }

  goToNutritionDetailPage(date) {
    console.log("Selected date is:::::" + date);
    this.navCtrl.push(NutritionPlanPage, { selectedDate: date });

  }

  gotoTopPicksDetails(topPicks) {
    console.log("Selected date is:::::" + JSON.stringify(topPicks));
    this.navCtrl.push(TopPicksDetailPage, { topPicks: topPicks });

  }

  goToProgressPage() {
    this.navCtrl.push(MyProgressPage, { isAddAssessment: true });
  }
  goToTodaysWorkoutPage() {
    this.navCtrl.push(MyWorkoutPlanTodayPage);
  }
  goToTodaysNutritionPage() {
    this.navCtrl.push(MealPlanForCurrentDatePage);
  }

  //Activity Indicator
  presentLoading() {
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }





}
