import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { MoreInfoAboutClubExerciseNextPage } from '../../pages/more-info-about-club-exercise-next/more-info-about-club-exercise-next';
// import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
// import { DomSanitizer } from '@angular/platform-browser';
import { WorkoutProvider } from '../../providers/workout/workout';
import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from '../../Model/common-response';
import { DateWiseExerciseDetail } from '../../Model/date-wise-exercise-detail';
// import { PlayVideoModalPage } from '../../pages/play-video-modal/play-video-modal';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import * as Constant from '../../Constant/YTImgConst';


/**
 * Generated class for the MoreInfoAboutClubExercisePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-more-info-about-club-exercise',
  templateUrl: 'more-info-about-club-exercise.html',
})
export class MoreInfoAboutClubExercisePage {

  loading: Loading;
  planId: any;
  loader: any;
  username: string;
  exerciseDescription: string = "The biceps is a muscle on the front part of the upper arm. The biceps includes a “short head” and a “long head” that work as a single muscle.The biceps is attached to the arm bones by tough connective tissues called tendons. The tendons that connect the biceps muscle to the shoulder joint in two places are called the proximal biceps tendons. The tendon that attaches the biceps muscle to the forearm bones  (radius and ulna) is called the distal biceps tendon. When the biceps contracts, it pulls the forearm up and rotates it outward";
  category: any;
  exerciseList: any[] = [];
  initialVLink = "https://www.youtube.com/embed/";
  videoLink: any;
  embedLink: any;
  iframe_html: any;
  youtubeId: any;
  exerciseIdDemo: number = 59;
  response: CommonResponse;
  isPresentExerciseParam1: boolean = false;
  //isPresentExerciseType: boolean = false;
  // isPresentMuscleGroup: boolean = false;
  // isPresentEquipmentName: boolean = false;
  // isPresentDifficulty: boolean = false;
  isSuperSet: boolean = false;
  exerciseImageUrl: string;
  //defaultImageUrl: string = "/assets/imgs/no-image-found-360x260.png";
  defaultImageUrl: string = "/assets/imgs/youtube_img.jpg";
  isPresentExerciseData: boolean = false;
  imgLinkString: string;
  completeVideoLink: string;

  dateWiseExerciseDetailObj: DateWiseExerciseDetail = new DateWiseExerciseDetail();


  // constructor(public navCtrl: NavController, public navParams: NavParams,
  //   private youtube: YoutubeVideoPlayer, private dom: DomSanitizer, public workoutProvider: WorkoutProvider,
  //   public commonProvider: CommonProvider, public alertCtrl: AlertController, public modalCtrl: ModalController,
  //   public activityLoader: LoadingController) {
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    // private youtube: YoutubeVideoPlayer, private dom: DomSanitizer, 
    public workoutProvider: WorkoutProvider,
    public commonProvider: CommonProvider, public alertCtrl: AlertController, public modalCtrl: ModalController,
    public activityLoader: LoadingController, private iab: InAppBrowser) {
    this.username = localStorage.getItem('username')
    this.category = this.navParams.get('category');
    console.log("I'm in clubworkout-category-details" + JSON.stringify(this.category));
    this.planId = this.navParams.get('planId');
    console.log("plan id in club-workout::::" + this.planId);



    // this.getClubWorkoutExerciseDetails(this.category.superSetId, this.category.setSeqId, this.category.exerciseId, this.planId, this.category.forDay);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoreInfoAboutExercisePage');
  }

  ionViewWillEnter(){
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
    this.getClubWorkoutExerciseDetails(this.category.superSetId, this.category.setSeqId, this.category.exerciseId, this.planId, this.category.forDay);
  }

  // openViderModal(dateWiseExerciseDetailObj) {
  //   console.log("Go to video page::::");
  //   console.log(JSON.stringify(dateWiseExerciseDetailObj));
  //   let modal = this.modalCtrl.create(PlayVideoModalPage, dateWiseExerciseDetailObj);
  //   modal.present();
  // }

  getClubWorkoutExerciseDetails(superSetId, setSeqId, exerciseId, planId, forDay) {
    //this.presentLoading();
    this.workoutProvider.getClubWorkoutExerciseDetails(superSetId, setSeqId, exerciseId, planId, forDay).then(res => {
      this.response = res;
      //this.dismissLodaing();
      console.log("exercise data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {

        if (typeof this.response.responseObj.getclubworkoutexercisedetails !== 'undefined' && this.response.responseObj.getclubworkoutexercisedetails.length > 0) {
          for (let exerciseDtls of this.response.responseObj.getclubworkoutexercisedetails) {
            this.dateWiseExerciseDetailObj = exerciseDtls;
            console.log("this.dateWiseExerciseDetailObj:::::" + JSON.stringify(this.dateWiseExerciseDetailObj));
            if ((this.dateWiseExerciseDetailObj.exerciseParam1 != null && this.dateWiseExerciseDetailObj.exerciseParam1 != '') || (this.dateWiseExerciseDetailObj.exerciseParam2 != null && this.dateWiseExerciseDetailObj.exerciseParam2 != '') || (this.dateWiseExerciseDetailObj.exerciseParam3 != null && this.dateWiseExerciseDetailObj.exerciseParam3 != '')) {
              this.isPresentExerciseParam1 = true;
            }

            if (this.dateWiseExerciseDetailObj.exerciseImage != null) {
              this.exerciseImageUrl = "data:image/png;base64," + this.dateWiseExerciseDetailObj.exerciseImage;
            }

            this.completeVideoLink = this.dateWiseExerciseDetailObj.videoLink;
            this.youtubeId = this.dateWiseExerciseDetailObj.ytkey;
            this.imgLinkString = "https://img.youtube.com/vi/" + this.youtubeId + Constant.HighQualityImage;
            console.log("Image url link--->" + this.imgLinkString);

            //var youtubeId = this.dateWiseExerciseDetailObj.videoLink.split("=");
            //console.log(youtubeId[1]);
            //this.youtubeId = youtubeId[1];

            //this.videoLink = this.initialVLink + this.youtubeId;
            //this.embedLink = this.dom.bypassSecurityTrustResourceUrl(this.videoLink);

            this.loading = this.activityLoader.create({
              content: 'Please wait...'
            });

            this.loading.present();
          }
          this.isPresentExerciseData = true;
        }
        else {
          this.showAlert("Alert", "Exercise details are not present.");
          this.isPresentExerciseData = false;
          //this.dismissLodaing();
        }

      }

      // else {
      //   this.showAlert("Alert", "Exercise detail is not present.");
      //   //this.dismissLodaing();
      // }
    })


  }

  handleIFrameLoadEvent(): void {
    this.loading.dismiss();
  }

  goToExerciseDetailsNextPage(category) {
    this.navCtrl.push(MoreInfoAboutClubExerciseNextPage, { category: category, planId: this.planId });
  }

  //Activity Indicator
  presentLoading() {
    
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  OpenBrowser() {
    console.log('in the browser function');
    const options: InAppBrowserOptions = {
      zoom: 'no',
      //hidden: 'no',
      //clearcache: 'yes',
      //clearsessioncache: 'yes',
      //toolbar: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      closebuttoncaption: 'Done'


    }

    // Opening a URL and returning an InAppBrowserObject
    const browser = this.iab.create(this.completeVideoLink, '_blank', options);
    browser.show();
  }


  /* sanitize(vLink){
     return this.dom.bypassSecurityTrustResourceUrl(vLink);
   }*/

  // openExerciseVideo() {
  //   this.youtube.openVideo(this.youtubeId);
  // }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
