import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TodaySWorkoutNewPage} from '../today-s-workout-new/today-s-workout-new';
import { MyProgressPage } from '../my-progress/my-progress';
import { MealPlanForCurrentDatePage } from '../meal-plan-for-current-date/meal-plan-for-current-date';

/**
 * Generated class for the MealPlanTodayTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-meal-plan-today-tabs',
  templateUrl: 'meal-plan-today-tabs.html',
})
export class MealPlanTodayTabsPage {

  mealPlanForCurrentDatePage = MealPlanForCurrentDatePage
  todaySWorkoutPage = TodaySWorkoutNewPage
  myProgressPage = MyProgressPage

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MealPlanTodayTabsPage');
  }

}
