import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { WorkoutProvider } from '../../providers/workout/workout';
import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from '../../Model/common-response';
import { MoreInfoAboutClubExercisePage } from '../../pages/more-info-about-club-exercise/more-info-about-club-exercise';

/**
 * Generated class for the TopPicksDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-top-picks-detail',
  templateUrl: 'top-picks-detail.html',
})
export class TopPicksDetailPage {

  loader: any;
  selectedDate: Date = new Date();
  workouts: Array<any>;
  topPicks: any;
  response: CommonResponse;
  clubWorkoutDetailsList: any[] = [];



  // swipeEvent(e) {
  //   if (e.direction == 2) {
  //     console.log('swiped left');
  //     this.nextButton(this.selectedDate)

  //   } else if (e.direction == 4) {
  //     console.log('swiped right');
  //     this.previousButton(this.selectedDate);

  //   }

  // }

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public commonProvider: CommonProvider, public activityLoader: LoadingController,
    public workoutProvider: WorkoutProvider, public alertCtrl: AlertController) {

    this.topPicks = this.navParams.get('topPicks');
    console.log("I'm in top-picks-details" + JSON.stringify(this.topPicks));

    this.clubWorkoutDetails(this.topPicks.planId);


    // this.workouts = [
    //   { id: 1, name: "Ankle Circles" },
    //   { id: 2, name: "Band Calf Raise" },
    //   { id: 3, name: "Barbell Floor Calf Raise" },
    //   { id: 4, name: "Barbell Rocking Standing Calf Raise" },
    //   { id: 5, name: "Barbell Seated Calf Raise" },
    //   { id: 6, name: "Bodyweight Standing Calf Raise" },
    //   { id: 7, name: "Box Jump Down with 1 Leg Stabilization" }
    // ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopPicksDetailPage');
  }

  viewMoreInfoAboutClubExercise(category) {
    console.log("Top picks details:::::" + JSON.stringify(category));
    this.navCtrl.push(MoreInfoAboutClubExercisePage, { category: category, planId: this.topPicks.planId });
  }

  //get club workout details by planid
  clubWorkoutDetails(planId){
    this.presentLoading();
    this.workoutProvider.getClubWorkoutDetails(planId).then(res => {
      this.response = res;
      this.dismissLodaing();
      console.log("workout list data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        if (typeof this.response.responseObj.getclubworkoutdetails !== 'undefined' && this.response.responseObj.getclubworkoutdetails.length > 0) {
          console.log("this.response.responseObj.getclubworkoutdetails:::::"+JSON.stringify(this.response.responseObj.getclubworkoutdetails));
          this.clubWorkoutDetailsList = this.response.responseObj.getclubworkoutdetails;
        }
        else {
          this.showAlert("Alert", "Exercise for a current date is not present.");
          //this.dismissLodaing();
        }
      }
    })
  }


  //get previous date
  previousButton(selectedDate) {
    var day = new Date(selectedDate);
    console.log("selectedDate::::" + day);

    var previousDay = new Date(day);
    previousDay.setDate(day.getDate() - 1);
    console.log("previous day of the selected day" + previousDay);
    //let date = new Date();
    //console.log(date.setDate(selectedDate.getDate() + 1));
    this.selectedDate = previousDay;
  }

  //get next date
  nextButton(selectedDate) {
    var day = new Date(selectedDate);
    console.log("selectedDate::::" + day);

    var nextDay = new Date(day);
    nextDay.setDate(day.getDate() + 1);
    console.log("next day of the selected day" + nextDay);
    //let date = new Date();
    //console.log(date.setDate(selectedDate.getDate() + 1));
    this.selectedDate = nextDay;
  }

  //Activity Indicator
  presentLoading() {
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }


  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
