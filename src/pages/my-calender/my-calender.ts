import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { MyCalenderResponseModel, CalenderEventArrayModel } from '../../Model/myCalendarModel';
// import { HometabsPage } from '../../pages/hometabs/hometabs';
// import { ScheduleMeetingPage } from '../../pages/schedule-meeting/schedule-meeting';
import { MealPlanForCurrentDatePage } from '../../pages/meal-plan-for-current-date/meal-plan-for-current-date';
import { MyWorkoutPlanTodayPage } from '../../pages/my-workout-plan-today/my-workout-plan-today';
import { WorkoutProvider } from '../../providers/workout/workout';
import { CommonResponse } from '../../Model/common-response';
import { MyMealPlanProvider } from '../../providers/my-meal-plan/my-meal-plan';
import { MyMealPlan } from '../../Model/my-meal-plan-model';
import { MealDetail } from '../../Model/meal-detail-model';
import { CustomerAbsent } from '../../Model/customer-absent-model';
// import { CalendarComponentOptions } from 'ion2-calendar';
import { CommonProvider } from '../../providers/common/common';
import { Events } from 'ionic-angular';



//@IonicPage()
@Component({
  selector: 'page-my-calender',
  templateUrl: 'my-calender.html',
})
export class MyCalenderPage {

  
  currentEvents: CustomerAbsent[] = [];
  tempList: CustomerAbsent[] = [];
  username: string;
  countSetExercise: number = 0;
  countSingleExercise: number = 0;
  countTotalChecked: number = 0;
  response: CommonResponse;
  keySets = [];
  exerciseList: any[] = [];
  supersetWorkouts: any[] = [];
  workouts: any[] = [];
  isCheckedSuperset: boolean = false;
  isCheckedSingle: boolean = false;
  isDisableWorkoutPlan: boolean = false;
  isDisableMealPlan: boolean = false;
  selectedDate: any;
  currentDate: Date = new Date();
  myMealPlan: MyMealPlan;
  Name: string;
  Desc: string;
  mealDetails: MealDetail[] = [];
  pdfPath: string;
  defaultPath: string = "http://182.73.216.93:8091/gymadminrest";
  meal_icon: any[] = [];
  mealDetailObj: MealDetail = new MealDetail();
  markDisabled: boolean = false;
  absentList: any[] = [];
  performancePrecentage: any;
  performancePrecentageFromSideMenu: any;
  performanceName: string;
  byDate: string;
  gymId: string;
  holiday: string;
  customerGoal: any;
  isNavigateFromCalendar: boolean = true;
  menuTitle: string;
  isSelectedFromSidemenu: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public commonProvider: CommonProvider, 
    public workoutProvider: WorkoutProvider, public myMealPlanProvider: MyMealPlanProvider, public events: Events) {

    

    this.username = localStorage.getItem('username');
    this.gymId = localStorage.getItem('gymId');
    console.log("this.gymId in my-calendar::::"+this.gymId);

    this.getGoalName(this.username);

    this.getAbsentList(this.username);

    this.getGymOffDay(this.gymId);

    console.log("this.currentDate:::::"+this.currentDate);
    this.byDate = this.formatDate(this.currentDate);
    console.log("Selected date::::"+this.selectedDate);
    if(this.selectedDate === undefined){
      this.getCustomerPerformanceForDay(this.username, this.byDate);
      this.getCustomerWorkoutPlan(this.username, this.byDate);
    this.getMyPlanDetail(this.username, this.byDate);
    }
    

   
   
  }

  //Get goal name
  getGoalName(username){
    this.workoutProvider.getGoalName(username).then(res => {
      this.response = res;
      console.log("Goal Name::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        this.customerGoal = this.response.responseObj.getCustomerGoal;
      }


    })
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad MyCalenderPage');
    this.menuTitle = this.navParams.get('menuTitle');
    console.log("menu title in my-calendar:::::" + JSON.stringify(this.menuTitle));
    if(this.menuTitle === 'My Calendar'){
      console.log("getCustomerPerformanceForDay() call from side menu");
      this.isSelectedFromSidemenu = true;
      //this.getCustomerPerformanceForDay(this.username, this.byDate);
      //this.getCustomerWorkoutPlan(this.username, this.byDate);
    //this.getMyPlanDetail(this.username, this.byDate);
    }

    // Get data from child page
	  this.events.subscribe('selectedDate', (selectedDate) => {
    this.selectedDate = selectedDate;
    console.log("this.selectedDate back to my calendar from workout page::::"+this.selectedDate);
    this.getCustomerPerformanceForDay(this.username, this.selectedDate);
    
  }); 
  }

  //to convert date to string format
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  //get gym off day
  getGymOffDay(gymId){
    this.workoutProvider.getGymOffDay(Number(gymId)).then(res => {
      this.response = res;
      if (this.response.status == 200 && this.response.message === "success") {
       console.log("Holiday::::"+this.response.responseObj.getGymOffDay);
       this.holiday = this.response.responseObj.getGymOffDay;
      }
    })
  }

  getAbsentList(username) {
    console.log("I'm in getAbsentList()");
    this.workoutProvider.getAbsentList(username).then(res => {
      this.response = res;
      if (this.response.status == 200 && this.response.message === "success") {
        // console.log("this.response.responseObj.getAbsentDates:::::"+JSON.stringify(this.response.responseObj.getAbsentDates));
        this.absentList = this.response.responseObj.getAbsentDates;
        console.log("this.absentList:::::" + JSON.stringify(this.absentList));
        for (let absentDate of this.absentList) {
          let splitedDate = absentDate.split("-");
          console.log(splitedDate[0]);
          let customerAbsentObj = new CustomerAbsent();
          customerAbsentObj.year = splitedDate[0];
          customerAbsentObj.month = splitedDate[1] - 1;
          customerAbsentObj.date = splitedDate[2];
          this.tempList.push(customerAbsentObj);
        }
        this.currentEvents = this.tempList;
        console.log("this.currentEvents:::::"+JSON.stringify(this.currentEvents));
      }
    })
  }


  onDaySelect(event) {
    console.log("Date selected::::" + JSON.stringify(event));
    var dateString = event.year + '-' + (event.month + 1) + '-' + event.date;
    console.log("Before assigning date::::"+dateString);
    this.selectedDate = dateString;
    console.log("After assigning date::::"+dateString);
    //let bydate = event.year + '-' + (event.month + 1) + '-' + event.date;
    this.getCustomerWorkoutPlan(this.username, dateString);
    this.getMyPlanDetail(this.username, dateString);
    this.getCustomerPerformanceForDay(this.username, dateString);

    


  

  }


  //get my workout plan list
  getCustomerWorkoutPlan(username, byDate) {
    this.countSetExercise = 0;
    this.countSingleExercise = 0;
    this.countTotalChecked = 0;
    var map1 = new Map();
    this.workoutProvider.getCustomerWorkoutPlan(username, byDate).then(res => {
      this.response = res;
      console.log("workout list data in my-calendar::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {

        if (typeof this.response.responseObj.customerworkoutplan.ListOfSet !== 'undefined') {
          var keyObj = this.response.responseObj.customerworkoutplan.ListOfSet;
          this.keySets = Object.keys(keyObj);
          console.log('obj contains ' + this.keySets.length + ' keys: ' + this.keySets);
          this.exerciseList = [];
          for (let key of this.keySets) {
            console.log(key)
            console.log(JSON.stringify(this.response.responseObj.customerworkoutplan.ListOfSet[key]));
            this.exerciseList.push(this.response.responseObj.customerworkoutplan.ListOfSet[key]);
          }
          console.log("this.exerciseList length>>>><<<<" + JSON.stringify(this.exerciseList.length));
          console.log("this.exerciseList>>>><<<<" + JSON.stringify(this.exerciseList));

          if (this.exerciseList.length > 0) {
            this.isDisableWorkoutPlan = false;
          }
          else {
            this.isDisableWorkoutPlan = true;
          }

          console.log("this.isDisableWorkoutPlan::::" + this.isDisableWorkoutPlan);
        }
      }
    })
  }

  //to get meal plan details
  getMyPlanDetail(username, byDate) {
    //this.presentLoading();
    this.myMealPlanProvider.getNutritionMealPlanDetails(username, byDate).then(res => {
      this.response = res;
      console.log("meal plan data in my-calendar page::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        //this.dismissLodaing();
        if (this.response.responseObj.getnutritionmealplandetails !== null) {
          console.log("this.response.responseObj.getnutritionmealplandetails::::" + JSON.stringify(this.response.responseObj.getnutritionmealplandetails));
          this.myMealPlan = this.response.responseObj.getnutritionmealplandetails;
          this.Name = this.myMealPlan.Name;
          console.log("this.Name::::" + this.Name);
          this.Desc = this.myMealPlan.Desc;
          console.log("this.myMealPlan.mealDetails.length::::" + this.myMealPlan.mealDetails.length);
          if (this.myMealPlan.mealDetails.length > 0) {
            this.isDisableMealPlan = false;
          }
          else {
            this.isDisableMealPlan = true;
          }


        }
      }
    })

  }

  //to get customer Performance Percentage For selected day
  getCustomerPerformanceForDay(username, selectDate) {
    console.log("selectedDate in getCustomerPerformanceForDay() is:::::::"+selectDate);
    this.performancePrecentage = 0;
    this.performancePrecentageFromSideMenu = 0;
    this.performanceName = "Marginal";
    this.commonProvider.getCustomerPerformanceForDayWise(username, selectDate).then(res => {
      this.response = res;
      console.log("Performance percentage:::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message.toLowerCase() === "success") {
        if (typeof this.response.responseObj.getcustomerperformanceforweek !== 'undefined' && this.response.responseObj.getcustomerperformanceforweek.length > 0) {
          for (let performanceList of this.response.responseObj.getcustomerperformanceforweek) {
            console.log("performanceList.completionPercentage:::" + performanceList.completionPercentage);
            this.performancePrecentage = performanceList.completionPercentage;
            this.performancePrecentageFromSideMenu  = performanceList.completionPercentage;
            if (this.performancePrecentage > 80) {
              this.performanceName = "Excellent";

            }
            else if (this.performancePrecentage >= 60 && this.performancePrecentage <= 80) {
              this.performanceName = "Good";
            }
            else {
              this.performanceName = "Marginal";
            }
            console.log("this.performanceName::::"+this.performanceName);
          }
        }
        else{
          this.performancePrecentageFromSideMenu = 0
          this.performancePrecentage = 0;
          this.performanceName = "Marginal";
        }
      }
    })
  }


  goToMyWorkoutsEvent() {
    if(this.selectedDate === undefined){
      this.navCtrl.push(MyWorkoutPlanTodayPage, { selectedDate: this.byDate , isNavigateFromCalendar: this.isNavigateFromCalendar});
    }
    else{
      this.navCtrl.push(MyWorkoutPlanTodayPage, { selectedDate: this.selectedDate , isNavigateFromCalendar: this.isNavigateFromCalendar});
      //this.getCustomerPerformanceForDay(this.username, this.selectedDate);
      //console.log("this.selectedDate back to my calendar from workout page::::"+this.selectedDate);
    }
    
  }

  goToMyNutritionPlan() {
    if(this.selectedDate === undefined){
      this.navCtrl.push(MealPlanForCurrentDatePage, { selectedDate: this.byDate, isNavigateFromCalendar: this.isNavigateFromCalendar});
    }
    else{
      this.navCtrl.push(MealPlanForCurrentDatePage, { selectedDate: this.selectedDate, isNavigateFromCalendar: this.isNavigateFromCalendar});
    }
  }

  // // goToScheduleMeeting(){
  // //   this.navCtrl.push(ScheduleMeetingPage,{

  // //   });
  // }
  //-----------------------------------------------------//

  /* ionViewWillEnter() {
     this.date = new Date();
     this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
     this.getDaysOfMonth();
     this.loadEventThisMonth();
   }*/

  //-------DAYS OF MONTH------//
  /*getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if(this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
    for (var j = 0; j < thisNumOfDays; j++) {
      this.daysInThisMonth.push(j+1);
    }

    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
    // var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var k = 0; k < (6-lastDayThisMonth); k++) {
      this.daysInNextMonth.push(k+1);
    }
    var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
    if(totalDays<36) {
      for(var l = (7-lastDayThisMonth); l < ((7-lastDayThisMonth)+7); l++) {
        this.daysInNextMonth.push(l);
      }
    }
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
    this.getDaysOfMonth();
  }

  loadEventThisMonth() {
    this.eventList = new Array();
    // var startDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    // var endDate = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0);
    this.eventList.push({"startDate":"2018-12-31 00:00:00","endDate":"2018-12-31 23:59:59"},{"startDate":"2018-12-9 00:00:00","endDate":"2018-12-9 23:59:59"});
    console.log("tttttt---->"+JSON.stringify(this.eventList));
    // this.calendar.listEventsInRange(startDate, endDate).then(
    //   (msg) => {        
    //     msg.forEach(item => {
    //       console.log("pkpkpk------>"+JSON.stringify(item));
    //       this.eventList.push(item);
    //     });
    //   },
    //   (err) => {
    //     console.log(err);
    //   }
    // );
  }

  checkEvent(day) {
    var hasEvent = false;
    var thisDate1 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 00:00:00";
    var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
    console.log("start date--->"+thisDate1);
    console.log("end date--->"+thisDate2);
    this.eventList.forEach(event => {
      if(((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
        hasEvent = true;
      }
    });
    console.log("has event--->"+hasEvent);
  return hasEvent;
  }

  selectDate(day) {
    // this.isSelected = false;
    // this.selectedEvent = new Array();
    var thisDate1 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 00:00:00";
    var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
    console.log("start date selected--->",thisDate1);
    console.log("end date selected--->",thisDate2);
    // this.eventList.forEach(event => {
    //   if(((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
        // this.isSelected = true;
    //   }
    // });    
    // return true;
  }*/

}
