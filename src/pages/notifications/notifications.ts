import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from "../../Model/common-response";
import {Notification} from "../../Model/notification"

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  username: string;
  notifications: Notification[] = [];
  startDate: any;
  endDate: any;
  minDate: any;
  maxDate: any;
  currDate: Date = new Date();
  dateConvertIntoString: string;
  response: CommonResponse;
  
 
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public commonProvider: CommonProvider,
    public alertCtrl: AlertController,) {
    this.dateConvertIntoString = this.currDate.toString();
    this.username = localStorage.getItem("username");
  

    this.getAllNotification(this.username);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

  getAllNotification(username){
    this.notifications = [];
    this.commonProvider.getAllNotification(username).then(res => {
      //this.presentLoading();
      this.response = res;
      if (this.response.status == 200 && this.response.message === 'success') {
        console.log("Response for notification:::" + JSON.stringify(this.response));
        if( this.response.responseObj.getAllNotification.length > 0){
          this.notifications = this.response.responseObj.getAllNotification;
        }
      }
      else{
        this.showAlert("Error", this.response.message);
      }
    })
  }

  // notificationSearch(){
  //   console.log("Start date:::::"+this.startDate);
  //   console.log("End date:::::"+this.endDate);
  //   this.getAllNotificationByDateRange(this.username, this.startDate, this.endDate);
  // }
  
  startDateChanged(gotDate) {
    if (this.endDate == undefined) {
      this.showAlert("Alert", "Please select end date");
    }
    if(this.startDate > this.endDate ){
      this.showAlert("Alert", "Start date is not greater than end date");
    }
    if(this.startDate != undefined && this.endDate != undefined && this.endDate >= this.startDate){
      this.getAllNotificationByDateRange(this.username, this.startDate, this.endDate);
    }
  }

  endDateChanged(gotDate) {
    if (this.startDate == undefined) {
      this.showAlert("Alert", "Please select start date");
    }
    if(this.endDate < this.startDate){
      this.showAlert("Alert", "End date is not less than start date");
    }
    if(this.startDate != undefined && this.endDate != undefined && this.endDate >= this.startDate){
      this.getAllNotificationByDateRange(this.username, this.startDate, this.endDate);
    }
  }

  
  getAllNotificationByDateRange(username, startDate, endDate){
    this.notifications = [];
    this.commonProvider.getAllNotificationByDateRange(username, startDate, endDate).then(res => {
      //this.presentLoading();
      this.response = res;
      if (this.response.status == 200) {
        console.log("Response after search notification:::" + JSON.stringify(this.response));
        if( this.response.responseObj.getAllNotification.length > 0){
          this.notifications = this.response.responseObj.getAllNotification;
        }
        else{
          this.showAlert("Alert", "Search result is not found.");
        }
        
      }
      else{
        this.showAlert("Error", this.response.message);
      }
    })
  }

  deleteNotification(notification){
    console.log("Notofication details::::"+JSON.stringify(notification));
    //let index = this.notifications.indexOf(notification);

    // if(index > -1){
    //   this.notifications.splice(index, 1);
    // }
    this.confirmMessage(notification);

  }

  deleteCustomerNotification(customerId, notificationId, notificationDate, customerNotificationId){
    this.commonProvider.deleteNotification(customerId, notificationId, notificationDate, customerNotificationId).then(res => {
      //this.presentLoading();
      this.response = res;
      if (this.response.status == 200) {
        console.log("Response after delete notification:::" + JSON.stringify(this.response));
        this.showAlert("Success", "Notification deleted successfully!");
        this.getAllNotification(this.username);
      }
      else{
        this.showAlert("Error", this.response.message);
      }
    })

  }

   //Alert
   showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ["OK"]
    });
    alert.present();
  }


  //cpnfirmation message
  confirmMessage(notification) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure you want to delete this notification?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.getAllNotification(this.username);
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.deleteCustomerNotification(notification.customerId, notification.notificationId, notification.notificationDate, notification.customerNotificationId);

          }
        }
      ]
    });
    alert.present();
  }
  

}
