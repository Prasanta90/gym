import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ViewPastDuesAndPaymentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-view-past-dues-and-payments',
  templateUrl: 'view-past-dues-and-payments.html',
})
export class ViewPastDuesAndPaymentsPage {
  paymentDetails: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.paymentDetails = [
      {amount: 600, paymentDate: '20-Nov-2018'},
      {amount: 600, paymentDate: '20-Oct-2019'},
      {amount: 600, paymentDate: '20-Sep-2019'},
      {amount: 600, paymentDate: '20-JUl-2019'},
      {amount: 600, paymentDate: '20-Jun-2019'}
    ]
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPastDuesAndPaymentsPage');
  }

}
