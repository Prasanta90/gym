import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
// import { PaymentDueDetail } from '../../Model/payment-due-detail';
import { PackageDetail } from '../../Model/package-details';
//import { Invoice } from '../../Model/invoice-model';
import { PaymentPackage } from '../../Model/payment-package';
import { MyProfileProvider } from '../../providers/my-profile/my-profile';
import { CommonResponse } from '../../Model/common-response';
import { CommonProvider } from '../../providers/common/common';
import { MembershipPlanPage } from '../../pages/membership-plan/membership-plan';


/**
 * Generated class for the PayNowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-pay-now',
  templateUrl: 'pay-now.html',
})
export class PayNowPage {

  //name: string = "Surajit Bera";
  currentDate: Date = new Date();
  paymentDate: string;
  //selectedPackageList: any[] = [];
  selectedPackage: PackageDetail;
  
  //packageCost: number = 0.00;
  username: string;
  customerName: string;
  firstNm: string;
  lastNm: string;
  customerId: any;
  netPayable: number = 0;
  invoice_email_flag: string = "Y";
  emailReceipt: string = "Y";
  paymentMode: string = "Online";
  response: CommonResponse;
  paidServiceId: any;
  PackageDetailList: PackageDetail[] = [];
  endDate: Date = new Date();
  customerImageUrl: string;


  //invoice: Invoice;
  paymentPackageObj: PaymentPackage = new PaymentPackage();

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public myProfileProvider: MyProfileProvider,
    public commonProvider: CommonProvider) {
    //this.paymentDate =  this.currentDate.toString();
    this.paymentDate = this.formatDate(this.currentDate);

    this.username = localStorage.getItem('username');
    this.firstNm = localStorage.getItem('firstNm');
    this.lastNm = localStorage.getItem('lastNm');
    this.customerId = localStorage.getItem('customerId');
    this.customerImageUrl = localStorage.getItem('customerImage');
    console.log("Customer image in pay now page:::::::"+this.customerImageUrl);
    

    if ((this.firstNm != null && this.firstNm != '') && (this.lastNm != null && this.lastNm != '')) {
      this.customerName = this.firstNm.concat(' ').concat(this.lastNm);
    }

    //this.selectedPackageList = this.navParams.get('selectedPackageList');
    // console.log("this.selectedPackageList****" + JSON.stringify(this.selectedPackageList));
    // for(let pkgDtls of  this.selectedPackageList){
    //   this.packageCost = this.packageCost + pkgDtls.balanceDue;

    // }
    this.selectedPackage = this.navParams.get('selectedPackage');
    console.log("this.selectedPackage****" + JSON.stringify(this.selectedPackage));
    //this.netPayable = this.selectedPackage.serviceCost - this.selectedPackage.maxDiscount;
    this.netPayable = this.selectedPackage.serviceCost ;
    console.log(this.selectedPackage.serviceDuration);
    // if (this.selectedPackage.serviceDuration != null && this.selectedPackage.serviceDuration != "") {
    //   let weightValue = this.selectedPackage.serviceDuration.split(" ");
    //   let serviceDurationValue = weightValue[0];
    //   let serviceDurationName = weightValue[1];
    //   switch(serviceDurationName){
    //     case 'Weeks':

    //   }
    // }

    console.log("Date after 30 days::::"+ new Date(this.currentDate.getTime() +7 * 4 * 24 * 60 * 60 * 1000));
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayNowPage');
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }


  goToPaymentPage() {
    // let desc = 'Credit towards <br>' + this.selectedPackage.serviceName + '<br> received from' + '<div>' + this.customerName + ' ' + '-' + this.username;
    // var desc1 = '<div> Credit towards</div>'
    // var desc2 = '<div>'+this.selectedPackage.serviceName +'</div>'
    // var desc3 = '<div> received from</div>'
    //+'</br> received from <br>' + this.customerName + ' ' + '-' + this.username+'</div>';
    //console.log("description:::"+desc);
    const options = {
      description: this.selectedPackage.serviceName,
      contact:this.username,
      image: 'https://i.imgur.com/ebuOCAa.png',
      //image: "../../assets/imgs/20190415_123318_0001.png",
      currency: 'INR',
      //key: 'rzp_test_Ah0sOHFqhxN3DA',
      key: 'rzp_live_TnDqL8YbLTWCpQ',
      // amount: this.packageCost * 100,
      amount: this.netPayable * 100,
      name: this.customerName,
      prefill: {
        email: '',
        contact: '',
        name: this.customerName,
      },
      theme: {
        color: '#528FF0'
      },
      modal: {
        ondismiss: function() {
          alert('dismissed');
        }
      }
    };

    // const successCallbacks = function(payment_id) {
    //   alert('payment_id: ' + payment_id);

    //   this.savePaymentValues();

    // };
    var successCallback = function (success) {
      alert('payment_id: ' + success.razorpay_payment_id)
      // var orderId = success.razorpay_order_id;
      // var signature = success.razorpay_signature;
      console.log('pay_id--->' + success.razorpay_payment_id);
      console.log('order_id--->' + success.razorpay_order_id);
      console.log('signature--->' + success.razorpay_signature);

    }


    const cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    // RazorpayCheckout.open(options, successCallback, cancelCallback);
    RazorpayCheckout.on('payment.success', successCallback => {
      this.savePaymentValues();
    })
    RazorpayCheckout.on('payment.cancel', cancelCallback)
    RazorpayCheckout.open(options)
  }

  savePaymentValues() {
    //this.paymentPackageObj = new PaymentPackage();
    this.paymentPackageObj.paymentDate = this.paymentDate;
    //this.paymentPackageObj.payment_id = payment_id;
    this.paymentPackageObj.customerId = this.customerId;
    this.paymentPackageObj.paymentAmount = this.netPayable;
    this.paymentPackageObj.discount_amt = this.selectedPackage.maxDiscount;
    this.paymentPackageObj.invoice_email_flag = this.invoice_email_flag;
    this.paymentPackageObj.emailReceipt = this.emailReceipt;
    this.paymentPackageObj.service_id = this.selectedPackage.serviceId;
    this.paymentPackageObj.invoice_gross_amt = this.netPayable;
    this.paymentPackageObj.discount_amt = this.selectedPackage.maxDiscount;
    this.paymentPackageObj.invoice_net_amt = this.selectedPackage.serviceCost;
    this.paymentPackageObj.particulars = "Payment successful";
    this.paymentPackageObj.paymentMode = this.paymentMode;
    this.paymentPackageObj.gymId = this.selectedPackage.gymId;
    if(this.selectedPackage.serviceType === 'Package'){
      this.paymentPackageObj.duration = this.selectedPackage.serviceDuration;
    }

    console.log("this.paymentPackageObj>>>>>>" + JSON.stringify(this.paymentPackageObj));

    this.postPaymentetails(this.paymentPackageObj);

  }

  postPaymentetails(paymentPackageObj){
    this.myProfileProvider.postPaymentetails(paymentPackageObj).then(res => {
      this.response = res;
      console.log("payment data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "1") {
        this.paidServiceId = this.response.responseObj.ServiceId;
        this.showAlert("Success", this.response.responseObj.buypackage);
        this.navCtrl.push(MembershipPlanPage,{ paidServiceId: this.paidServiceId});
        
      }
      else{
        this.showAlert("Error", this.response.responseObj.buypackage);
        
      }
    })
  }

  
  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
