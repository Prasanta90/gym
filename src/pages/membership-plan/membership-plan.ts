import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Slides } from 'ionic-angular';
import { MyProfileProvider } from '../../providers/my-profile/my-profile';
import { CommonResponse } from '../../Model/common-response';
import { PackageDetail } from '../../Model/package-details';
import { PayNowPage } from '../../pages/pay-now/pay-now';
import { CommonProvider } from '../../providers/common/common';


/**
 * Generated class for the MembershipPlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-membership-plan',
  templateUrl: 'membership-plan.html',
})
export class MembershipPlanPage {

  @ViewChild('swipedTabsSlider') swipedTabsSlider: Slides;
  selectedSegment: string = "0";
  username: string;
  selectedSegmentName: string = "product";
  membershipPlans: Array<any>;
  response: CommonResponse;
  packageDetail: PackageDetail;
  PackageDetailList: PackageDetail[] = [];
  serviceCostAfterDiscount: number = 0;
  paidServiceId: any;
  currentdate: Date = new Date();


  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public myProfileProvider: MyProfileProvider, public commonProvider: CommonProvider) {
      this.username = localStorage.getItem('username');
      this.paidServiceId = this.navParams.get('paidServiceId');
      console.log("this.paidServiceId****" + JSON.stringify(this.paidServiceId));

    this.getMyPackageDetails(this.selectedSegmentName, this.username);

  }

  //swiped tabs
  selectTab(index) {
    this.swipedTabsSlider.slideTo(index, 500);
    console.log("selectedSEgment in selectTab() is::::"+this.selectedSegment);
    console.log("Index is::::" + index);
    console.log("type of index:::::" + typeof index);
    if (index === 0) {
      this.selectedSegmentName = "product";
      this.getMyPackageDetails(this.selectedSegmentName, this.username);
    }
    else if (index === 1) {
      this.selectedSegmentName = "package";
      this.getMyPackageDetails(this.selectedSegmentName, this.username);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MembershipPlanPage');
  }

  // segmentChanged(ev: any) {
  //   console.log('Segment changed', ev);
  //   this.getMyPackageDetails(ev);
    
  // }

  moveButton($event) {
    this.selectedSegment = $event._snapIndex.toString();

    console.log("I'm in movebutton(), selected segment is:::::" + this.selectedSegment);
    console.log("I'm in movebutton(), type of selected segment is:::::" + typeof this.selectedSegment);
    console.log("######" + this.selectedSegment)
    if (this.selectedSegment === "0") {
      this.selectedSegmentName = "product";
      this.getMyPackageDetails(this.selectedSegmentName, this.username);
    }
    else if (this.selectedSegment === "1") {
      this.selectedSegmentName = "package";
      this.getMyPackageDetails(this.selectedSegmentName, this.username);
    }
  }

  getMyPackageDetails(serviceType: string, username: string) {
    //this.presentLoading();
    this.myProfileProvider.getMyPackageDetails(serviceType, username).then(res => {
      this.response = res;
      console.log("service data::::::" + JSON.stringify(this.response));
      this.PackageDetailList = [];
      if (this.response.status == 200 && this.response.message === "success") {
        //this.dismissLodaing();
        if (this.response.responseObj.getpackagedetails.length > 0) {
          this.PackageDetailList = [];
          for (let packageDtls of this.response.responseObj.getpackagedetails) {
            this.packageDetail = new PackageDetail();
            this.packageDetail = packageDtls;
            this.PackageDetailList.push(this.packageDetail);
            console.log("Package details within if:::::>>>>" + JSON.stringify(this.PackageDetailList));

            // if (packageDtls.maxDiscount === 0 || packageDtls.maxDiscount === null) {
            //   this.packageDetail = packageDtls;
            //   this.packageDetail.isDiscountValueZero = true;
             
            //   this.PackageDetailList.push(this.packageDetail);
            //   console.log("Package details within if:::::>>>>" + JSON.stringify(this.PackageDetailList));
            // }
            // else {
            //   this.packageDetail = packageDtls;
            //   this.packageDetail.isDiscountValueZero = false;
            //   this.serviceCostAfterDiscount = packageDtls.serviceCost - packageDtls.maxDiscount;
            //   this.packageDetail.serviceCostAfterDiscount = this.serviceCostAfterDiscount;
              
            //   this.PackageDetailList.push(this.packageDetail);
            //   console.log("Package details:::::>>>>" + JSON.stringify(this.PackageDetailList));
            // }

          }
        }
      }
    })
  }


  goToPaymentPage(plan){

    this.navCtrl.push(PayNowPage,{ selectedPackage: plan});
  }
}
