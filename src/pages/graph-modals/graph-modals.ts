import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Chart } from 'chart.js';
// import { MyProgressPage } from '../../pages/my-progress/my-progress';
import { CustomerAssessment } from '../../Model/customer-assessment';
import { Girth } from '../../Model/girth-details';
import { SkinFold } from '../../Model/skinFold-details';
import { MyProgressProvider } from '../../providers/my-progress/my-progress';
import { CommonResponse } from '../../Model/common-response';

/**
 * Generated class for the GraphModalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-graph-modals',
  templateUrl: 'graph-modals.html',
})
export class GraphModalsPage {

  username: string;
  response: CommonResponse;

  isShowWeightGraph: boolean = false;
  isShowGirthtGraph: boolean = false;
  isShowSkinFoldGraph: boolean = false;
  isShowWaterGraph: boolean = false;
  isShowBmiGraph: boolean = false;
  isShowBodyFatGraph: boolean = false;
  isShowLeanMassGraph: boolean = false;
  charNum: any;
  assessment: CustomerAssessment[] = [];

  customerAssessmentObj: CustomerAssessment;
  getcustomerassessment: CustomerAssessment[] = [];
  girthObj: Girth;
  skinFoldObj: SkinFold;

  //for weight
  customer_weight: any[] = [];
  //fpr girth
  customer_chest: any[] = [];
  customer_waist: any[] = [];
  customer_neck: any[] = [];
  customer_left_thigh: any[] = [];
  customer_right_calf: any[] = [];
  customer_right_arm: any[] = [];
  customer_hip: any[] = [];
  customer_left_calf: any[] = [];
  customer_shoulder: any[] = [];
  customer_left_arm: any[] = [];
  whr: any[] = [];
  customer_right_thigh: any[] = [];
  //for skin fold
  mm_chest: any[] = [];
  mm_tricep: any[] = [];
  mm_calf: any[] = [];
  mm_bicep: any[] = [];
  mm_skin_average: any[] = [];
  mm_midaxillary: any[] = [];
  mm_skin_fold_total: any[] = [];
  mm_abdominal: any[] = [];
  mm_subscapular: any[] = [];
  mm_thigh: any[] = [];
  mm_suprailiac: any[] = [];

  water_percentage: any[] = [];

  customer_bmi: any[] = [];

  body_fat_percentage: any[] = [];

  lean_mass: any[] = [];


  weightDateList: any[] = [];
  weightPlotValues: any[] = [];

  chestDateList: any[] = [];
  chestPlotValues: any[] = [];
  waistDateList: any[] = [];
  waistPlotValues: any[] = [];
  neckDateList: any[] = [];
  neckPlotValues: any[] = [];
  leftThighDateList: any[] = [];
  leftThighPlotValues: any[] = [];
  rightCalfDateList: any[] = [];
  rightCalfPlotValues: any[] = [];
  rightArmDateList: any[] = [];
  rightArmPlotValues: any[] = [];
  hipDateList: any[] = [];
  HipPlotValues: any[] = [];
  leftCalfDateList: any[] = [];
  leftCalfPlotValues: any[] = [];
  shoulderDateList: any[] = [];
  shoulderPlotValues: any[] = [];
  leftArmDateList: any[] = [];
  leftArmPlotValues: any[] = [];
  whrDateList: any[] = [];
  whrPlotValues: any[] = [];
  rightThighDateList: any[] = [];
  rightThighPlotValues: any[] = [];

  mmChestDateList: any[] = [];
  mmChestPlotValues: any[] = [];
  mmTricepDateList: any[] = [];
  mmTricepPlotValues: any[] = [];
  mmCalfDateList: any[] = [];
  mmCalfPlotValues: any[] = [];
  mmBicepDateList: any[] = [];
  mmBicepPlotValues: any[] = [];
  mmSkinAverageDateList: any[] = [];
  mmSkinAveragePlotValues: any[] = [];
  mmMidaxillaryDateList: any[] = [];
  mmMidaxillaryPlotValues: any[] = [];
  mmSkinFoldTotalDateList: any[] = [];
  mmSkinFoldTotalPlotValues: any[] = [];
  mmAbdominalDateList: any[] = [];
  mmAbdominalPlotValues: any[] = [];
  mmSubscapularlDateList: any[] = [];
  mmSubscapularPlotValues: any[] = [];
  mmThighDateList: any[] = [];
  mmThighPlotValues: any[] = [];
  mmSuprailiacDateList: any[] = [];
  mmSuprailiacPlotValues: any[] = [];

  //for water percentage
  waterPercentageDateList: any[] = [];
  waterPercentagePlotValues: any[] = [];

  //for bmi

  bmiDateList: any[] = [];
  bmiPlotValues: any[] = [];

  //for bodyfat
  bodyFatDateList: any[] = [];
  bodyFatPlotValues: any[] = [];

  //for lean mass
  leanMassDateList: any[] = [];
  leanMassPlotValues: any[] = [];



  @ViewChild('lineChartGirth') lineChartGirth;
  @ViewChild('lineChartWeight') lineChartWeight: ElementRef;;
  @ViewChild('lineChartSkinFold') lineChartSkinFold;
  @ViewChild('lineChartWaterPer') lineChartWaterPer;
  @ViewChild('lineChartBMI') lineChartBMI;
  @ViewChild('lineChartBodyfatPer') lineChartBodyfatPer;
  @ViewChild('lineChartLeanMass') lineChartLeanMass;


  public lineChartG: any;
  public lineChartWt: any;
  public lineChartSF: any;
  public lineChartWP: any;
  public lineChartBmi: any;
  public lineChartBodyFatPer: any;
  public lineChartleanmass: any;




  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public myProgressProvider: MyProgressProvider, public alertCtrl: AlertController) {
    console.log("constructor");
    this.username = localStorage.getItem('username');

    this.charNum = this.navParams.get('charNum');
    console.log("this.charNum****" + JSON.stringify(this.charNum));
    this.assessment = this.navParams.get('assessment');
    console.log("this.assessment in graph-modals****" + JSON.stringify(this.assessment));

    //this.createLineChartForWeight();
    this.isShowWeightGraph = true;
    this.isShowGirthtGraph = true;
    this.isShowSkinFoldGraph = true;
    this.isShowWaterGraph = true;
    this.isShowBmiGraph = true;
    this.isShowBodyFatGraph = true;
    this.isShowLeanMassGraph = true;



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraphModalsPage');

    if (this.charNum === 0) {
      console.log(typeof this.charNum);

      this.getWeightMeasurementInfo();


      //this.createLineChartForWeight();
      this.isShowWeightGraph = true;
      this.isShowGirthtGraph = false;
      this.isShowSkinFoldGraph = false;
      this.isShowWaterGraph = false;
      this.isShowBmiGraph = false;
      this.isShowBodyFatGraph = false;
      this.isShowLeanMassGraph = false;
    }
    if (this.charNum === 1) {
      this.getGirthMeasurementInfo();
      //this.createLineChartForGirth();
      this.isShowWeightGraph = false;
      this.isShowGirthtGraph = true;
      this.isShowSkinFoldGraph = false;
      this.isShowWaterGraph = false;
      this.isShowBmiGraph = false;
      this.isShowBodyFatGraph = false;
      this.isShowLeanMassGraph = false;
    }
    if (this.charNum === 2) {
      this.getSkinFoldMeasurementInfo();
      //this.createLineChartForSkinFold();
      this.isShowWeightGraph = false;
      this.isShowGirthtGraph = false;
      this.isShowSkinFoldGraph = true;
      this.isShowWaterGraph = false;
      this.isShowBmiGraph = false;
      this.isShowBodyFatGraph = false;
      this.isShowLeanMassGraph = false;
    }
    if (this.charNum === 3) {
      this.getWaterPerMeasurementInfo();
      //this.createLineChartForWaterPercentage();
      this.isShowWeightGraph = false;
      this.isShowGirthtGraph = false;
      this.isShowSkinFoldGraph = false;
      this.isShowWaterGraph = true;
      this.isShowBmiGraph = false;
      this.isShowBodyFatGraph = false;
      this.isShowLeanMassGraph = false;
    }
    if (this.charNum === 4) {
      this.getBmiPerMeasurementInfo();
      // this.createLineChartForBMI();
      this.isShowWeightGraph = false;
      this.isShowGirthtGraph = false;
      this.isShowSkinFoldGraph = false;
      this.isShowWaterGraph = false;
      this.isShowBmiGraph = true;
      this.isShowBodyFatGraph = false;
      this.isShowLeanMassGraph = false;
    }
    if (this.charNum === 5) {
      this.getBodyFatPerMeasurementInfo();
      // this.createLineChartForBodyfatPer();
      this.isShowWeightGraph = false;
      this.isShowGirthtGraph = false;
      this.isShowSkinFoldGraph = false;
      this.isShowWaterGraph = false;
      this.isShowBmiGraph = false;
      this.isShowBodyFatGraph = true;
      this.isShowLeanMassGraph = false;
    }
    if (this.charNum === 6) {
      this.getLeanMassPerMeasurementInfo();
      // this.createLineChartForLeanMass();
      this.isShowWeightGraph = false;
      this.isShowGirthtGraph = false;
      this.isShowSkinFoldGraph = false;
      this.isShowWaterGraph = false;
      this.isShowBmiGraph = false;
      this.isShowBodyFatGraph = false;
      this.isShowLeanMassGraph = true;
    }
  }

  //to get weight measurement information
  getWeightMeasurementInfo() {
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();

    this.getcustomerassessment = this.assessment;
    for (let assess of this.getcustomerassessment) {
      this.customerAssessmentObj.customerWeight = assess.customerWeight;
      this.customerAssessmentObj.username = this.username;
    }

    this.girthObj.customerChest = null;
    this.girthObj.customerHip = null;
    this.girthObj.customerLeftArm = null;
    this.girthObj.customerLeftCalf = null;
    this.girthObj.customerLeftThigh = null;
    this.girthObj.customerNeck = null;
    this.girthObj.customerRightArm = null;
    this.girthObj.customerRightCalf = null;
    this.girthObj.customerRightThigh = null;
    this.girthObj.customerShoulder = null;
    this.girthObj.customerWaist = null;
    this.girthObj.whr = null;

    this.customerAssessmentObj.girth = this.girthObj;

    this.skinFoldObj.mmAbdominal = null;
    this.skinFoldObj.mmBicep = null;
    this.skinFoldObj.mmCalf = null;
    this.skinFoldObj.mmChest = null;
    this.skinFoldObj.mmMidaxillary = null;
    this.skinFoldObj.mmSkinFoldAverage = null;
    this.skinFoldObj.mmSkinFoldTotal = null;
    this.skinFoldObj.mmSubscapular = null;
    this.skinFoldObj.mmSuoraillac = null;
    this.skinFoldObj.mmThigh = null;
    this.skinFoldObj.mmTricep = null;

    this.customerAssessmentObj.skinFold = this.skinFoldObj;

    this.customerAssessmentObj.customerDoc = null;
    this.customerAssessmentObj.customerHieght = null;
    this.customerAssessmentObj.customerImage = null;
    this.customerAssessmentObj.customer_assessment_note = null;
    this.customerAssessmentObj.customer_bp = null;
    this.customerAssessmentObj.leanMass = null;
    this.customerAssessmentObj.mstCustomer = null;
    this.customerAssessmentObj.waterPercentage = null;
    this.customerAssessmentObj.customerBmi = null;

    console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.getcustomerassessmentgraphdata();
  }

  //to get girth measurement information
  getGirthMeasurementInfo() {
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();

    this.getcustomerassessment = this.assessment;
    for (let assess of this.getcustomerassessment) {
      this.girthObj.customerChest = assess.girth.customerChest;
      this.girthObj.customerHip = assess.girth.customerHip;
      this.girthObj.customerLeftArm = assess.girth.customerLeftArm;
      this.girthObj.customerLeftCalf = assess.girth.customerLeftCalf;
      this.girthObj.customerLeftThigh = assess.girth.customerLeftThigh;
      this.girthObj.customerNeck = assess.girth.customerNeck;
      this.girthObj.customerRightArm = assess.girth.customerRightArm;
      this.girthObj.customerRightCalf = assess.girth.customerRightCalf;
      this.girthObj.customerRightThigh = assess.girth.customerRightThigh;
      this.girthObj.customerShoulder = assess.girth.customerShoulder;
      this.girthObj.customerWaist = assess.girth.customerWaist;
      this.girthObj.whr = assess.girth.whr;
      this.customerAssessmentObj.username = this.username;
    }



    this.customerAssessmentObj.girth = this.girthObj;

    this.skinFoldObj.mmAbdominal = null;
    this.skinFoldObj.mmBicep = null;
    this.skinFoldObj.mmCalf = null;
    this.skinFoldObj.mmChest = null;
    this.skinFoldObj.mmMidaxillary = null;
    this.skinFoldObj.mmSkinFoldAverage = null;
    this.skinFoldObj.mmSkinFoldTotal = null;
    this.skinFoldObj.mmSubscapular = null;
    this.skinFoldObj.mmSuoraillac = null;
    this.skinFoldObj.mmThigh = null;
    this.skinFoldObj.mmTricep = null;

    this.customerAssessmentObj.skinFold = this.skinFoldObj;

    this.customerAssessmentObj.customerDoc = null;
    this.customerAssessmentObj.customerHieght = null;
    this.customerAssessmentObj.customerImage = null;
    this.customerAssessmentObj.customer_assessment_note = null;
    this.customerAssessmentObj.customer_bp = null;
    this.customerAssessmentObj.leanMass = null;
    this.customerAssessmentObj.mstCustomer = null;
    this.customerAssessmentObj.waterPercentage = null;
    this.customerAssessmentObj.customerWeight = null;
    this.customerAssessmentObj.customerBmi = null;

    console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.getcustomerassessmentgraphdata();
  }


  //to get skin fold measurement information
  getSkinFoldMeasurementInfo() {
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();

    this.getcustomerassessment = this.assessment;
    for (let assess of this.getcustomerassessment) {
      this.skinFoldObj.mmAbdominal = assess.skinFold.mmAbdominal;
      this.skinFoldObj.mmBicep = assess.skinFold.mmBicep;
      this.skinFoldObj.mmCalf = assess.skinFold.mmCalf;
      this.skinFoldObj.mmChest = assess.skinFold.mmChest;
      this.skinFoldObj.mmMidaxillary = assess.skinFold.mmMidaxillary;
      this.skinFoldObj.mmSkinFoldAverage = assess.skinFold.mmSkinFoldAverage;
      this.skinFoldObj.mmSkinFoldTotal = assess.skinFold.mmSkinFoldTotal;
      this.skinFoldObj.mmSubscapular = assess.skinFold.mmSubscapular;
      this.skinFoldObj.mmSuoraillac = assess.skinFold.mmSuoraillac;
      this.skinFoldObj.mmThigh = assess.skinFold.mmThigh;
      this.skinFoldObj.mmTricep = assess.skinFold.mmTricep;
      this.customerAssessmentObj.username = this.username;
    }

    this.girthObj.customerChest = null;
    this.girthObj.customerHip = null;
    this.girthObj.customerLeftArm = null;
    this.girthObj.customerLeftCalf = null;
    this.girthObj.customerLeftThigh = null;
    this.girthObj.customerNeck = null;
    this.girthObj.customerRightArm = null;
    this.girthObj.customerRightCalf = null;
    this.girthObj.customerRightThigh = null;
    this.girthObj.customerShoulder = null;
    this.girthObj.customerWaist = null;
    this.girthObj.whr = null;

    this.customerAssessmentObj.girth = this.girthObj;

    this.customerAssessmentObj.skinFold = this.skinFoldObj;



    this.customerAssessmentObj.customerDoc = null;
    this.customerAssessmentObj.customerHieght = null;
    this.customerAssessmentObj.customerImage = null;
    this.customerAssessmentObj.customer_assessment_note = null;
    this.customerAssessmentObj.customer_bp = null;
    this.customerAssessmentObj.leanMass = null;
    this.customerAssessmentObj.mstCustomer = null;
    this.customerAssessmentObj.waterPercentage = null;
    this.customerAssessmentObj.customerWeight = null;
    this.customerAssessmentObj.customerBmi = null;

    console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.getcustomerassessmentgraphdata();
  }

  //to get water percentage measurement information
  getWaterPerMeasurementInfo() {
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();

    this.getcustomerassessment = this.assessment;
    for (let assess of this.getcustomerassessment) {
      this.customerAssessmentObj.waterPercentage = assess.waterPercentage;
      this.customerAssessmentObj.username = this.username;
    }

    this.girthObj.customerChest = null;
    this.girthObj.customerHip = null;
    this.girthObj.customerLeftArm = null;
    this.girthObj.customerLeftCalf = null;
    this.girthObj.customerLeftThigh = null;
    this.girthObj.customerNeck = null;
    this.girthObj.customerRightArm = null;
    this.girthObj.customerRightCalf = null;
    this.girthObj.customerRightThigh = null;
    this.girthObj.customerShoulder = null;
    this.girthObj.customerWaist = null;
    this.girthObj.whr = null;

    this.customerAssessmentObj.girth = this.girthObj;

    this.skinFoldObj.mmAbdominal = null;
    this.skinFoldObj.mmBicep = null;
    this.skinFoldObj.mmCalf = null;
    this.skinFoldObj.mmChest = null;
    this.skinFoldObj.mmMidaxillary = null;
    this.skinFoldObj.mmSkinFoldAverage = null;
    this.skinFoldObj.mmSkinFoldTotal = null;
    this.skinFoldObj.mmSubscapular = null;
    this.skinFoldObj.mmSuoraillac = null;
    this.skinFoldObj.mmThigh = null;
    this.skinFoldObj.mmTricep = null;

    this.customerAssessmentObj.skinFold = this.skinFoldObj;

    this.customerAssessmentObj.customerDoc = null;
    this.customerAssessmentObj.customerHieght = null;
    this.customerAssessmentObj.customerImage = null;
    this.customerAssessmentObj.customer_assessment_note = null;
    this.customerAssessmentObj.customer_bp = null;
    this.customerAssessmentObj.leanMass = null;
    this.customerAssessmentObj.mstCustomer = null;
    this.customerAssessmentObj.customerWeight = null;
    this.customerAssessmentObj.customerBmi = null;
    //this.customerAssessmentObj.waterPercentage = null;

    console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.getcustomerassessmentgraphdata();
  }

  //to get BMI percentage measurement information
  getBmiPerMeasurementInfo() {
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();

    this.getcustomerassessment = this.assessment;
    for (let assess of this.getcustomerassessment) {
      this.customerAssessmentObj.customerBmi = assess.customerBmi;
      this.customerAssessmentObj.username = this.username;
    }

    this.girthObj.customerChest = null;
    this.girthObj.customerHip = null;
    this.girthObj.customerLeftArm = null;
    this.girthObj.customerLeftCalf = null;
    this.girthObj.customerLeftThigh = null;
    this.girthObj.customerNeck = null;
    this.girthObj.customerRightArm = null;
    this.girthObj.customerRightCalf = null;
    this.girthObj.customerRightThigh = null;
    this.girthObj.customerShoulder = null;
    this.girthObj.customerWaist = null;
    this.girthObj.whr = null;

    this.customerAssessmentObj.girth = this.girthObj;

    this.skinFoldObj.mmAbdominal = null;
    this.skinFoldObj.mmBicep = null;
    this.skinFoldObj.mmCalf = null;
    this.skinFoldObj.mmChest = null;
    this.skinFoldObj.mmMidaxillary = null;
    this.skinFoldObj.mmSkinFoldAverage = null;
    this.skinFoldObj.mmSkinFoldTotal = null;
    this.skinFoldObj.mmSubscapular = null;
    this.skinFoldObj.mmSuoraillac = null;
    this.skinFoldObj.mmThigh = null;
    this.skinFoldObj.mmTricep = null;

    this.customerAssessmentObj.skinFold = this.skinFoldObj;

    this.customerAssessmentObj.customerDoc = null;
    this.customerAssessmentObj.customerHieght = null;
    this.customerAssessmentObj.customerImage = null;
    this.customerAssessmentObj.customer_assessment_note = null;
    this.customerAssessmentObj.customer_bp = null;
    this.customerAssessmentObj.leanMass = null;
    this.customerAssessmentObj.mstCustomer = null;
    this.customerAssessmentObj.customerWeight = null;
    this.customerAssessmentObj.waterPercentage = null;

    console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.getcustomerassessmentgraphdata();
  }

  //to get body fat percentage measurement information
  getBodyFatPerMeasurementInfo() {
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();

    this.getcustomerassessment = this.assessment;
    for (let assess of this.getcustomerassessment) {
      this.customerAssessmentObj.bodyFatPercentage = assess.bodyFatPercentage;
      this.customerAssessmentObj.username = this.username;
    }

    this.girthObj.customerChest = null;
    this.girthObj.customerHip = null;
    this.girthObj.customerLeftArm = null;
    this.girthObj.customerLeftCalf = null;
    this.girthObj.customerLeftThigh = null;
    this.girthObj.customerNeck = null;
    this.girthObj.customerRightArm = null;
    this.girthObj.customerRightCalf = null;
    this.girthObj.customerRightThigh = null;
    this.girthObj.customerShoulder = null;
    this.girthObj.customerWaist = null;
    this.girthObj.whr = null;

    this.customerAssessmentObj.girth = this.girthObj;

    this.skinFoldObj.mmAbdominal = null;
    this.skinFoldObj.mmBicep = null;
    this.skinFoldObj.mmCalf = null;
    this.skinFoldObj.mmChest = null;
    this.skinFoldObj.mmMidaxillary = null;
    this.skinFoldObj.mmSkinFoldAverage = null;
    this.skinFoldObj.mmSkinFoldTotal = null;
    this.skinFoldObj.mmSubscapular = null;
    this.skinFoldObj.mmSuoraillac = null;
    this.skinFoldObj.mmThigh = null;
    this.skinFoldObj.mmTricep = null;

    this.customerAssessmentObj.skinFold = this.skinFoldObj;

    this.customerAssessmentObj.customerDoc = null;
    this.customerAssessmentObj.customerHieght = null;
    this.customerAssessmentObj.customerImage = null;
    this.customerAssessmentObj.customer_assessment_note = null;
    this.customerAssessmentObj.customer_bp = null;
    this.customerAssessmentObj.leanMass = null;
    this.customerAssessmentObj.mstCustomer = null;
    this.customerAssessmentObj.customerWeight = null;
    this.customerAssessmentObj.waterPercentage = null;

    console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.getcustomerassessmentgraphdata();
  }

  //to get lean mass percentage measurement information
  getLeanMassPerMeasurementInfo() {
    this.customerAssessmentObj = new CustomerAssessment();
    this.girthObj = new Girth();
    this.skinFoldObj = new SkinFold();

    this.getcustomerassessment = this.assessment;
    for (let assess of this.getcustomerassessment) {
      this.customerAssessmentObj.leanMass = assess.leanMass;
      this.customerAssessmentObj.username = this.username;
    }

    this.girthObj.customerChest = null;
    this.girthObj.customerHip = null;
    this.girthObj.customerLeftArm = null;
    this.girthObj.customerLeftCalf = null;
    this.girthObj.customerLeftThigh = null;
    this.girthObj.customerNeck = null;
    this.girthObj.customerRightArm = null;
    this.girthObj.customerRightCalf = null;
    this.girthObj.customerRightThigh = null;
    this.girthObj.customerShoulder = null;
    this.girthObj.customerWaist = null;
    this.girthObj.whr = null;

    this.customerAssessmentObj.girth = this.girthObj;

    this.skinFoldObj.mmAbdominal = null;
    this.skinFoldObj.mmBicep = null;
    this.skinFoldObj.mmCalf = null;
    this.skinFoldObj.mmChest = null;
    this.skinFoldObj.mmMidaxillary = null;
    this.skinFoldObj.mmSkinFoldAverage = null;
    this.skinFoldObj.mmSkinFoldTotal = null;
    this.skinFoldObj.mmSubscapular = null;
    this.skinFoldObj.mmSuoraillac = null;
    this.skinFoldObj.mmThigh = null;
    this.skinFoldObj.mmTricep = null;

    this.customerAssessmentObj.skinFold = this.skinFoldObj;

    this.customerAssessmentObj.customerDoc = null;
    this.customerAssessmentObj.customerHieght = null;
    this.customerAssessmentObj.customerImage = null;
    this.customerAssessmentObj.customer_assessment_note = null;
    this.customerAssessmentObj.customer_bp = null;
    //this.customerAssessmentObj.leanMass = null;
    this.customerAssessmentObj.mstCustomer = null;
    this.customerAssessmentObj.customerWeight = null;
    this.customerAssessmentObj.waterPercentage = null;

    console.log("getcustomerassessment to send:::::" + JSON.stringify(this.customerAssessmentObj));

    this.getcustomerassessmentgraphdata();
  }



  getcustomerassessmentgraphdata() {

    this.myProgressProvider.getCustomerAssessmentGraphData(this.customerAssessmentObj).then(res => {
      this.response = res;
      console.log("aaaaaaa:::" + JSON.stringify(this.response));
      if (this.response.status == 200) {
        this.customer_weight = this.response.responseObj.getcustomerassessmentgraphdata.customer_weight;


        this.customer_chest = this.response.responseObj.getcustomerassessmentgraphdata.customer_chest;
        this.customer_waist = this.response.responseObj.getcustomerassessmentgraphdata.customer_waist;
        this.customer_neck = this.response.responseObj.getcustomerassessmentgraphdata.customer_neck;
        this.customer_left_thigh = this.response.responseObj.getcustomerassessmentgraphdata.customer_left_thigh;
        this.customer_right_calf = this.response.responseObj.getcustomerassessmentgraphdata.customer_right_calf;
        this.customer_right_arm = this.response.responseObj.getcustomerassessmentgraphdata.customer_right_arm;
        this.customer_hip = this.response.responseObj.getcustomerassessmentgraphdata.customer_hip;
        this.customer_left_calf = this.response.responseObj.getcustomerassessmentgraphdata.customer_left_calf;
        this.customer_shoulder = this.response.responseObj.getcustomerassessmentgraphdata.customer_shoulder;
        this.customer_left_arm = this.response.responseObj.getcustomerassessmentgraphdata.customer_left_arm;
        this.whr = this.response.responseObj.getcustomerassessmentgraphdata.whr;
        this.customer_right_thigh = this.response.responseObj.getcustomerassessmentgraphdata.customer_right_thigh;

        this.mm_chest = this.response.responseObj.getcustomerassessmentgraphdata.mm_chest;
        this.mm_tricep = this.response.responseObj.getcustomerassessmentgraphdata.mm_tricep;
        this.mm_calf = this.response.responseObj.getcustomerassessmentgraphdata.mm_calf;
        this.mm_bicep = this.response.responseObj.getcustomerassessmentgraphdata.mm_bicep;
        this.mm_skin_average = this.response.responseObj.getcustomerassessmentgraphdata.mm_skin_average;
        this.mm_midaxillary = this.response.responseObj.getcustomerassessmentgraphdata.mm_midaxillary;
        this.mm_skin_fold_total = this.response.responseObj.getcustomerassessmentgraphdata.mm_skin_fold_total;
        this.mm_abdominal = this.response.responseObj.getcustomerassessmentgraphdata.mm_abdominal;
        this.mm_subscapular = this.response.responseObj.getcustomerassessmentgraphdata.mm_subscapular;
        this.mm_thigh = this.response.responseObj.getcustomerassessmentgraphdata.mm_thigh;
        this.mm_suprailiac = this.response.responseObj.getcustomerassessmentgraphdata.mm_suprailiac;

        this.water_percentage = this.response.responseObj.getcustomerassessmentgraphdata.water_percentage;

        this.customer_bmi = this.response.responseObj.getcustomerassessmentgraphdata.customer_bmi;

        this.body_fat_percentage = this.response.responseObj.getcustomerassessmentgraphdata.body_fat_percentage;

        this.lean_mass = this.response.responseObj.getcustomerassessmentgraphdata.lean_mass;

        //assign weight values to plot graph
        if (typeof this.customer_weight !== 'undefined' && this.customer_weight.length > 0) {
          console.log("this.myProgressProvider.customer_weight:::::" + JSON.stringify(this.customer_weight));
          for (let weight of this.customer_weight) {
            console.log("Weight****" + JSON.stringify(weight));
            this.weightDateList.push(weight.dateKey);
            console.log(this.weightDateList);
            if (weight.plotValue != null && weight.plotValue != "") {
              let weightValue = weight.plotValue.split(" ");
              if (weightValue[0] != "") {
                this.weightPlotValues.push(weightValue[0]);
              console.log("this.weightPlotValues****" + this.weightPlotValues);
              }
              

            }
          }
          this.createLineChartForWeight();
        }



        //for girth
        if (typeof this.customer_chest !== 'undefined' && this.customer_chest.length > 0) {
          //console.log("this.myProgressProvider.customer_chest:::::" + JSON.stringify(this.myProgressProvider.customer_chest));
          for (let chest of this.customer_chest) {
            console.log("chest****" + JSON.stringify(chest));
            this.chestDateList.push(chest.dateKey);
            console.log(this.chestDateList);
            if (chest.plotValue != null && chest.plotValue != "") {
              let chestValue = chest.plotValue.split(" ");
              if (chestValue[0] != "") {
                this.chestPlotValues.push(chestValue[0]);
                console.log(this.chestPlotValues);
              }

            }
          }
          this.createLineChartForGirth();
        }
        // else{
        //   //this.showAlert("Error", "Graph data is not available.")
        // }

        if (typeof this.customer_waist !== 'undefined' && this.customer_waist.length > 0) {
          //console.log("this.myProgressProvider.customer_waist:::::" + JSON.stringify(this.myProgressProvider.customer_waist));
          for (let waist of this.customer_waist) {
            console.log("chest****" + JSON.stringify(waist));
            this.waistDateList.push(waist.dateKey);
            //console.log(this.chestDateList);
            if (waist.plotValue != null && waist.plotValue != "") {
              let waistValue = waist.plotValue.split(" ");
              if (waistValue[0] != "") {
                this.waistPlotValues.push(waistValue[0]);
                //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }
        // else{
        //   //this.showAlert("Error", "Graph data is not available.")
        // }


        if (typeof this.customer_neck !== 'undefined' && this.customer_neck.length > 0) {
          //console.log("this.myProgressProvider.customer_neck:::::" + JSON.stringify(this.myProgressProvider.customer_neck));
          for (let neck of this.customer_neck) {
            //console.log("chest****" + JSON.stringify(neck));
            this.neckDateList.push(neck.dateKey);
            //console.log(this.chestDateList);
            if (neck.plotValue != null && neck.plotValue != "") {
              let neckValue = neck.plotValue.split(" ");
              if (neckValue[0] != "") {
                this.neckPlotValues.push(neckValue[0]);
                //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }
        // else{
        //   //this.showAlert("Error", "Graph data is not available.")
        // }


        if (typeof this.customer_left_thigh !== 'undefined' && this.customer_left_thigh.length > 0) {
          //console.log("this.myProgressProvider.customer_left_thigh:::::" + JSON.stringify(this.myProgressProvider.customer_left_thigh));
          for (let leftThigh of this.customer_left_thigh) {
            //console.log("chest****" + JSON.stringify(neck));
            this.leftThighDateList.push(leftThigh.dateKey);
            //console.log(this.chestDateList);
            if (leftThigh.plotValue != null && leftThigh.plotValue != "") {
              let leftThighValue = leftThigh.plotValue.split(" ");
              if (leftThighValue[0] != "") {
                this.leftThighPlotValues.push(leftThighValue[0]);
                //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }
        // else{
        //   //this.showAlert("Error", "Graph data is not available.")
        // }

        if (typeof this.customer_right_calf !== 'undefined' && this.customer_right_calf.length > 0) {
          //console.log("this.myProgressProvider.customer_right_calf:::::" + JSON.stringify(this.myProgressProvider.customer_right_calf));
          for (let rightCalf of this.customer_right_calf) {
            //console.log("chest****" + JSON.stringify(neck));
            this.rightCalfDateList.push(rightCalf.dateKey);
            //console.log(this.chestDateList);
            if (rightCalf.plotValue != null && rightCalf.plotValue != "") {
              let rightCalfValue = rightCalf.plotValue.split(" ");
              if (rightCalfValue[0] != "") {
                this.rightCalfPlotValues.push(rightCalfValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }


        if (typeof this.customer_right_arm !== 'undefined' && this.customer_right_arm.length > 0) {
          // console.log("this.myProgressProvider.customer_right_arm:::::" + JSON.stringify(this.myProgressProvider.customer_right_arm));
          for (let rightArm of this.customer_right_arm) {
            //console.log("chest****" + JSON.stringify(neck));
            this.rightArmDateList.push(rightArm.dateKey);
            //console.log(this.chestDateList);
            if (rightArm.plotValue != null && rightArm.plotValue != "") {
              let rightArmValue = rightArm.plotValue.split(" ");
              if (rightArmValue[0] != "") {
                this.rightArmPlotValues.push(rightArmValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }

        if (typeof this.customer_hip !== 'undefined' && this.customer_hip.length > 0) {
          //console.log("this.myProgressProvider.customer_hip:::::" + JSON.stringify(this.myProgressProvider.customer_hip));
          for (let hip of this.customer_hip) {
            //console.log("chest****" + JSON.stringify(neck));
            this.hipDateList.push(hip.dateKey);
            //console.log(this.chestDateList);
            if (hip.plotValue != null && hip.plotValue != "") {
              let hipValue = hip.plotValue.split(" ");
              if (hipValue[0] != "") {
                this.HipPlotValues.push(hipValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }

        if (typeof this.customer_left_calf !== 'undefined' && this.customer_left_calf.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let leftCalf of this.customer_left_calf) {
            //console.log("chest****" + JSON.stringify(neck));
            this.leftCalfDateList.push(leftCalf.dateKey);
            //console.log(this.chestDateList);
            if (leftCalf.plotValue != null && leftCalf.plotValue != "") {
              let leftCalfValue = leftCalf.plotValue.split(" ");
              if (leftCalfValue[0] != "") {
                this.leftCalfPlotValues.push(leftCalfValue[0]);
                //console.log(this.chestPlotValues);
              }
             
            }
          }
          this.createLineChartForGirth();
        }

        if (typeof this.customer_shoulder !== 'undefined' && this.customer_shoulder.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let shoulder of this.customer_shoulder) {
            //console.log("chest****" + JSON.stringify(neck));
            this.shoulderDateList.push(shoulder.dateKey);
            //console.log(this.chestDateList);
            if (shoulder.plotValue != null && shoulder.plotValue != "") {
              let shoulderValue = shoulder.plotValue.split(" ");
              if (shoulderValue[0] != "") {
                this.shoulderPlotValues.push(shoulderValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }

        if (typeof this.customer_left_arm !== 'undefined' && this.customer_left_arm.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let leftArm of this.customer_left_arm) {
            //console.log("chest****" + JSON.stringify(neck));
            this.leftArmDateList.push(leftArm.dateKey);
            //console.log(this.chestDateList);
            if (leftArm.plotValue != null && leftArm.plotValue != "") {
              let leftArmValue = leftArm.plotValue.split(" ");
              if (leftArmValue[0] != "") {
                this.leftArmPlotValues.push(leftArmValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }

        if (typeof this.whr !== 'undefined' && this.whr.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let whr of this.whr) {
            //console.log("chest****" + JSON.stringify(neck));
            this.whrDateList.push(whr.dateKey);
            //console.log(this.chestDateList);
            if (whr.plotValue != null && whr.plotValue != "") {
              let whrValue = whr.plotValue.split(" ");
              if (whrValue[0] != "") {
                this.whrPlotValues.push(whrValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }

        if (typeof this.customer_right_thigh !== 'undefined' && this.customer_right_thigh.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let rightThigh of this.customer_right_thigh) {
            //console.log("chest****" + JSON.stringify(neck));
            this.rightThighDateList.push(rightThigh.dateKey);
            //console.log(this.chestDateList);
            if (rightThigh.plotValue != null && rightThigh.plotValue != "") {
              let rightThighValue = rightThigh.plotValue.split(" ");
              if (rightThighValue[0] != "") {
                this.rightThighPlotValues.push(rightThighValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForGirth();
        }

        //for skin fold
        if (typeof this.mm_chest !== 'undefined' && this.mm_chest.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmChest of this.mm_chest) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmChestDateList.push(mmChest.dateKey);
            //console.log(this.chestDateList);
            if (mmChest.plotValue != null && mmChest.plotValue != "") {
              let mmChestValue = mmChest.plotValue.split(" ");
              if (mmChestValue[0] != "") {
                this.mmChestPlotValues.push(mmChestValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();

        }

        if (typeof this.mm_tricep !== 'undefined' && this.mm_tricep.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmTricep of this.mm_tricep) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmTricepDateList.push(mmTricep.dateKey);
            //console.log(this.chestDateList);
            if (mmTricep.plotValue != null && mmTricep.plotValue != "") {
              let mmTriceptValue = mmTricep.plotValue.split(" ");
              if (mmTriceptValue[0] != "") {
                this.mmTricepPlotValues.push(mmTriceptValue[0]);
                //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();
        }

        if (typeof this.mm_calf !== 'undefined' && this.mm_calf.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmCalf of this.mm_calf) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmCalfDateList.push(mmCalf.dateKey);
            //console.log(this.chestDateList);
            if (mmCalf.plotValue != null && mmCalf.plotValue != "") {
              let mmCalfValue = mmCalf.plotValue.split(" ");
              if (mmCalfValue[0] != "") {
                this.mmCalfPlotValues.push(mmCalfValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();
        }

        if (typeof this.mm_bicep !== 'undefined' && this.mm_bicep.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmBicep of this.mm_bicep) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmBicepDateList.push(mmBicep.dateKey);
            //console.log(this.chestDateList);
            if (mmBicep.plotValue != null && mmBicep.plotValue != "") {
              let mmBicepValue = mmBicep.plotValue.split(" ");
              if (mmBicepValue[0] != "") {
                this.mmBicepPlotValues.push(mmBicepValue[0]);
                //console.log(this.chestPlotValues);
              }
             
            }
          }
          this.createLineChartForSkinFold();
        }

        // if (typeof this.mm_skin_average !== 'undefined' && this.mm_skin_average.length > 0) {
        //   //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
        //   for (let mmSkinAverage of this.mm_skin_average) {
        //     //console.log("chest****" + JSON.stringify(neck));
        //     this.mmSkinAverageDateList.push(mmSkinAverage.dateKey);
        //     //console.log(this.chestDateList);
        //     if (mmSkinAverage.plotValue != null && mmSkinAverage.plotValue != "") {
        //       let mmSkinAverageValue = mmSkinAverage.plotValue.split(" ");
        //       if (mmSkinAverageValue[0] != "") {
        //         this.mmSkinAveragePlotValues.push(mmSkinAverageValue[0]);
        //       //console.log(this.chestPlotValues);
        //       }
              
        //     }
        //   }
        //   this.createLineChartForSkinFold();
        // }

        if (typeof this.mm_midaxillary !== 'undefined' && this.mm_midaxillary.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmMidaxillary of this.mm_midaxillary) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmMidaxillaryDateList.push(mmMidaxillary.dateKey);
            //console.log(this.chestDateList);
            if (mmMidaxillary.plotValue != null && mmMidaxillary.plotValue != "") {
              let mmMidaxillaryValue = mmMidaxillary.plotValue.split(" ");
              if (mmMidaxillaryValue[0] != "") {
                this.mmMidaxillaryPlotValues.push(mmMidaxillaryValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();
        }

        // if (typeof this.mm_skin_fold_total !== 'undefined' && this.mm_skin_fold_total.length > 0) {
        //   //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
        //   for (let mmSkinFoldTotal of this.mm_skin_fold_total) {
        //     //console.log("chest****" + JSON.stringify(neck));
        //     this.mmSkinFoldTotalDateList.push(mmSkinFoldTotal.dateKey);
        //     //console.log(this.chestDateList);
        //     if (mmSkinFoldTotal.plotValue != null && mmSkinFoldTotal.plotValue != "") {
        //       let mmSkinFoldTotalValue = mmSkinFoldTotal.plotValue.split(" ");
        //       if (mmSkinFoldTotalValue[0] != "") {
        //         this.mmSkinFoldTotalPlotValues.push(mmSkinFoldTotalValue[0]);
        //       //console.log(this.chestPlotValues);
        //       }
              
        //     }
        //   }
        //   this.createLineChartForSkinFold();
        // }

        if (typeof this.mm_abdominal !== 'undefined' && this.mm_abdominal.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmAbdominal of this.mm_abdominal) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmAbdominalDateList.push(mmAbdominal.dateKey);
            //console.log(this.chestDateList);
            if (mmAbdominal.plotValue != null && mmAbdominal.plotValue != "") {
              let mmAbdominalValue = mmAbdominal.plotValue.split(" ");
              if (mmAbdominalValue[0] != "") {
                this.mmAbdominalPlotValues.push(mmAbdominalValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();
        }

        if (typeof this.mm_subscapular !== 'undefined' && this.mm_subscapular.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmSubscapular of this.mm_subscapular) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmSubscapularlDateList.push(mmSubscapular.dateKey);
            //console.log(this.chestDateList);
            if (mmSubscapular.plotValue != null && mmSubscapular.plotValue != "") {
              let mmSubscapularValue = mmSubscapular.plotValue.split(" ");
              if (mmSubscapularValue[0] != "") {
                this.mmSubscapularPlotValues.push(mmSubscapularValue[0]);
                //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();
        }

        if (typeof this.mm_thigh !== 'undefined' && this.mm_thigh.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmThigh of this.mm_thigh) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmThighDateList.push(mmThigh.dateKey);
            //console.log(this.chestDateList);
            if (mmThigh.plotValue != null && mmThigh.plotValue != "") {
              let mmThighValue = mmThigh.plotValue.split(" ");
              if (mmThighValue[0] != "") {
                this.mmThighPlotValues.push(mmThighValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();
        }

        if (typeof this.mm_suprailiac !== 'undefined' && this.mm_suprailiac.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let mmSuprailiac of this.mm_suprailiac) {
            //console.log("chest****" + JSON.stringify(neck));
            this.mmSuprailiacDateList.push(mmSuprailiac.dateKey);
            //console.log(this.chestDateList);
            if (mmSuprailiac.plotValue != null && mmSuprailiac.plotValue != "") {
              let mmSuprailiacValue = mmSuprailiac.plotValue.split(" ");
              if (mmSuprailiacValue[0] != "") {
                this.mmSuprailiacPlotValues.push(mmSuprailiacValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForSkinFold();
        }

        //for water percentage
        if (typeof this.water_percentage !== 'undefined' && this.water_percentage.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let waterPercentage of this.water_percentage) {
            //console.log("chest****" + JSON.stringify(neck));
            this.waterPercentageDateList.push(waterPercentage.dateKey);
            //console.log(this.chestDateList);
            if (waterPercentage.plotValue != null && waterPercentage.plotValue != "") {
              let waterPercentageValue = waterPercentage.plotValue.split(" ");
              if (waterPercentageValue[0] != "") {
                this.waterPercentagePlotValues.push(waterPercentageValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForWaterPercentage();
        }

        //for BMI percentage
        if (typeof this.customer_bmi !== 'undefined' && this.customer_bmi.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let bmi of this.customer_bmi) {
            //console.log("chest****" + JSON.stringify(neck));
            this.bmiDateList.push(bmi.dateKey);
            //console.log(this.chestDateList);
            if (bmi.plotValue != null && bmi.plotValue != "") {
              let bmiValue = bmi.plotValue.split(" ");
              if (bmiValue[0] != "") {
                this.bmiPlotValues.push(bmiValue[0]);
              //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForBMI();
        }


        //for body fat percentage
        if (typeof this.body_fat_percentage !== 'undefined' && this.body_fat_percentage.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let bodyFat of this.body_fat_percentage) {
            //console.log("chest****" + JSON.stringify(neck));
            this.bodyFatDateList.push(bodyFat.dateKey);
            //console.log(this.chestDateList);
            if (bodyFat.plotValue != null && bodyFat.plotValue != "") {
              let bodyFatValue = bodyFat.plotValue.split(" ");
              if (bodyFatValue[0] != "") {
                this.bodyFatPlotValues.push(bodyFatValue[0]);
                //console.log(this.chestPlotValues);
              }
              
            }
          }
          this.createLineChartForBodyfatPer();
        }

        //for lean mass percentage
        if (typeof this.lean_mass !== 'undefined' && this.lean_mass.length > 0) {
          //console.log("this.myProgressProvider.customer_left_calf:::::" + JSON.stringify(this.myProgressProvider.customer_left_calf));
          for (let leanMass of this.lean_mass) {
            //console.log("chest****" + JSON.stringify(neck));
            this.leanMassDateList.push(leanMass.dateKey);
            //console.log(this.chestDateList);
            if (leanMass.plotValue != null && leanMass.plotValue != "") {
              let leanMassValue = leanMass.plotValue.split(" ");
              if (leanMassValue[0] != "") {
                this.leanMassPlotValues.push(leanMassValue[0]);
                //console.log(this.chestPlotValues);
              }
             
            }
          }
          this.createLineChartForLeanMass();
        }


      }
    })


    // else {
    //   this.showAlert("Error", "Graph data is not available.")
    // }




  }

  //to show weight grapg
  createLineChartForWeight() {

    this.lineChartWt = new Chart(this.lineChartWeight.nativeElement, {

      type: 'line',
      data: {
        //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        labels: this.weightDateList,
        datasets: [
          {
            label: "Weight",
            fill: true,
            lineTension: 0.1,
            //backgroundColor: "rgb(75,192,192,0.8)",
            //borderColor: "rgb(75,192,192,1)",
            backgroundColor: "rgba(168, 219, 230, 0.5)",
            borderColor: "rgba(168, 219, 230, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [10, 65, 59, 80, 81],
            data: this.weightPlotValues,
            spanGaps: true,
          }
        ]
      }

    });
  }




  //to show girth graph
  createLineChartForGirth() {
    this.lineChartG = new Chart(this.lineChartGirth.nativeElement, {

      type: 'line',
      data: {
        //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        labels: this.chestDateList,
        datasets: [
          {
            label: "Chest",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.5)",
            borderColor: "rgba(75,192,192,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [65, 59, 80, 81, 56, 55, 40, 55, 44, 56, 59, 46],
            data: this.chestPlotValues,
            spanGaps: true,
          },
          {
            label: "Waist",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(225,0,0,0.5)",
            borderColor: "rgba(225,0,0,0.7)", // The main line color
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [10, 20, 30, 41, 50, 65, 40, 35, 74, 36, 69, 26],
            data: this.waistPlotValues,
            spanGaps: true,
          },
          {
            label: "Neck",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,0.5)",
            borderColor: "rgba(255,255,0,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [10, 20, 60, 95, 64, 78, 90, 50, 70, 40, 70, 89],
            data: this.neckPlotValues,
            spanGaps: false,
          },
          {
            label: "Left Thigh",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,255,0.5)",
            borderColor: "rgba(0,0,255,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.leftThighPlotValues,
            spanGaps: true,
          },
          {
            label: "Right Calf",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,255,0.5)",
            borderColor: "rgba(255,0,255,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.rightCalfPlotValues,
            spanGaps: true,
          },
          {
            label: "Right Arm",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(192,192,192,0.5)",
            borderColor: "rgba(192,192,192,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.rightArmPlotValues,
            spanGaps: true,
          },
          {
            label: "Hip",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.5)",
            borderColor: "rgba(255,0,0,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.HipPlotValues,
            spanGaps: true,
          },
          {
            label: "Left Calf",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(93, 190, 55, 0.5)",
            borderColor: "rgba(93, 190, 55, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            //pointRadius: 5,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.leftCalfPlotValues,
            spanGaps: true,
          },
          {
            label: "Shoulder",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(190, 55, 84, 0.5)",
            borderColor: "rgba(190, 55, 84, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.shoulderPlotValues,
            spanGaps: true,
          },
          {
            label: "left Arm",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(32, 9, 14, 0.5)",
            borderColor: "rgba(32, 9, 14, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.leftArmPlotValues,
            spanGaps: true,
          },
          {
            label: "Whr",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(9, 29, 32, 0.5)",
            borderColor: "rgba(9, 29, 32, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.whrPlotValues,
            spanGaps: true,
          },
          {
            label: "Right Thigh",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(77, 191, 203, 0.5)",
            borderColor: "rgba(77, 191, 203, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.rightThighPlotValues,
            spanGaps: true,
          }
        ]
      }

    });

  }

  //to show skinfolds graph
  createLineChartForSkinFold() {
    this.lineChartSF = new Chart(this.lineChartSkinFold.nativeElement, {

      type: 'line',
      data: {
        //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        labels: this.mmChestDateList,
        datasets: [
          {
            label: "Chest",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.5)",
            borderColor: "rgba(75,192,192,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [65, 59, 80, 81, 56, 55, 40, 55, 44, 56, 59, 46],
            data: this.mmChestPlotValues,
            spanGaps: true,
          },
          {
            label: "Tricep",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(225,0,0,0.5)",
            borderColor: "rgba(225,0,0,0.7)", // The main line color
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [10, 20, 30, 41, 50, 65, 40, 35, 74, 36, 69, 26],
            data: this.mmTricepPlotValues,
            spanGaps: true,
          },
          {
            label: "Calf",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,0.5)",
            borderColor: "rgba(255,255,0,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [10, 20, 60, 95, 64, 78, 90, 50, 70, 40, 70, 89],
            data: this.mmCalfPlotValues,
            spanGaps: false,
          },
          {
            label: "Bicep",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,255,0.5)",
            borderColor: "rgba(0,0,255,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmBicepPlotValues,
            spanGaps: true,
          },
          {
            label: "Skin Average",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,255,0.5)",
            borderColor: "rgba(255,0,255,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmSkinAveragePlotValues,
            spanGaps: true,
          },
          {
            label: "Midaxillary",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(192,192,192,0.5)",
            borderColor: "rgba(192,192,192,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmMidaxillaryPlotValues,
            spanGaps: true,
          },
          {
            label: "Skin Fold Total",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.5)",
            borderColor: "rgba(255,0,0,0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmSkinFoldTotalPlotValues,
            spanGaps: true,
          },
          {
            label: "Abdominal",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(93, 190, 55, 0.5)",
            borderColor: "rgba(93, 190, 55, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmAbdominalPlotValues,
            spanGaps: true,
          },
          {
            label: "Subscapular",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(190, 55, 84,0.5)",
            borderColor: "rgba(190, 55, 84, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmSubscapularPlotValues,
            spanGaps: true,
          },
          {
            label: "Thigh",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(32, 9, 14, 0.5)",
            borderColor: "rgba(32, 9, 14, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmThighPlotValues,
            spanGaps: true,
          },
          {
            label: "Suprailiac",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(9, 29, 32, 0.5)",
            borderColor: "rgba(9, 29, 32, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(75,192,192,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [15, 59, 30, 60, 20, 55, 70, 80, 40, 22, 39, 66],
            data: this.mmSuprailiacPlotValues,
            spanGaps: true,
          }
        ]
      }

    });
  }


  createLineChartForWaterPercentage() {
    this.lineChartWP = new Chart(this.lineChartWaterPer.nativeElement, {

      type: 'line',
      data: {
        //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        labels: this.waterPercentageDateList,
        datasets: [
          {
            label: "Water %",
            fill: true,
            lineTension: 0.1,
            backgroundColor: "rgba(168, 219, 230, 0.5)",
            borderColor: "rgba(168, 219, 230, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(168, 219, 230, 1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(168, 219, 230, 1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [65, 59, 80, 81, 56, 55, 40, 55, 44, 56, 59, 46],
            data: this.waterPercentagePlotValues,
            spanGaps: true,
          }
        ]
      }

    });
  }

  createLineChartForBMI() {
    this.lineChartBmi = new Chart(this.lineChartBMI.nativeElement, {

      type: 'line',
      data: {
        //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        labels: this.bmiDateList,
        datasets: [
          {
            label: "BMI %",
            fill: true,
            lineTension: 0.1,
            //backgroundColor: "rgb(172,110,239,0.8)",
            //borderColor: "rgb(172,110,239,1)",
            backgroundColor: "rgba(168, 219, 230, 0.5)",
            borderColor: "rgba(168, 219, 230, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgb(172,110,239,1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgb(172,110,239,1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [65, 59, 80, 81, 56, 55, 40, 55, 44, 56, 59, 46],
            data: this.bmiPlotValues,
            spanGaps: true,
          }
        ]
      }

    });
  }

  createLineChartForBodyfatPer() {
    this.lineChartBodyFatPer = new Chart(this.lineChartBodyfatPer.nativeElement, {

      type: 'line',
      data: {
        //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        labels: this.bodyFatDateList,
        datasets: [
          {
            label: "Body Fat %",
            fill: true,
            lineTension: 0.1,
            //backgroundColor: "rgba(227, 230, 168, 0.8)",
            //borderColor: "rgba(227, 230, 168, 1)",
            backgroundColor: "rgba(168, 219, 230, 0.5)",
            borderColor: "rgba(168, 219, 230, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(227, 230, 168, 1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(227, 230, 168, 1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [65, 59, 80, 81, 56, 55, 40, 55, 44, 56, 59, 46],
            data: this.bodyFatPlotValues,
            spanGaps: true,
          }
        ]
      }

    });
  }

  createLineChartForLeanMass() {
    this.lineChartleanmass = new Chart(this.lineChartLeanMass.nativeElement, {

      type: 'line',
      data: {
        //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        labels: this.leanMassDateList,
        datasets: [
          {
            label: "Lean Mass %",
            fill: true,
            lineTension: 0.1,
            //backgroundColor: "rgba(230, 168, 195, 0.8)",
            //borderColor: "rgba(230, 168, 195, 1)",
            backgroundColor: "rgba(168, 219, 230, 0.5)",
            borderColor: "rgba(168, 219, 230, 0.7)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            //pointBorderColor: "rgba(230, 168, 195, 1)",
            pointBorderColor: "rgba(4, 3, 1, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            // pointHoverBackgroundColor: "rgba(230, 168, 195, 1)",
            // pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBackgroundColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderColor: "rgba(255, 106, 56, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            //data: [65, 59, 80, 81, 56, 55, 40, 55, 44, 56, 59, 46],
            data: this.leanMassPlotValues,
            spanGaps: true,
          }
        ]
      }

    });
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
