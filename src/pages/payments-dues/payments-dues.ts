import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
// import { ViewPastDuesAndPaymentsPage } from '../../pages/view-past-dues-and-payments/view-past-dues-and-payments';
// import { PayNowPage } from '../../pages/pay-now/pay-now';
import { MyProfileProvider } from '../../providers/my-profile/my-profile';
import { CommonResponse } from '../../Model/common-response';
import { Payment } from '../../Model/payment-model';
import { PaymentDueDetail } from '../../Model/payment-due-detail';
import { PaymentDueDetailList } from '../../Model/payment-due-detail-list';


/**
 * Generated class for the PaymentsDuesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-payments-dues',
  templateUrl: 'payments-dues.html',
})
export class PaymentsDuesPage {


  username: string;
  response: CommonResponse;
  payment: Payment;
  //paymentDueDetail: PaymentDueDetail[] = [];
  paidDetailas: PaymentDueDetail[] = [];
  paidDetailsMultiple: PaymentDueDetail[] = [];
  paidDetailsMultipleList: PaymentDueDetailList[] = [];
  isMultipleListOfSameInivocePresent: boolean = false;
  isSingleListPresent: boolean = false;
  selectedPackage: PaymentDueDetail[] = [];
  selectedPackageObj: PaymentDueDetail;
  paymentDueDetailObj: PaymentDueDetail = new PaymentDueDetail();
  paymentDueDetailForMultipleObj: PaymentDueDetail = new PaymentDueDetail();
  testMultipleList: any[] =[];




  constructor(public navCtrl: NavController, public navParams: NavParams,
    public myProfileProvider: MyProfileProvider, public alertCtrl: AlertController) {

    this.username = localStorage.getItem('username');
    console.log("this.username in payment plan::::" + this.username);
    //this.getPaymentDueDetails(this.username);
    this.getPaymentDueOrPaidDetails(this.username);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentsDuesPage');
  }





  //get payment due or paid details
  getPaymentDueOrPaidDetails(username) {
    //this.presentLoading();
    this.myProfileProvider.getPaymentDueOrPaidDetails(username).then(res => {
      this.response = res;
      console.log("payment paid data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {
        if (this.response.responseObj.getpaymentdetails.length > 0) {
          this.testMultipleList = this.response.responseObj.getpaymentdetails;
          console.log("this.testMultipleList::::::"+JSON.stringify(this.testMultipleList));
          for (let paymentDue of this.response.responseObj.getpaymentdetails) {
           
            for(let payment of paymentDue){
             // this.paymentDueDetailForMultipleObj = null;
             // this.paidDetailsMultiple = [];
              console.log("Length of paymentlist::::"+paymentDue.length);
              if(paymentDue.length > 1){

                console.log("paymentDue.length in if::::"+paymentDue.length);
                this.isMultipleListOfSameInivocePresent = true;
                

                console.log("payment:::::"+JSON.stringify(paymentDue));
                
                if (payment.paidAmount !== 0 ) {
                  console.log("paid<<<>>>>>>" + JSON.stringify(payment));
                  this.paymentDueDetailForMultipleObj = payment;
                  this.paymentDueDetailForMultipleObj.checked = false;
                  if (payment.serviceType === 'Product') {
                    this.paymentDueDetailForMultipleObj.start_date = '';
                    this.paymentDueDetailForMultipleObj.end_date = '';
                  }
                  else {
                    this.paymentDueDetailForMultipleObj.start_date = payment.start_date;
                    this.paymentDueDetailForMultipleObj.end_date = payment.end_date;
                  }
    
                  this.paidDetailsMultiple.push(this.paymentDueDetailForMultipleObj);
                  console.log("this.paidDetailsMultiple:::::::::::"+JSON.stringify(this.paidDetailsMultiple));
                
    
                }
                console.log("this.paidDetailsMultiple::::::"+JSON.stringify(this.paidDetailsMultiple));
              }
              else{
                console.log("paymentDue.length in else::::"+paymentDue.length);
                this.isSingleListPresent = true;
                if (payment.paidAmount !== 0) {
                  console.log("paid<<<>>>>>>" + JSON.stringify(payment));
                  this.paymentDueDetailObj = payment;
                  this.paymentDueDetailObj.checked = false;
                  if (payment.serviceType === 'Product') {
                    this.paymentDueDetailObj.start_date = '';
                    this.paymentDueDetailObj.end_date = '';
                  }
                  else {
                    this.paymentDueDetailObj.start_date = payment.start_date;
                    this.paymentDueDetailObj.end_date = payment.end_date;
                  }
    
    
                  this.paidDetailas.push(this.paymentDueDetailObj);
    
    
                }
              }      
             
            }
          
            
           
          }
          console.log("this.paidDetailas::::"+JSON.stringify(this.paidDetailas));
        }
        else {
          this.showAlert("Alert", "Payment history is not available.");
        }

      }

      else {
        this.showAlert("Error", "Sorry! Server side error has occurred.");
      }
    })

  }



  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }


}
