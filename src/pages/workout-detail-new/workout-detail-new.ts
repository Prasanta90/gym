import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MoreInfoAboutExercisePage } from '../more-info-about-exercise/more-info-about-exercise';

/**
 * Generated class for the WorkoutDetailNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-workout-detail-new',
  templateUrl: 'workout-detail-new.html',
})
export class WorkoutDetailNewPage {
  selectedDate: Date;
  workouts: Array<any>;
  isChecked: boolean = false;
  previousDate: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.selectedDate = this.navParams.get('selectedDate');
    console.log("this.selectedDate in workout-detail-new:::::" + this.selectedDate);



    this.workouts = [
      { id: 1, name: "Push up" },
      { id: 2, name: "Pull up" },
      { id: 3, name: "Dips" }]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkoutDetailNewPage');
  }


  //get previous date
  previousButton(selectedDate) {
    var day = new Date(selectedDate);
    console.log("selectedDate::::"+day); 

    var previousDay = new Date(day);
    previousDay.setDate(day.getDate() - 1);
    console.log("previous day of the selected day"+previousDay); 
    //let date = new Date();
    //console.log(date.setDate(selectedDate.getDate() + 1));
    this.selectedDate = previousDay;
  }

  //get next date
  nextButton(selectedDate) {
    var day = new Date(selectedDate);
    console.log("selectedDate::::"+day); 

    var nextDay = new Date(day);
    nextDay.setDate(day.getDate() + 1);
    console.log("next day of the selected day"+nextDay); 
    //let date = new Date();
    //console.log(date.setDate(selectedDate.getDate() + 1));
    this.selectedDate = nextDay;
  }

  onCheckMultiple(selectedAll) {
    console.log(JSON.stringify(selectedAll));
    if (selectedAll != null && selectedAll != 0) {
      console.log("selected");
      this.isChecked = true;
    }
  }

  viewMoreInfoAboutExercise(category) {
    console.log("Category details:::::" + JSON.stringify(category));
    this.navCtrl.push(MoreInfoAboutExercisePage, { category: category });
  }

  saveLog() {

  }

}
