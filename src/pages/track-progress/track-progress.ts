import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ViewController, ModalController  } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Chart } from 'chart.js';
import { MyProgressPage } from '../../pages/my-progress/my-progress';
import { GraphModalsPage } from '../../pages/graph-modals/graph-modals';
import { CustomerAssessment } from '../../Model/customer-assessment';
//import { GraphPackageConsumptionPage } from '../graph-package-consumption/graph-package-consumption';

/**
 * Generated class for the TrackProgressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-track-progress',
  templateUrl: 'track-progress.html',
})
export class TrackProgressPage {

  assessment: CustomerAssessment[] = [];


  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform, 
    private menu: MenuController, public modalCtrl: ModalController) {
      this.assessment = this.navParams.get('assessment');
      console.log("this.assessment in track-progress****" + JSON.stringify(this.assessment));
  }


  openModal(characterNum: any, assessment: any) {

    // let modal = this.modalCtrl.create(GraphModalsPage, {
    //   charNum: characterNum, assessment: this.assessment
    // } );
    console.log("characterNum::::"+JSON.stringify(characterNum) );
    let modal = this.modalCtrl.create(GraphModalsPage, characterNum, assessment);
    modal.present();
  }

  ionViewDidLoad() {


  }

  

  

}
