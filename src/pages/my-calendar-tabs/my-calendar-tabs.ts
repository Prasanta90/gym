import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyProgressPage } from '../my-progress/my-progress';
import { MyCalenderPage } from '../my-calender/my-calender';
import { TodaySWorkoutNewPage} from '../today-s-workout-new/today-s-workout-new';
import { Myprofile} from '../../Model/my-profile';


/**
 * Generated class for the MyCalendarTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-calendar-tabs',
  templateUrl: 'my-calendar-tabs.html',
})
export class MyCalendarTabsPage {

  user: Myprofile;
  myCalenderRoot = MyCalenderPage
  myProgressPage = MyProgressPage
  todaySWorkoutPage = TodaySWorkoutNewPage
  calendarParams: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user=this.navParams.get('user');
    console.log("I'm in HometabsPage:::::::User Details:::::"+JSON.stringify(this.user));
    this.calendarParams = this.user;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyCalendarTabsPage');
  }

}
