import { FormsModule } from '@angular/forms';
//import { MbscModule } from '@mobiscroll/angular';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { CalendarModule } from 'ionic3-calendar-en';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { VideoPlayer } from '@ionic-native/video-player';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { DatePipe } from '@angular/common';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import {DocumentViewer} from '@ionic-native/document-viewer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Device } from '@ionic-native/device/ngx';
import { Random1Page } from './random1';
//import { EmbedVideo, EmbedVideoService } from 'ngx-embed-video';
//import { EmbedVideoService } from 'ngx-embed-video';

// Import ng-circle-progress
import { NgCircleProgressModule } from 'ng-circle-progress';
//import {RoundProgressModule} from 'angular-svg-round-progressbar';

// import {ProgressBarModule} from "angular-progress-bar"


//pipes
import { TextLimitPipe } from '../pipes/text-limit/text-limit';
import { SummaryPipe } from '../pipes/summary/summary';

//components
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { ExpandableComponent } from '../components/expandable/expandable';

//pages

import { LoginPage } from '../pages/login/login';
import { TrackProgressPage } from '../pages/track-progress/track-progress';
import { MyGymPage } from '../pages/my-gym/my-gym';
import { NutritionPlanPage } from '../pages/nutrition-plan/nutrition-plan';
import { ClubWorkoutPage } from '../pages/club-workout/club-workout';
import { MyProfilePage } from '../pages/my-profile/my-profile';
import { MyCalenderPage } from '../pages/my-calender/my-calender';
import { GymClassPage } from '../pages/gym-class/gym-class';
import { AccountSettingPage } from '../pages/account-setting/account-setting';
//import { AboutUsPage } from '../pages/about-us/about-us';
import { LogoutPage } from '../pages/logout/logout';
import { HometabsPage } from '../pages/hometabs/hometabs';
import { TodaySWorkoutPage } from '../pages/today-s-workout/today-s-workout';
import { MyProfileTabsPage } from '../pages/my-profile-tabs/my-profile-tabs';
//import { InboxPage } from '../pages/inbox/inbox';
import { NotificationsPage } from '../pages/notifications/notifications';
import { HealthInfoPage } from '../pages/health-info/health-info';
import { PaymentsDuesPage } from '../pages/payments-dues/payments-dues';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { MoreInfoAboutExercisePage } from '../pages/more-info-about-exercise/more-info-about-exercise';
import { GraphPackageConsumptionPage } from '../pages/graph-package-consumption/graph-package-consumption';
import { ViewPastDuesAndPaymentsPage } from '../pages/view-past-dues-and-payments/view-past-dues-and-payments';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { MyProgressPage } from '../pages/my-progress/my-progress';
import { AddMyProgressPage } from '../pages/add-my-progress/add-my-progress';
import { WorkoutDetailsPage } from '../pages/workout-details/workout-details';
import { TodaySWorkoutNewPage } from '../pages/today-s-workout-new/today-s-workout-new';
import { WorkoutDetailNewPage } from '../pages/workout-detail-new/workout-detail-new';
import { MoreInfoAboutExerciseNextPage } from '../pages/more-info-about-exercise-next/more-info-about-exercise-next';
import { SlidesPage } from '../pages/slides/slides';
import { GraphModalsPage } from '../pages/graph-modals/graph-modals';
import { MyProgressTabsPage } from '../pages/my-progress-tabs/my-progress-tabs';
import { MyWorkoutPlanPage } from '../pages/my-workout-plan/my-workout-plan';
import { PayNowPage } from '../pages/pay-now/pay-now';
import { TopPicksDetailPage } from '../pages/top-picks-detail/top-picks-detail';
import { MembershipPlanPage } from '../pages/membership-plan/membership-plan';
import { MembershipPlanTabsPage } from '../pages/membership-plan-tabs/membership-plan-tabs';
import { ScheduleMeetingPage } from '../pages/schedule-meeting/schedule-meeting';
import { MyCalendarTabsPage } from '../pages/my-calendar-tabs/my-calendar-tabs';
import { HelpPage } from '../pages/help/help';
import { MyWorkoutPlanTodayPage } from '../pages/my-workout-plan-today/my-workout-plan-today';
import { MyWorkoutPlanTodayTabsPage } from '../pages/my-workout-plan-today-tabs/my-workout-plan-today-tabs';
import { MealPlanForCurrentDatePage } from '../pages/meal-plan-for-current-date/meal-plan-for-current-date';
import { PlayVideoModalPage } from '../pages/play-video-modal/play-video-modal';
import { MoreInfoAboutClubExercisePage } from '../pages/more-info-about-club-exercise/more-info-about-club-exercise';
import { ClubWorkoutDescriptionPage } from '../pages/club-workout-description/club-workout-description';
import { MoreInfoAboutClubExerciseNextPage } from '../pages/more-info-about-club-exercise-next/more-info-about-club-exercise-next';
import { SwipeSegmentDirective } from '../directives/swipe-segment/swipe-segment';
import { MealPlanTodayTabsPage } from '../pages/meal-plan-today-tabs/meal-plan-today-tabs';
import { PaymentHistoryTabsPage } from '../pages/payment-history-tabs/payment-history-tabs';



//Services
import { CommonProvider } from '../providers/common/common';
import { LoginProvider } from '../providers/login/login';
import { MyProfileProvider } from '../providers/my-profile/my-profile';
import { from } from 'rxjs';
import { MyProgressProvider } from '../providers/my-progress/my-progress';
import { GlobalsProvider } from '../providers/globals/globals';
import { MyMealPlanProvider } from '../providers/my-meal-plan/my-meal-plan';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { Firebase } from '@ionic-native/firebase';
import { FcmService } from './../fcm.service';
import { WorkoutProvider } from '../providers/workout/workout';
//import { PaymentProvider } from '../providers/payment/payment';

import { Keyboard } from '@ionic-native/keyboard/ngx';

const config = {
  apiKey: 'AIzaSyDDSd6F1_blCdheDx5Sxeg2mY5OruhWN6M',
  authDomain: 'gymapp-50fa0.firebaseapp.com',
  databaseURL: 'https://gymapp-50fa0.firebaseio.com',
  projectId: 'gymapp-50fa0',
  storageBucket: 'gymapp-50fa0.appspot.com',
  messagingSenderId: '859352388317'
  };




@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    TodaySWorkoutPage,
    TrackProgressPage,
    MyGymPage,
    NutritionPlanPage,
    ClubWorkoutPage,
    MyProfilePage,
    MyCalenderPage,
    GymClassPage,
    AccountSettingPage,
    //AboutUsPage,
    LogoutPage,
    //InboxPage,
    NotificationsPage,
    HealthInfoPage,
    PaymentsDuesPage,
    HometabsPage,
    MyProfileTabsPage,
    ForgotPasswordPage,
    MoreInfoAboutExercisePage,
    GraphPackageConsumptionPage,
    ViewPastDuesAndPaymentsPage,
    ChangePasswordPage,
    MyProgressPage,
    AddMyProgressPage,
    WorkoutDetailsPage,
    TextLimitPipe,
    SummaryPipe,
    TodaySWorkoutNewPage,
    WorkoutDetailNewPage,
    MoreInfoAboutExerciseNextPage,
    SlidesPage,
    GraphModalsPage,
    MyProgressTabsPage,
    MyWorkoutPlanPage,
    PayNowPage,
    TopPicksDetailPage,
    MembershipPlanPage,
    MembershipPlanTabsPage,
    ScheduleMeetingPage,
    MyCalendarTabsPage,
    HelpPage,
    MyWorkoutPlanTodayPage,
    MyWorkoutPlanTodayTabsPage,
    MealPlanForCurrentDatePage,
    PlayVideoModalPage,
    MoreInfoAboutClubExercisePage,
    ClubWorkoutDescriptionPage,
    MoreInfoAboutClubExerciseNextPage,
    ProgressBarComponent,
    ExpandableComponent,
    SwipeSegmentDirective,
    MealPlanTodayTabsPage,
    PaymentHistoryTabsPage
  ],
  imports: [
    FormsModule, 
    //MbscModule,
    BrowserModule,
    CalendarModule,
    IonicModule.forRoot(MyApp),
    HttpModule, 
    HttpClientModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    //EmbedVideo.forRoot(),
  
    //ProgressBarModule
    //RoundProgressModule
    
    //Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 3,
      innerStrokeWidth: 3,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#ccc",
      animationDuration: 300
    }),
    

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    TodaySWorkoutPage,
    TrackProgressPage,
    MyGymPage,
    NutritionPlanPage,
    ClubWorkoutPage,
    MyProfilePage,
    MyCalenderPage,
    GymClassPage,
    AccountSettingPage,
    //AboutUsPage,
    LogoutPage,
    //InboxPage,
    NotificationsPage,
    HealthInfoPage,
    PaymentsDuesPage,
    HometabsPage,
    MyProfileTabsPage,
    ForgotPasswordPage,
    MoreInfoAboutExercisePage,
    GraphPackageConsumptionPage,
    ViewPastDuesAndPaymentsPage,
    ChangePasswordPage,
    MyProgressPage,
    AddMyProgressPage,
    WorkoutDetailsPage,
    TodaySWorkoutNewPage,
    WorkoutDetailNewPage,
    SlidesPage,
    GraphModalsPage,
    MyProgressTabsPage,
    MyWorkoutPlanPage,
    PayNowPage,
    TopPicksDetailPage,
    MembershipPlanPage,
    MembershipPlanTabsPage,
    ScheduleMeetingPage,
    MyCalendarTabsPage,
    HelpPage,
    MyWorkoutPlanTodayPage,
    MyWorkoutPlanTodayTabsPage,
    MoreInfoAboutExerciseNextPage,
    MealPlanForCurrentDatePage,
    PlayVideoModalPage,
    MoreInfoAboutClubExercisePage,
    ClubWorkoutDescriptionPage,
    MoreInfoAboutClubExerciseNextPage,
    MealPlanTodayTabsPage,
    PaymentHistoryTabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommonProvider,
    FileTransfer,
    FileTransferObject,
    File,
    DocumentViewer,
    //DocumentViewer,
    Camera,
    YoutubeVideoPlayer,
    LoginProvider,
    MyProfileProvider,
    MyProgressProvider,
    DatePipe,
    GlobalsProvider,
    FileOpener,
    InAppBrowser,
    MyMealPlanProvider,
    Device,
    Firebase,
    FcmService,
    WorkoutProvider,
    Keyboard,
    //InAppBrowser
    //EmbedVideoService
    //PaymentProvider
    //VideoPlayer
  ]
})
export class AppModule {}
